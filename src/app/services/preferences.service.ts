import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalService } from '../services/global.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PreferencesService {

  constructor(
    private http: HttpClient,
    private _globalServ: GlobalService
  ) { }

  getSupercategories(value: any) {
    if (this._globalServ.cityagenda == 2) {
      return this.http.get(environment.apiUrl + '/get-evtcategories');

    }
    else
      return this.http.get(environment.apiUrl + '/supercategories', { params: value });


    // if (this._globalServ.currentPreferences)
    //   return this.http.get(environment.apiUrl + '/get-artsubcategoriesByCategory/' + value);
    // else
    //   return this.http.get(environment.apiUrl + '/supercategories', { params: value });
  }

  getSupercategoriesByUser(id: any) {
    return this.http.get(environment.apiUrl + '/app-user-preferences/' + id);
  }
  saveSupercategories(value: any) {
    if (this._globalServ.currentPreferences)
      return this.http.get(environment.apiUrl + '/cityguide/articles/category/' + this._globalServ.currentPreferences + '?preferences=' + value.supercategoryids);
    else
      return this.http.post(environment.apiUrl + '/app-user-set-preferences', value)
  }

  getPrefencesByCategory(id: any) {
    return this.http.get(environment.apiUrl + '/get-artsubcategoriesByCategory/' + id);
  }
}
