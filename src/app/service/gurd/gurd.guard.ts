import { Injectable,Component } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanDeactivate, Routes } from '@angular/router';
import { Observable } from 'rxjs';
import { GroupCreatePageModule } from 'src/app/pages/group-create/group-create.module';
import { AlertController, NavController, Platform } from '@ionic/angular';
import { Location } from '@angular/common';
import { resolve } from 'url';
import Swal from 'sweetalert2';
import {GlobalService} from "../../services/global.service";
export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable({
  providedIn: 'root'
})
export class GurdGuard implements  CanDeactivate<CanComponentDeactivate>{
  
  constructor(private alertController: AlertController, private location: Location, private _globalServ: GlobalService){

  }
  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
      let header = this._globalServ.language === 'en' ? "Discard Group ?" : "¿Descartar grupo?";
      let message = this._globalServ.language === 'en' ? "Your group hasn't been created yet.If you cancel now , your progress won't be saved." : "Su grupo aún no se ha creado. Si cancela ahora, su progreso no se guardará.";
      let button1 = this._globalServ.language === 'en' ? "Keep" : "Mantener";
      let button2 = this._globalServ.language === 'en' ? "Discard" : "Descarte";
    if(localStorage.getItem('goBack') == 'true'){
      return Swal.fire({
        title: header,
        text: message,
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: button2,
        cancelButtonText: button1
      }).then((result) => {
        console.log(result);
        if (result.value) {
          return true;
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          return false;
        }
      })
    }else{
      return true;
    }
    
  }
}


