import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { SearchService } from '../../services/search.service';
import {Router, NavigationExtras, ActivatedRoute} from '@angular/router';
import { Storage } from '@ionic/storage';
import { get } from 'lodash';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import * as moment from 'moment';
@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  showArrow: boolean = true;
  searchbarSize: number = 12;
  showRecent: boolean = true;

  selectAll: any;
  selectPeople: any;
  selectStudentPlan: any;
  selectEvents: any;
  selectOfferDiscount: any;

  selectgroupplan: any;
  selectcityagenda: any;
  selectcityguide: any;



  keyWord: any = '';

  getListPeople: any;
  getListStudentPlan: any;
  getListOfferDiscount: any;
  getListEvent: any;
  getGroupPlan: any;
  getCityAgendas: any = [];
  getCityGuides: any = [];

  origListPeople: any;
  origListStudentPlan: any;
  origListOfferDiscount: any;
  origListEvent: any;
  origGroupPlans: any;
  origCityAgendas: any;
  origCityGuides: any;

  allFirst: any = true;

  recentList: any[] = [];

  seeAll: any = {
    people: false,
    student_plan: false,
    events: false,
    discount: false,
    group_plan: false,
    city_agenda: false,
    city_guide: false
  };

  constructor(
    public _globalServ: GlobalService,
    private _searchServ: SearchService,
    private storage: Storage,
    private router: Router,
    private iab: InAppBrowser,
    private keyBoard: Keyboard,
    private changeDetc: ChangeDetectorRef,
    private actRoute: ActivatedRoute,
  ) {
    // this.getAll();
    this.actRoute.params.subscribe(val => {
      console.log("---**search - constructor****----");
      this._globalServ.showIcon = true;
    });
    this.storage.get('recentList').then((val) => {
      if (val != null) {
        this.recentList = JSON.parse(val)
      }
    });
  }

  ngOnInit() {
    console.log("---**search - ngonit****----");
    this.getAll();
  }

  onCancel() {
    console.log('---cancel---');
    this._globalServ.showIcon = true;
    this.showArrow = true;
    this.showRecent = true;
    this.searchbarSize = 10;
  }

  onFocus() {
    console.log("----onfocus----");
    this.showArrow = false;
    this.searchbarSize = 12;
    // this.showRecent = false;
    this._globalServ.showIcon = false;
  }

  getAll() {
    this.selectAll = true;
    this.selectPeople = null;
    this.selectStudentPlan = null;
    this.selectEvents = null;
    this.selectOfferDiscount = null;
    this.selectgroupplan = null;
    this.selectcityagenda = null;
    this.selectcityguide = null;
    if (this.allFirst == true) {
      this._searchServ.getList({ 'type': 'people' }).subscribe(data => {
        this.origListPeople = data;
        this.getListPeople = this.origListPeople.slice(0, 5);
      });
      this._searchServ.getList({ 'type': 'student_plan' }).subscribe(data => {
        this.origListStudentPlan = data;
        this.getListStudentPlan = this.origListStudentPlan.slice(0, 5);
      });

      this._searchServ.getList({ 'type': 'events' }).subscribe(data => {
        this.origListEvent = get(data, 'events', []);
        this.getListEvent = this.origListEvent.slice(0, 5);
      });
      this._searchServ.getList({ 'type': 'discount' }).subscribe(data => {
        this.origListOfferDiscount = data;
        this.getListOfferDiscount = this.origListOfferDiscount.slice(0, 5);

      });
      this._searchServ.getList({ 'type': 'group_plan' }).subscribe(data => {
        this.origGroupPlans = data;
        this.getGroupPlan = this.origGroupPlans.slice(0, 5);
        console.log("group =>", this.getGroupPlan);
      });

      this._searchServ.getList({ 'type': 'city_agenda' }).subscribe(data => {
        this.origCityAgendas = data;
        this.getCityAgendas = this.origCityAgendas.slice(0, 5);
      });

      this._searchServ.getList({ 'type': 'city_guide' }).subscribe(data => {
        this.origCityGuides = data;
        this.getCityGuides = this.origCityGuides.slice(0, 5);
      });
      this.allFirst = false;
    } else {
      this.getListPeople = this.origListPeople.slice(0, 5);
      this.getListStudentPlan = this.origListStudentPlan.slice(0, 5);
      this.getListEvent = this.origListEvent.slice(0, 5);
      this.getListOfferDiscount = this.origListOfferDiscount.slice(0, 5);
      this.getCityGuides = this.origCityGuides.slice(0, 5);
      this.getCityAgendas = this.origCityAgendas.slice(0, 5);
      this.getGroupPlan = this.origGroupPlans.slice(0, 5);
    }
  }

  getDataByToggle(attr) {
    this.seeAll[attr] = !this.seeAll[attr];
    if (attr === 'people') {
      this.getListPeople = this.origListPeople.slice(0, (this.seeAll[attr] ? this.origListPeople.length : 5));
    }
    if (attr === 'student_plan') {
      this.getListStudentPlan = this.origListStudentPlan.slice(0, (this.seeAll[attr] ? this.origListStudentPlan.length : 5));
    }
    if (attr === 'events') {
      this.getListEvent = this.origListEvent.slice(0, (this.seeAll[attr] ? this.origListEvent.length : 5));
    }
    if (attr === 'discount') {
      this.getListOfferDiscount = this.origListOfferDiscount.slice(0, (this.seeAll[attr] ? this.origListOfferDiscount.length : 5));
    }
    if (attr === 'group_plan') {
      this.getGroupPlan = this.origGroupPlans.slice(0, (this.seeAll[attr] ? this.origGroupPlans.length : 5));
    }
    console.log(this.seeAll)
  }

  getPeople() {
    this.selectAll = null;
    this.selectPeople = true;
    this.selectStudentPlan = null;
    this.selectEvents = null;
    this.selectOfferDiscount = null;
    this.selectgroupplan = null;
    this.selectcityagenda = null;
    this.selectcityguide = null;

    this.getListPeople = this.origListPeople;
    if (this.keyWord != '') {
      this.getListPeople = this.getListPeople.filter(s => s.name.toUpperCase().includes(this.keyWord.toUpperCase()));
    }
  }

  getStudentPlan() {
    this.selectAll = null;
    this.selectPeople = null;
    this.selectStudentPlan = true;
    this.selectEvents = null;
    this.selectOfferDiscount = null;
    this.selectgroupplan = null;
    this.selectcityagenda = null;
    this.selectcityguide = null;
    this.getListStudentPlan = this.origListStudentPlan;
    if (this.keyWord != '') {
      this.getListStudentPlan = this.getListStudentPlan.filter(s => s.name.toUpperCase().includes(this.keyWord.toUpperCase()));
    }
  }

  //pending
  getEvents() {
    this.selectAll = null;
    this.selectPeople = null;
    this.selectStudentPlan = null;
    this.selectEvents = true;
    this.selectOfferDiscount = null;
    this.selectgroupplan = null;
    this.selectcityagenda = null;
    this.selectcityguide = null;
    this.getListEvent = this.origListEvent;
    if (this.keyWord != '') {
      this.getListEvent = this.getListEvent.filter(s => s.title.toUpperCase().includes(this.keyWord.toUpperCase()));
    }
  }

  getOfferDiscount() {
    this.selectAll = null;
    this.selectPeople = null;
    this.selectStudentPlan = null;
    this.selectEvents = null;
    this.selectOfferDiscount = true;
    this.selectgroupplan = null;
    this.selectcityagenda = null;
    this.selectcityguide = null;
    this.getListOfferDiscount = this.origListOfferDiscount;
    if (this.keyWord != '') {
      this.getListOfferDiscount = this.getListOfferDiscount.filter(s => s.name.toUpperCase().includes(this.keyWord.toUpperCase()));
    }
  }

  kuSearch(value: any) {
    console.log("------********kuSearch---------");
    this.keyWord = value.target.value;
    this.showRecent = false;
    if (value.keyCode == 13) {
      console.log("-----keyBoardhide----");
      this.keyBoard.hide();
      this._globalServ.showIcon = true;
    } else {
      console.log("-----keyBoardshow----");
      this._globalServ.showIcon = false;
      if (this.selectAll == true) {
        this.keyPeopleFuc(value);
        this.keyStudentPlanFunc(value);
        this.keyEventFunc(value);
        this.keyOfferDiscountKey(value);
        this.keyGroupPlans(value);
        this.keyCityAgenda(value);
        this.keyCityGuide(value);
      }
      if (this.selectPeople == true) {
        this.keyPeopleFuc(value);
      }
      if (this.selectStudentPlan == true) {
        this.keyStudentPlanFunc(value);
      }
      if (this.selectEvents == true) {
        this.keyEventFunc(value);
      }
      if (this.selectOfferDiscount == true) {
        this.keyOfferDiscountKey(value);
      }
      if (this.selectgroupplan == true) {
        this.keyGroupPlans(value);
      }
      if (this.selectcityagenda == true) {
        this.keyCityAgenda(value);
      }
      if (this.selectcityguide == true) {
        this.keyCityGuide(value);
      }
    }
  }

  keyPeopleFuc(value: any) {
    this.getListPeople = this.origListPeople;
    this.getListPeople = this.getListPeople.filter(s => s.name.toUpperCase().includes(value.target.value.toUpperCase()));
  }

  keyStudentPlanFunc(value: any) {
    this.getListStudentPlan = this.origListStudentPlan;
    this.getListStudentPlan = this.getListStudentPlan.filter(s => s.name.toUpperCase().includes(value.target.value.toUpperCase()));
  }

  keyEventFunc(value: any) {
    // debugger;
    this.getListEvent = this.origListEvent;
    this.getListEvent = this.getListEvent.filter(s => s.title.toUpperCase().includes(value.target.value.toUpperCase()));
  }

  keyOfferDiscountKey(value: any) {
    this.getListOfferDiscount = this.origListOfferDiscount;
    this.getListOfferDiscount = this.getListOfferDiscount.filter(s => s.name.toUpperCase().includes(value.target.value.toUpperCase()));
  }

  keyGroupPlans(value: any) {
    this.getGroupPlan = this.origGroupPlans;
    this.getGroupPlan = this.getGroupPlan.filter(s => s.name.toUpperCase().includes(value.target.value.toUpperCase()));
  }

  keyCityAgenda(value: any) {
    this.getCityAgendas = this.origCityAgendas;
    this.getCityAgendas = this.origCityAgendas.filter(s => s.name.toUpperCase().includes(value.target.value.toUpperCase()));
  }

  keyCityGuide(value: any) {
    this.getCityGuides = this.origCityGuides;
    this.getCityGuides = this.getCityGuides.filter(s => s.name.toUpperCase().includes(value.target.value.toUpperCase()));
  }

  onClear() {
    console.log("------********onclear--------- " + this.showArrow);
    this._globalServ.showIcon = this.showArrow;
    this.getListPeople = this.origListPeople;
    this.getListStudentPlan = this.origListStudentPlan;
    this.getListEvent = this.origListEvent;
    this.getListOfferDiscount = this.origListOfferDiscount;
  }

  getGroupPlans() {
    this.selectAll = null;
    this.selectPeople = null;
    this.selectStudentPlan = null;
    this.selectEvents = null;
    this.selectOfferDiscount = null;
    this.selectgroupplan = true;
    this.selectcityagenda = null;
    this.selectcityguide = null;
    this.getGroupPlan = this.origGroupPlans;
    if (this.keyWord != '') {
      this.getGroupPlan = this.getGroupPlan.filter(s => s.name.toUpperCase().includes(this.keyWord.toUpperCase()));
    }
  }
  getCityAgenda() {
    this.selectAll = null;
    this.selectPeople = null;
    this.selectStudentPlan = null;
    this.selectEvents = null;
    this.selectOfferDiscount = null;
    this.selectgroupplan = null;
    this.selectcityagenda = true;
    this.selectcityguide = null;
    this.getCityAgendas = this.origCityAgendas;
    if (this.keyWord != '') {
      this.getCityAgendas = this.getCityAgendas.filter(s => s.name.toUpperCase().includes(this.keyWord.toUpperCase()));
    }
  }
  getCityGuide() {
    this.selectAll = null;
    this.selectPeople = null;
    this.selectStudentPlan = null;
    this.selectEvents = null;
    this.selectOfferDiscount = null;
    this.selectgroupplan = null;
    this.selectcityagenda = null;
    this.selectcityguide = true;
    this.getCityGuides = this.origCityGuides;
    if (this.keyWord != '') {
      this.getCityGuides = this.getCityGuides.filter(s => s.name.toUpperCase().includes(this.keyWord.toUpperCase()));
    }
  }

  goToPeople(value: any) {


    let navExtra: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({
          id: value.id,
          email: value.email
        })

      }
    }
    this.router.navigate(['/view-profile'], navExtra);
    for (let item of this.recentList) {
      if (value.id == item.id) {
        return
      }
    }
    if (this.recentList.length > 10) {
      this.recentList.splice(0, 1);
    }
    value.type = 'people';
    this.recentList.push(value);
    this.storage.set('recentList', JSON.stringify(this.recentList));
  }

  goToStudentPlan(value: any) {

    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'id': value.id, 'type': value.type })
      },
    };
    this.router.navigate(['plan-item'], navigationExtras)
    for (let item of this.recentList) {
      if (value.id == item.id) {
        return
      }
    }
    if (this.recentList.length > 10) {
      this.recentList.splice(0, 1);
    }
    value.type = 'student_plan';
    this.recentList.push(value);
    this.storage.set('recentList', JSON.stringify(this.recentList));
  }

  goToEvent(value: any) {

    /*if (this.recentList.length > 10) {
      this.recentList.splice(0, 1);
    }
    value.type = 'event'; 
    this.recentList.push(value);
    this.storage.set('recentList', JSON.stringify(this.recentList)); */
    this.iab.create(value.eventLink, '_system');
    for (let item of this.recentList) {
      if (value.id == item.id) {
        return
      }
    }
  }

  goToOfferDiscount(value: any) {
    var discData;
    this._searchServ.getOfferAnddiscount(value.id).subscribe(data => {
      discData = data;
      let navigationExtras: NavigationExtras = {
        queryParams: {
          data: JSON.stringify({ 'item': discData.discount })
        }
      };
      this.router.navigate(['/offers-discoutns-item'], navigationExtras);
    });
    for (let item of this.recentList) {
      if (value.id == item.id) {
        return
      }
    }
    if (this.recentList.length > 10) {
      this.recentList.splice(0, 1);
    }
    value.type = 'discount';
    this.recentList.push(value);
    this.storage.set('recentList', JSON.stringify(this.recentList));
  }

  getDate(date) {
    return moment(date).format('ddd, MMM DD')
  }

  search_data() {
    console.log("------********search data--------- " + this.showArrow);
    this.showRecent = false;
    this._globalServ.showIcon = this.showArrow;
  }


  goRecentSearches(item) {
    console.log(item);
    if (item.object_type == "discount") {
      this.goToOfferDiscount(item)
    }

    if (item.object_type == "student_plan") {
      this.goToStudentPlan(item);
    }

    if (item.object_type == "people") {
      this.goToPeople(item)
    }

    if (item.object_type == 'group_plan') {
      this.goToGroupPlan(item);
    }

    if (item.object_type == 'city_agenda') {
      this.goToCityAgenda(item);
    }

    if (item.object_type == 'city_guide') {
      this.goToCityGuide(item);
    }

    if (item.object_type == 'plan_date') {
      this.goToEvent(item);
    }

  }

  goToGroupPlan(value) {

    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'id': value.id }),
      }
    };
    this.router.navigate(['group-plan-item'], navigationExtras)
    for (let item of this.recentList) {
      if (value.id == item.id) {
        return
      }
    }
    console.log(value)
    if (this.recentList.length > 10) {
      this.recentList.splice(0, 1);
    }
    value.type = 'group_plan';
    this.recentList.push(value);

    this.storage.set('recentList', JSON.stringify(this.recentList));
  }

  goToCityAgenda(value) {

    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'id': value.id })
      },
    };
    this.router.navigate(['city-agenda-item'], navigationExtras)
    for (let item of this.recentList) {
      if (value.id == item.id) {
        return
      }
    }
    console.log(value)
    if (this.recentList.length > 10) {
      this.recentList.splice(0, 1);
    }
    value.type = 'city_agenda';
    this.recentList.push(value);
    this.storage.set('recentList', JSON.stringify(this.recentList));
  }

  goToCityGuide(value) {

    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'id': value.id })
      },
    };
    this.router.navigate(['city-guide-item'], navigationExtras)
    for (let item of this.recentList) {
      if (value.id == item.id) {
        return
      }
    }
    console.log(value)
    if (this.recentList.length > 10) {
      this.recentList.splice(0, 1);
    }
    value.type = 'city_agenda';
    this.recentList.push(value);
    this.storage.set('recentList', JSON.stringify(this.recentList));
  }

  showTabMenus(event) {
    console.log("event: " , event);
    this._globalServ.showIcon = true;
  }
}




