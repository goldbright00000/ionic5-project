import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { CityGuideService } from '../../services/city-guide.service';
import { GlobalService } from '../../services/global.service';
import { environment } from '../../../environments/environment';
import {NavController} from "@ionic/angular";

@Component({
  selector: 'app-city-guide-item-list',
  templateUrl: './city-guide-item-list.page.html',
  styleUrls: ['./city-guide-item-list.page.scss'],
})
export class CityGuideItemListPage implements OnInit {

  id: any;
  getListItem: any;
  name_EN: any;
  name_ES: any;
  url: any = environment.apiUrl;
  pageTitleName: any = false;


  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    public _cityGuideServ: CityGuideService,
    public _globalServ: GlobalService,

    private navCtrl: NavController
  ) {
    if (this.actRoute.snapshot.queryParams.data != null) {
      let getParams = JSON.parse(this.actRoute.snapshot.queryParams.data);
      this.id = getParams.id;
      this._globalServ.currentPreferences = this.id;
      this.name_EN = getParams.name_EN;
      this.name_ES = getParams.name_ES;
    }
    // this._cityGuideServ.getArticlesCategory(this.id)
    // .subscribe(data => {
    //   this.getListItem = data;
    // }, error => {
    //   console.log(error);
    // })
    this.actRoute.params.subscribe(val =>{
      if(this._globalServ.category && this._globalServ.category.length){
          console.log(this._globalServ.category.join());
        this._cityGuideServ.getArticlesCategoyByPreference(this.id, this._globalServ.category.join())
        .subscribe(data => {
        this.getListItem = data;
        console.log(this.getListItem);
      }, error => {
        console.log(error);
       })
      }else{
          this._cityGuideServ.getArticlesCategory(this.id)
        .subscribe(data => {
        this.getListItem = data;
      }, error => {
        console.log(error);
       })
      }
    })
  }

  getFilterData

  ngOnInit() {
    console.log("city guide list page")
  }

  logScrolling(e) {
    if (e.detail.scrollTop > 50) {
      this.pageTitleName = true;
    } else {
      this.pageTitleName = false;
    }
  }
  ionViewDidEnter() {
    this._globalServ.filtersArry = '';
    this._cityGuideServ.getArticlesCategory(this.id)
      .subscribe(data => {
        this.getListItem = data;
      }, error => {
        console.log(error);
      })
    this._globalServ.cityGiude = true;
  }
  ionViewDidLeave() {
    this._globalServ.cityGiude = undefined;
  }
  goToCityGuideItem(value: any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'id': value.id })
      },
    };
    this.router.navigate(['city-guide-item'], navigationExtras)
  }

  openFilter(){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'city_guide' : 1, 'cat_id': this.id})
      }
    };
      this.navCtrl.navigateForward(['filters'], navigationExtras);
  }
}
