import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';

import { UserService } from 'src/app/services/user.service';
import { Platform } from '@ionic/angular';
import { get } from 'lodash';
import { GroupService } from 'src/app/services/group.service';
@Component({
  selector: 'app-group-create-success',
  templateUrl: './group-create-success.page.html',
  styleUrls: ['./group-create-success.page.scss'],
})
export class GroupCreateSuccessPage implements OnInit {
  plan: any;
  success: any;
  appURL: any;
  allGroups: any;
  id: any;

  constructor(
    private router: Router, 
    public _groupServ: GroupService, 
    private platform: Platform, 
    private route: ActivatedRoute, 
    public _userServ: UserService, 
    private socialSharing: SocialSharing,
    private ga: GoogleAnalytics) {
    this.route.queryParams.subscribe((params: any) => {
      if (params.data)
        this.success = params.data;
      if (this.router.getCurrentNavigation().extras.state) {
        this.plan = this.router.getCurrentNavigation().extras.state.title;
        this._groupServ.getGroupList().subscribe(data => {
          console.log('group list: ', data);
          this.allGroups = get(data, 'group', []);
          for (let group of this.allGroups) {
            if (group.title == this.plan) {
              this.id = group.id;
            }
          }
        }, err => {
          console.log('get group err: ', err);
        });
      }
    });
    this._userServ.appInfo().subscribe((res: any) => {

      if (this.platform.is('ios'))
        this.appURL = res.app_info[1].url
      else
        this.appURL = res.app_info[0].url
    })

  }

  ngOnInit() {
  }
  invitePeople() {

    this.router.navigate(['group-create-invite']);
  }
  continue() {
    // this.router.navigate(['tabs/plan-list']);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'id': this.id })
      },
    };
    this.router.navigate(['group-detail'], navigationExtras)
  }
  share(to) {

    if (to == "whatsapp") {
      this.socialSharing.shareViaWhatsApp('Hi! Download VIVE IE app and join my group ' + this.plan, null,  this.appURL).then((res) => {
        // Success!
        this.ga.trackEvent("App Link", "Shared via Whatsapp");
        console.log(res)
      }).catch((err) => {
        // Error!
        console.log(err)
      });
    }
    if (to == "messanger") {
      this.socialSharing.shareVia("com.facebook.orca", 'Hi! Download VIVE IE app and join my group ' + this.plan, null, null,  this.appURL).then((res) => {
        // Success!
        this.ga.trackEvent("App Link", "Shared via Messanger");
        console.log(res)
      }).catch((err) => {
        // Error!
        console.log(err)
      });
    }
    if (to == "sms") {
      this.socialSharing.shareViaSMS('Hi! Download VIVE IE app and join my group ' + this.plan + ' ' + this.appURL, null).then((res) => {
        // Success!
        this.ga.trackEvent("App Link", "Shared via SMS");
        console.log(res)
      }).catch((err) => {
        // Error!
        console.log(err)
      });
    }

  }
}
