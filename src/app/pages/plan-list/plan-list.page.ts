import { Component, OnInit, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { GlobalService } from '../../services/global.service';
import { PlanService } from '../../services/plan.service';
import { get } from 'lodash';
import { ActionSheetController, NavController, IonRefresher } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { TabsPage } from '../tabs/tabs.page'
import { FilterService } from 'src/app/services/filter.service';
import { Calendar } from '@ionic-native/calendar/ngx';


@Component({
  selector: 'app-plan-list',
  templateUrl: './plan-list.page.html',
  styleUrls: ['./plan-list.page.scss'],
})
export class PlanListPage implements OnInit {
  @ViewChild(IonRefresher)
  refresher: IonRefresher;
  getHighlightPlans: any;
  getFilteredPlans: any;
  getClubPlans: any;
  getMyPlans: any;
  pageTitleName: any = false;
  isMyPlans: any = false;
  ieClubSpinner = true;
  constructor(
    private splashScreen: SplashScreen,
    private router: Router,
    public _globalServ: GlobalService,
    public actionSheetController: ActionSheetController,
    public storage: Storage,
    public _planServ: PlanService,
    private _filterServ: FilterService,
    public tab: TabsPage,
    private actRoute: ActivatedRoute,
    private navCtrl: NavController,
    private calendar: Calendar
  ) {
    this.actRoute.params.subscribe(val => {
      this.getPlan();
    })

    //window.scrollTo(0,0);
  }


  ngOnInit() {

  }

  async getPlan() {
    console.log('idUser: ', this._globalServ.idUser);
    await this._planServ.getDataPlanList();
    await this.getFilterByUser();
    //document.querySelector('ion-tab-bar').style.display = 'none';
    navigator.geolocation.getCurrentPosition(function (position) {
    });
  }
  ionViewWillEnter() {
    console.log('ionViewWillEnter');
  }
  ionViewDidEnter() {
    this.refresher.disabled = false;
  }
  ionViewWillLeave() {
    this.refresher.disabled = true;
  }
  getFilterByUser() {
    this._filterServ.getFilters().subscribe(data => {
      let getFilters: any;
      getFilters = data;
      let getFiltersByUser: any = [];
      this._filterServ.getFiltersByUser(this._globalServ.idUser).
        subscribe(data => {
          getFiltersByUser = data;
          console.log("filter by user =>", getFiltersByUser);
          let type = getFiltersByUser.user_filters.filter(s => s.Filter.sub_type_en == 'Type of feed');
          let flag;
          for (let i = 0; i < getFiltersByUser.user_filters.length; i++) {
             if (getFiltersByUser.user_filters[i].Filter.sub_type_en == 'Type of feed' 
            && ((getFiltersByUser.user_filters[i].Filter.option_en == 'Student plans') || (getFiltersByUser.user_filters[i].Filter.option_en == 'Group plans') )) {
              flag = true;
              break;
            } else {
              flag = false;
            }
          }

          if (flag) {
            this.ieClubSpinner = false;
            console.log("ie club flag => ", this.ieClubSpinner);
          } else {

            this._planServ.getIEClubPlans().subscribe(data => {
              // console.log('data: ', get(data, 'events', []));
              this.getClubPlans = get(data, 'events', []);
              this.getClubPlans = this.getClubPlans.map(o => {
                return {
                  ...o,
                  type: 'club_plan'
                }
              });
              this.ieClubSpinner = true;
            }, err => {
              console.log(err);
            })
          }
          console.log("type of feed =>", type);
        })
    }, error => { console.log(error); })
  }

  ionViewWillLoad() {
    console.log('ionViewWillEnter')


  }
  logScrolling(e) {
    if (e.detail.scrollTop > 50) {
      this.pageTitleName = true;
    } else {
      this.pageTitleName = false;
    }
  }


  showMyPlans() {

    this._planServ.getMyPlans(this._globalServ.idUser).subscribe(data => {
      console.log('data: ', get(data, 'plans', []));
      this.getMyPlans = get(data, 'plans', []);
      this.isMyPlans = true;
      this.getMyPlans = this.getMyPlans.map(o => {
        return {
          ...o,
          type: 'my_plan'
        }
      })
    }, err => {
      console.log(err);
    })
  }

  openFilter() {

    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'city_guide': 0 })
      }
    };
    this.navCtrl.navigateForward(['filters'], navigationExtras);
  }
  async doRefresh(event) {
  
    console.log('Begin async operation');

    await this.getPlan();


    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }

  openCalender(){
    console.log('Clicked');
    this.calendar.openCalendar(new Date())
  }
}
