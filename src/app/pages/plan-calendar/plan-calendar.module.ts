import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { PlanCalendarPage } from "./plan-calendar.page";
import { ComponentModule } from "../../components/component.module";

const routes: Routes = [
  {
    path: "",
    component: PlanCalendarPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PlanCalendarPage]
})
export class PlanCalendarPageModule {}
