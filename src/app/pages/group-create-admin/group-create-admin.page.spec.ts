import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupCreateAdminPage } from './group-create-admin.page';

describe('GroupCreateAdminPage', () => {
  let component: GroupCreateAdminPage;
  let fixture: ComponentFixture<GroupCreateAdminPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupCreateAdminPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupCreateAdminPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
