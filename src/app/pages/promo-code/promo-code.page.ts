import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';
import { UserService } from 'src/app/services/user.service';
import { ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';


@Component({
	selector: 'app-promo-code',
	templateUrl: './promo-code.page.html',
	styleUrls: ['./promo-code.page.scss'],
})
export class PromoCodePage implements OnInit {
	code: any
	wrongCode: boolean;
	congrats: boolean;
	skip: any;
	constructor(private translate: TranslateService,
		private _globalServ: GlobalService,
		private _userServ: UserService,
		private toastCtrl: ToastController,
		private actRoute: ActivatedRoute,
		private storage: Storage,
		private router: Router) {
		this.storage.get('promoApplied').then(val => {
			if (val) {
				this.code = val;
				this.congrats = true;
			} else
				this.congrats = false;
		});
	}

	async ngOnInit() {
		if (this.actRoute.snapshot.queryParams.data != null) {
		let getParams = JSON.parse(this.actRoute.snapshot.queryParams.data);
		this.skip = getParams.lauch;
		}
		await this.storage.get('promoApplied').then(val => {
			if (val) {
				this.code = val;
				this.congrats = true;
			} else
				this.congrats = false;
		});
	}

	continue() {
		if (this.code == '' || !this.code)
			this.showToast('Enter a promo code');
		else {


			this._userServ.checkPromoCode(this.code).subscribe((data: any) => {
				console.log(data);
				if (data.message == "Updated referral count.") {
					this.storage.set("promoApplied", this.code);
					this.router.navigate(['promo-code-success']);

				}
				else {
					this.wrongCode = true;
				}


			}, err => {
				this.wrongCode = true;
				console.log('err public profile: ', err);
			});
		}
	}

	async showToast(msg) {
		const toast = await this.toastCtrl.create({
			message: msg,
			duration: 2000
		})
		await toast.present();
	}
	changed() {
		this.wrongCode = false;
	}
	gotodashboard() {
		this.router.navigate(['tabs/plan-list']);
	}
}
