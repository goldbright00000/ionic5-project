import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { GlobalService } from '../../services/global.service';
import { Storage } from '@ionic/storage';
import { ToastController, Platform, ActionSheetController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';
import { get } from 'lodash';
import { environment } from '../../../environments/environment';
import { GroupService } from 'src/app/services/group.service';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

import { Keyboard } from '@ionic-native/keyboard/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
@Component({
  selector: 'app-edit-group',
  templateUrl: './edit-group.page.html',
  styleUrls: ['./edit-group.page.scss'],
})
export class EditGroupPage implements OnInit {

  chosenImage: any = null;
  title: any = '';
  description: any = '';
  groupCreateSettingsValue: any = '';
  pageTitleName: any = false;
  category: any;
  pick: boolean;
  done: boolean = true;
  data: any;
  pathImageCropper: any;
  pathImagePost: any;
  pathImage: any;
  adminlist: any = [];
  groupAdminSettingValue: any;
  public formValid: boolean = false;
  

  constructor(
    public base64: Base64,
    public router: Router,
    public _globalServ: GlobalService,
    private storage: Storage,
    private toastController: ToastController,
    public _userServ: UserService,
    public _groupServ: GroupService,
    private keyboard: Keyboard,
    private transfer: FileTransfer,
    private crop: Crop,
    private sanitizer: DomSanitizer,
    private webview: WebView,
    private camera: Camera,
    private platform: Platform,
    private route: ActivatedRoute,
    private ga: GoogleAnalytics,

    public actionSheetController: ActionSheetController,

  ) {
    this.route.queryParams.subscribe((params: any) => {
      if (this.router.getCurrentNavigation().extras.queryParams) {
        this.data = this.router.getCurrentNavigation().extras.queryParams.data;
        console.log(this.data);
        this.title = this.data.group.title;
	    	this.description = this.data.group.description;
        this.chosenImage = this.data.group.image;
        this.category = this.data.groupCategories;
        this.adminlist = this.data.groupAdmins;
        this.storage.set('group-create-category', this.data.groupCategories);
        this.storage.set('group-create-admin', this.data.groupAdmins);
        this.getSettingOfGroup(this.data.group.id);
        // this.groupCreateSettingsValue = this.data.groupSettings.Groups_Setting.name;
        // this.storage.set('group-create-settings', this.data.groupSettings.Groups_Setting.id);
        
        this._globalServ.groupImagePath = this.data.group.image;
        this.pathImageCropper = this.data.group.image;
        this.pick = true;
        this._globalServ.invited = this.data.members.length;
        this.storage.set('group-create-members', this.data.members);
      }
    });
    if (this.platform.is('android')) {
      if (this.platform.is('ios')) {
        window.addEventListener('keyboardWillShow', (event: any) => {
          console.log("keyboard show", event)
          document.getElementById('container').style.marginBottom = (event.keyboardHeight) + 'px';
        })

        window.addEventListener('keyboardDidHide', (event: any) => {
          console.log("keyboard show", event)
          document.getElementById('container').style.marginBottom = 'unset';
        })
      }
    }

  }

  ngOnInit() {
  }

  logScrolling(e) {
    if (e.detail.scrollTop > 50) {
      this.pageTitleName = true;
    } else {
      this.pageTitleName = false;
    }
  }

  ionViewDidEnter() {
    this.validate();
    if (this._groupServ.backToEdit) {
      this._groupServ.backToEdit = false;
    }
	      this.storage.get('group-create-category').then((val) => {
		    console.log("categories values", val);
        this.category = [];
        this._groupServ.getGroupCategories().subscribe((data: any) => {
          if (data && val && data.length && val.length) {
            data.forEach(allCategory => {
              val.forEach(selectedCategory => {
                if (allCategory.id === selectedCategory.id || allCategory.id === selectedCategory) {
                  this.pick = true;
                  this.category.push(allCategory);
                }
              });
            });
          }
            this.validate();
        });
      });
    console.log('this._globalServ.groupImagePath:  ', this._globalServ.groupImagePath);
     //this.chosenImage = this._globalServ.groupImagePath;
     this.getGroupSettings();

	   this.getAdminSettings();
  }
  ionViewDidLeave() {
    //this._globalServ.invited = null;
  }
  getSettingOfGroup(groupID){ 
      this._groupServ.getGroupSettingsSingle(groupID).subscribe((data: any) => {
         this.groupCreateSettingsValue = data.Groups_Setting.name;
         this.storage.set('group-create-settings', data.Groups_Setting.id);
      });
  }
  async goToselectImage() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'plan': true })
      }
    };
    let titleLabel;
    let cancelLabel;
    let EditLabel;
    let ChangeLabel;
    let RemoveLabel;
    if (this._globalServ.language == 'en') {
      titleLabel = 'Photo';
      cancelLabel = 'Cancel';
      EditLabel = 'Edit Photo';
      ChangeLabel = 'Change Photo';
      RemoveLabel = 'Remove Photo';
    }
    if (this._globalServ.language == 'es') {
      titleLabel = '';
      cancelLabel = 'Cancel';
      EditLabel = 'Edit Photo';
      ChangeLabel = 'Change Photo';
      RemoveLabel = 'Remove Photo'
    }
    // const actionSheetForCancel = await this.actionSheetController.create({
    // 	cssClass: 'actionsheet2',
    // 	buttons: [{
    // 		text: cancelLabel,
    // 		handler: () => {
    // 			actionSheetForPlan_group.dismiss()
    // 		}
    // 	}
    // 	]
    // });


    const actionSheetForPlan_group = await this.actionSheetController.create({
      header: titleLabel,

      buttons: [
        {
          text: EditLabel,

          handler: () => {

            // this.router.navigate(['image-cropper'], navigationExtras);
            this.cropFunc()

          }
        },
        {
          text: ChangeLabel,
          handler: () => {
            this.selectFile()
            // this.router.navigate(['image-cropper'], navigationExtras);
          }
        },
        {
          text: RemoveLabel,
          handler: () => {
            this._globalServ.planImagePath = null;



          }
        },
        {
          text: cancelLabel,

          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    }).then(actionsheet => {
      actionsheet.present();
    });;



  }
  selectFile() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: 2
    }

    this.camera.getPicture(options).then((imageData) => {
      this.pathImage = this.sanitizer.bypassSecurityTrustUrl(this.webview.convertFileSrc(imageData));
      // this.pathImage = this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + imageData);
      this.pathImageCropper = imageData;
      this.pathImagePost = imageData;
      this._globalServ.groupImagePath = this.pathImage;
      this._globalServ.groupImagePost = this.pathImagePost;
      this.chosenImage = this.pathImage;
      this.cropFunc()

      console.log("Image path postt", this.pathImagePost);
    }, (err) => {
      console.log(err);
    });
  }
  async cropFunc() {
    await this.crop.crop(this.pathImageCropper, { quality: 75 })
      .then(
        newImage => {
          this.pathImage = this.sanitizer.bypassSecurityTrustUrl(this.webview.convertFileSrc(newImage));
          // this.pathImage = this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + newImage);
          this.pathImageCropper = newImage;
          this.pathImagePost = newImage;
          this._globalServ.groupImagePath = this.pathImage;
          this._globalServ.groupImagePost = this.pathImagePost;
          this.chosenImage = this.pathImage;
        },
        error => console.error('Error cropping image', error)
      );
  }
  createCategory() {
      let navigationExtras: NavigationExtras = {
          queryParams: {
              data: JSON.stringify({ 'group_create': true })
          }
      };
    this.router.navigate(['group-create-category'], navigationExtras);
  }

  pickSettings() {
    this.router.navigate(['group-create-settings']);
  }
  adminPeopel() {
    localStorage.setItem('goBack', 'false');
    this.router.navigate(['group-create-admin']);
  }

  invitePeople() {
    this.router.navigate(['group-create-invite']);
  }

  async showAlert(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  doCreateGroup(id, params) {
    // let filePath: string = this.chosenImage
    // this.base64.encodeFile(filePath).then((base64File: string) => {
    //   console.log(base64File);
    //   this._globalServ.groupImagePost = base64File
    // }, (err) => {
    //   console.log(err);
    // });
    if (!this._globalServ.groupImagePost) {


      console.log('<<<  starting group creating....   >>>');
      let navigationExtras: NavigationExtras = {
        state: {
          title: this.title,
        }
      };
      this._groupServ.saveEditGroupDetails(id, params).subscribe(data => {
        this.done = true;
        // this.showAlert('Created a group successfully');
        console.log('<<<<group updated successfully !!!>>>>');
        
        this.storage.set('group-create-category', []);
        this.storage.set('group-create-settings', -1); // null or -1
        this.storage.set('group-create-members', []);
        this._globalServ.groupImagePath = null;
        this._globalServ.groupImagePost = null;
        this.title = this.description = '';

        this.chosenImage = null;

        this.ga.trackEvent("Group - "+this.data.group.title, "Edited");
        this.router.navigate(['group-create-success'], navigationExtras);
      }, err => {
        console.log('err: ', err);
        this.done = false;
        //  this.showAlert(err.message);
      })

    }
    else {
      console.log('<<<  starting group creating....   >>>');
      let navigationExtras: NavigationExtras = {
        state: {
          title: this.title,
        }
      };
      const fileTransfer: FileTransferObject = this.transfer.create();
      let filename = 'groupImage_' + Math.random() * 100000000000000000;
      let options: FileUploadOptions = {
        fileKey: 'image',
        fileName: filename + '.jpg',
        params: params,
      }

      fileTransfer.upload(this._globalServ.groupImagePost, environment.apiUrl + '/edit-group', options)
        .then((data) => {
          //   this.showAlert('Created a group successfully');
          console.log('<<<<created group successfully !!!>>>>');

          this.storage.set('group-create-category', []);
          this.storage.set('group-create-settings', -1); // null or -1
          this.storage.set('group-create-members', []);
          this._globalServ.groupImagePath = null;
          this._globalServ.groupImagePost = null;
          this.title = this.description = '';

          this.chosenImage = null;

          this.router.navigate(['group-create-success'], navigationExtras);
        }, (err) => {
          // error
          console.log('error', err);
          this.done = false;
          // this.showAlert(err.message);
        });
    }
  }

  getGroupSettings() {
    this._groupServ.getGroupSettings().subscribe(data => {
      console.log('group settings data: ', data);
      let groupSettings = [];
      groupSettings = JSON.parse(JSON.stringify(data));
      this.storage.get('group-create-settings').then((groupSettingsVal) => {
        console.log('groupSettingsVal--- : ', groupSettingsVal);
        if (groupSettingsVal > 0) {
          this.groupCreateSettingsValue = groupSettings.filter(o => o.id === groupSettingsVal)[0].name;
        } else {
          this.groupCreateSettingsValue = '';
        }
      });
    }, err => {
      console.log('group settings err: ', err);
    })
  }
  getAdminSettings() {
      this._groupServ.getPeoplesForInvitation().subscribe(data => {

      let groupAdminSettings = [];
      groupAdminSettings = JSON.parse(JSON.stringify(data));
	  
      this.storage.get('group-create-admin').then((groupAdminSettingsVal) => {
        this.adminlist = groupAdminSettingsVal;
        console.log('groupSettingsVal--- : ', groupAdminSettingsVal);
        if (groupAdminSettingsVal[0] > 0) {
          this.groupAdminSettingValue = groupAdminSettings.filter(o => o.id === groupAdminSettingsVal[0])[0].name;
          console.log(this.groupAdminSettingValue);
        }
      });
    }, err => {
      console.log('group settings err: ', err);
    })
  }

  save() {

    let noticeMsg = '';
    let group_supercategory_ids = '';
    let group_settings_ids = '';
    let group_member_ids = '';

    let email;
    let fk_user_id;
    if(!this._globalServ.groupImagePath) {
        this.showAlert('Please upload the image');
        return;
    }

    this._userServ.getPublicProfile(this._globalServ.idUser).subscribe(data => {
      let profile = get(data, '[0]', {});
      email = get(profile, 'email', '');
      fk_user_id = this._globalServ.idUser;

      if (this._globalServ.groupImagePath) {
        this.storage.get('group-create-category').then((groupSuperCategoryVal) => {
          if (groupSuperCategoryVal.length <= 3 && groupSuperCategoryVal.length !== 0) {

            groupSuperCategoryVal.forEach(category => {
              if (typeof category === "object") {
                group_supercategory_ids = group_supercategory_ids + category.id.toString() + ',';
              } else {
                group_supercategory_ids = group_supercategory_ids + category.toString() + ',';
              }
            });
            this.storage.get('group-create-settings').then((groupSettingsVal) => {
              if (groupSettingsVal > 0) {

                group_settings_ids = groupSettingsVal.toString();
                this.storage.get('group-create-members').then((groupMemberVal) => {
                  if (groupMemberVal.length > 0) {
                    groupMemberVal.forEach(member => {
                      if (typeof member === "object") {
                        group_member_ids = group_member_ids + member.id.toString() + ',';
                      } else {
                        group_member_ids = group_member_ids + member.toString() + ',';
                      }
                    });
                  }
                    const imgIndex = this.data.group.image ? this.data.group.image.lastIndexOf("/") : 0;
                    const imagePath = imgIndex ? this.data.group.image.substr(imgIndex + 1, this.data.group.image.length) : (this.data.group.image ? this.data.group.image : "");
                    if (this.title !== '') {
                        if (this.description !== '') {
                            const params = {
                                title: this.title,
                                description: this.description,
                                // image: this.chosenImage,
                                group_settings_ids: group_settings_ids,
                                group_supercategory_ids: group_supercategory_ids.substr(0, group_supercategory_ids.length - 1),
                                group_member_ids: group_member_ids.substr(0, group_member_ids.length - 1),
                            };
                            this.done = false;
                            this.doCreateGroup(this.data.group.id, params);
                        } else {
                            this.showAlert('Please fill in the group description');
                        }
                    } else {
                        this.showAlert('Please fill in the group title');
                    }
                });
              } else {
                this.showAlert('Please choose group setting');
              }
            });
          } else {
            this.showAlert('It is limited to choose 3 categories or Please choose categories');
          }
        });
      } else {
        this.showAlert('Please upload group image');
      }

    }, err => {
      console.log('err public profile: ', err);
    });
  }

  hideKeyboard() {
    this.keyboard.hide();
  }

    validate() {
        this.formValid = this.title && this.category && this.category.length && this.description ? true : false;
    }
}