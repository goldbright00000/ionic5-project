import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { GlobalService } from '../../services/global.service';
import { environment } from '../../../environments/environment';
import { get } from 'lodash';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-profile-plans',
  templateUrl: './view-profile-plans.page.html',
  styleUrls: ['./view-profile-plans.page.scss'],
})
export class ViewProfilePlansPage implements OnInit {

  Participations: any;
  url: any = environment.apiUrl;
  firstName: any = '';
  plans: any = [];
  userId: any;
  planCheck: boolean;
  constructor(
    private _userServ: UserService,
    private _globalServ: GlobalService,
    private actRout: ActivatedRoute
  ) {
    this.actRout.queryParams.subscribe(data=>{
      console.log(data.id);
      if(data.id){
        this.userId = data.id
      }
      this.getProfile();
      this.getPlans();
    })
    
  }

  ngOnInit() {
  }

  getPlans() {
    if(this.userId === undefined){
      this._userServ.getAppPlans(this._globalServ.idUser).subscribe((data: any) => {
        this.plans = data.plans;
        console.log('user plans: ', this.plans);
      }, err => { 
        console.log('err user plans: ', err);
      });
    }else{
      this._userServ.getAppPlans(this.userId).subscribe((data: any) => {
        this.plans = data.plans;
        this.planCheck = this.plans.length ? true : false;
        console.log('user plans: ', this.plans);
      }, err => { 
        console.log('err user plans: ', err);
      });
    }
    
  }

  getProfile() {
    if(this.userId === undefined){
      this._userServ.getProfile(this._globalServ.idUser, { 'entity': this._globalServ.org })
      .subscribe(dataProf => {
        this.firstName = get(dataProf, 'user.first_name', '');
      }, error => { console.log(error); })
    }else{
      this._userServ.getProfile(this.userId, { 'entity': this._globalServ.org })
      .subscribe(dataProf => {
          var name = get(dataProf, 'user.name', '');
          var firstName = name.split(' ');
        this.firstName = firstName[0];

      }, error => { console.log(error); })
    }
  }

}
