import { Component, OnInit } from '@angular/core';
import { ActivatedRoute , Router, NavigationExtras,} from '@angular/router';
@Component({
  selector: 'app-group-all',
  templateUrl: './group-all.page.html',
  styleUrls: ['./group-all.page.scss'],
})
export class GroupAllPage implements OnInit {
    groups = [];
  scrollPos: any = 0;
  title: any; 
  constructor(
    private router: Router,
    private navParam: ActivatedRoute
  ) { }

  ngOnInit() {
    this.navParam.queryParams.subscribe(data=>{
      console.log(data);
      this.groups = JSON.parse(data.groups)
      this.title = data.title
      console.log(this.groups);
    })
  }

  goToGroupDetail(value) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'id': value.id })
      },
    };
    this.router.navigate(['group-detail'], navigationExtras)
  }

  logScrolling(ev) {
    this.scrollPos = ev.detail.scrollTop;
  }

  logScrollStart() {

  }

  logScrollEnd() {

  }

  showFixedHeader() {
    if (this.scrollPos > 50) {
      return true;
    }
    return false;
  }


}
