import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { PromoCodeSuccessPage } from './promo-code-success.page';
import { ComponentModule } from '../../components/component.module';


const routes: Routes = [
{
  path: '',
  component: PromoCodeSuccessPage
}
];

@NgModule({
  imports: [
  CommonModule,
  FormsModule,
  ComponentModule,
  IonicModule,
  RouterModule.forChild(routes)
  ],
  declarations: [PromoCodeSuccessPage]
})
export class PromoCodeSuccessPageModule {}
