import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';



@Component({
	selector: 'app-promo-code-success',
	templateUrl: './promo-code-success.page.html',
	styleUrls: ['./promo-code-success.page.scss'],
})
export class PromoCodeSuccessPage implements OnInit {

	constructor(private translate: TranslateService,private router: Router) { }

	ngOnInit() {
	}

	continue() {
		this.router.navigate(['tabs/plan-list']);
	}

}
