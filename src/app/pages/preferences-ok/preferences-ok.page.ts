import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { GlobalService } from 'src/app/services/global.service';

@Component({
  selector: 'app-preferences-ok',
  templateUrl: './preferences-ok.page.html',
  styleUrls: ['./preferences-ok.page.scss'],
})
export class PreferencesOkPage implements OnInit {

  constructor(
    private router: Router,
    private _globalServ: GlobalService
  ) { }

  ngOnInit() {
  }

  goToPlans() {
    this._globalServ.currentPreferences = undefined;
    let index = window.history.length;
    window.history.go(-index);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'lauch':true}),
       
      }, replaceUrl: true
    };
    if (this._globalServ.timesLogin <= 1) this.router.navigate(['promo-code'], navigationExtras)
    else this.router.navigate(['tabs/plan-list'], navigationExtras)
  }


}
