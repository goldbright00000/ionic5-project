import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GainVoucherPage } from './gain-voucher.page';
import { ComponentModule } from 'src/app/components/component.module';

const routes: Routes = [
{
  path: '',
  component: GainVoucherPage
}
];

@NgModule({
  imports: [
  CommonModule,
  FormsModule,
  ComponentModule,
  IonicModule,
  RouterModule.forChild(routes)
  ],
  declarations: [GainVoucherPage]
})
export class GainVoucherPageModule {}
