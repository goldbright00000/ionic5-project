import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GainVoucherPage } from './gain-voucher.page';

describe('GainVoucherPage', () => {
  let component: GainVoucherPage;
  let fixture: ComponentFixture<GainVoucherPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GainVoucherPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GainVoucherPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
