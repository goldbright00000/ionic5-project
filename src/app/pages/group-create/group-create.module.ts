import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GroupCreatePage } from './group-create.page';

import { ComponentModule } from '../../components/component.module';
import { GurdGuard } from 'src/app/service/gurd/gurd.guard';

const routes: Routes = [
  {
    path: '',
    component: GroupCreatePage,
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentModule,
    IonicModule,
    RouterModule.forChild(routes),
   
  ],
  declarations: [GroupCreatePage],
})
export class GroupCreatePageModule {}
