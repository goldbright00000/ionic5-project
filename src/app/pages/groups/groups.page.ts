import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { GroupService } from '../../services/group.service';
import { environment } from '../../../environments/environment';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';

import { get, filter } from 'lodash';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.page.html',
  styleUrls: ['./groups.page.scss'],
})
export class GroupsPage implements OnInit {

  assetsBaseUrl: any = environment.apiUrl + '/get-ceu-club-image/';
  allGroups: any = [];
  allActiveGroups: any = [];
  common: any = [];
  combineGroups: any = [];

  recommendedGroups: any = [];

  scrollPos: any = 0;

  isViewAllForGroups: any = false;
  isViewAllForRecommend: any = false;

  constructor(
    public _groupServ: GroupService,
    public _globalServ: GlobalService,
    private router: Router,
    private actRoute: ActivatedRoute
  ) {
    // this.loadAllGroups();
    // this.loadRecommendedGroups();
    this.actRoute.params.subscribe(val => {
      this.loadAllGroups();
    })
  }

  ngOnInit() {
    // this.loadAllGroups();
  }

  ionViewWillEnter() {
    this.loadAllGroups();
    this.loadRecommendedGroups();
    setTimeout(() => {
      this.loadAllActiveGroups();
    }, 2000);
  }

  loadAllGroups() {
    this._groupServ.getGroupList().subscribe(data => {
      console.log('group list 1: ', data);
      this.allGroups = get(data, 'group', []);

    }, err => {
      console.log('get group err: ', err);
    });
  }


  loadAllActiveGroups() {
    this._groupServ.getAllActiveGroupList().subscribe(data => {
      console.log('group list: ', data);
      this.allActiveGroups = get(data, 'group', []);
      this.common = [...this.allGroups, ...this.recommendedGroups]
      console.log(this.common)
      this.common.forEach(element => {
        const index = this.allActiveGroups.findIndex(order => element.id === order.id);
        this.allActiveGroups.splice(index, 1);
      });

    }, err => {
      console.log('get group err: ', err);
    });
  }


  loadRecommendedGroups() {
    this._groupServ.getRecommendedGroupList().subscribe(data => {
      console.log('recommendedGroups list: ', data);
      this.recommendedGroups = get(data, 'group', []);
    }, err => {
      console.log('get recommendedGroups err: ', err);
    });
  }

  showFixedHeader() {
    if (this.scrollPos > 50) {
      return true;
    }
    return false;
  }

  logScrolling(ev) {
    this.scrollPos = ev.detail.scrollTop;
  }

  logScrollStart() {

  }

  logScrollEnd() {

  }

  goToGroupDetail(value) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'id': value.id })
      },
    };
    this.router.navigate(['group-detail'], navigationExtras)
  }

  getNumberOfLimitForGroups(type) {
    if (type === 'groups') {
      return this.isViewAllForGroups ? this.allGroups.length : 9;
    } else {
      return this.isViewAllForRecommend ? this.recommendedGroups.length : 3;
    }
  }

  viewAllGroup(type) {
    if (type == 'recommended') {
      let navParam: NavigationExtras = {
        queryParams: {
          groups: JSON.stringify(this.recommendedGroups),
          title: 'groups.recommendedGroups'
        }
      }
      this.router.navigate(['/group-all'], navParam)
    } else if (type == 'explore') {
      let navParam: NavigationExtras = {
        queryParams: {
          groups: JSON.stringify(this.allActiveGroups),
          title: 'groups.explore'
        }
      }
      this.router.navigate(['/group-all'], navParam)
    } else {
      let navParam: NavigationExtras = {
        queryParams: {
          groups: JSON.stringify(this.allGroups),
          title: 'groups.myGroups'
        }
      }
      this.router.navigate(['/group-all'], navParam)
    }
  }

}
