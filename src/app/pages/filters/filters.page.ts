import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { FilterService } from '../../services/filter.service';
import { GlobalService } from '../../services/global.service';
import { DatePipe } from '@angular/common'
import { PlanService } from '../../services/plan.service';
import { CityGuideService } from '../../services/city-guide.service';
import { CityAgendaService } from '../../services/city-agenda.service';
import { OfferDiscountService } from '../../services/offer-discount.service';
import { Location } from '@angular/common';
import * as moment from 'moment';

@Component({
  selector: 'filters',
  templateUrl: './filters.page.html',
  styleUrls: ['./filters.page.scss'],
  providers: [DatePipe]
})
export class FiltersPage implements OnInit {

  getFilters: any;
  listSortBy: any;
  listTypeOfFeed: any;
  listDate: any;
  listCity: any;
  idFiltersPost: any[] = [];
  getFiltersByUser: any;
  customDataValue: any;
  day: any;
  month: any;
  year: any;
  city_guide: any;
  city_guide_agenda: any;
  cat_id: any;

  mydate;
  blank: boolean;

  constructor(
    private router: Router,
    public datePicker: DatePicker,
    public _filterServ: FilterService,
    public _globalServ: GlobalService,
    public datepipe: DatePipe,
    private _planServ: PlanService,
    private _cityGuideServ: CityGuideService,
    private _cityAgendaServ: CityAgendaService,
    private _offerDiscountServ: OfferDiscountService,
    private location: Location,
    private actRoute: ActivatedRoute,

  ) {

  }

  ngOnInit() {
    console.log("get data")
    this.blank = true;

    this.city_guide = 0;
    if (this.actRoute.snapshot.queryParams.data != null) {
      let getParams = JSON.parse(this.actRoute.snapshot.queryParams.data);
      this.city_guide = getParams.city_guide;
      this._globalServ.cityagenda = getParams.city_guide;
      // this.city_guide = getParams.city_guide_agenda;
      console.log("city guide =>", this.city_guide);
      if (getParams.hasOwnProperty('cat_id')) {
        this.cat_id = getParams.cat_id;
        console.log(this.cat_id);
      }
      console.log("ccvcvccccvvgf", getParams);
    }

    this._filterServ.getFilters().subscribe(data => {
      this.getFilters = data;
      console.log("this.getFilters", this.getFilters);
      this._filterServ.getFiltersByUser(this._globalServ.idUser).
        subscribe(data => {
          this.getFiltersByUser = data;
          console.log(this.getFiltersByUser);
          for (let itemOne of this.getFilters.filters) {
            if (this.getFiltersByUser.user_filters != null) {
              for (let itemTwo of this.getFiltersByUser.user_filters) {
                if (itemOne.id == itemTwo.fk_filter_id) {
                  itemOne.checked = true;
                  this.idFiltersPost.push(itemOne.id);
                  if (itemOne.option_en == 'Custom date') {
                    this.mydate = itemTwo.custom_date ? moment(itemTwo.custom_date).format("MMM D YYYY") : this.closeChip();
                    console.log('-->', this.mydate);
                  }
                }
              }
            }
          }
        })
      this.listSortBy = this.getFilters.filters.filter(s => s.sub_type_en == 'Sort by');
      this.listTypeOfFeed = this.getFilters.filters.filter(s => s.sub_type_en == 'Type of feed');
      this.listDate = this.getFilters.filters.filter(s => s.sub_type_en == 'Date');
      this.listCity = this.getFilters.filters.filter(s => s.sub_type_en == 'City');
    }, error => { console.log(error); })
  }

  goToPreferences() {
    if (this.idFiltersPost.includes(4)) {
      var saveBoolean = false;
    } else {
      var saveBoolean = true;
    }
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'save': false, 'cat_id': this.cat_id, 'ispref': true })
      }
    };

    this.router.navigate(['preferences'], navigationExtras)
  }

  customDate() {
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
    }).then(
      date => {
        console.log('Got date: ', date);
        this.customDataValue = date;
        // debugger;
        var dateDisply = new Date(this.customDataValue);
        this.day = dateDisply.getDate();
        var monthh = new Array();
        monthh[0] = "January";
        monthh[1] = "February";
        monthh[2] = "March";
        monthh[3] = "April";
        monthh[4] = "May";
        monthh[5] = "June";
        monthh[6] = "July";
        monthh[7] = "August";
        monthh[8] = "September";
        monthh[9] = "October";
        monthh[10] = "November";
        monthh[11] = "December";
        this.month = monthh[dateDisply.getMonth()];
        this.year = dateDisply.getFullYear();
      },
      err => console.log('Error occurred while getting date: ', err)
    ).finally(() => {
      if (this.customDataValue == null) {
        for (let item of this.listDate) {
          console.log(item);
          if (item.option_en == 'Custom date') {
            item.checked = null;
            let getIndex = this.idFiltersPost.indexOf(item.id);
            this.idFiltersPost.splice(getIndex, 1);
            return;
          }
        }
      }
    });
  }

  //SortBy
  activeSortBy(value: any) {
    console.log('--------------------------------');
    console.log(value);
    for (let item of this.listSortBy) {
      if (item.id != value.id) {
        item.checked = null;
        let getIndex = this.idFiltersPost.indexOf(item.id);
        if (getIndex != -1) {
          this.idFiltersPost.splice(getIndex, 1);
        }
      }
    }
    for (let item of this.listSortBy) {
      if (item.id == value.id) {
        item.checked = true;
        this.idFiltersPost.push(item.id);
      }
    }
    console.log(this.idFiltersPost);
  }

  desactiveSortBy(value: any) {
    for (let item of this.listSortBy) {
      if (item.id == value.id) {
        item.checked = null;
        let getIndex = this.idFiltersPost.indexOf(value.id);
        this.idFiltersPost.splice(getIndex, 1);
        console.log('-->', this.idFiltersPost);
        return;
      }
    }
  }

  //typeOfFeed
  activeTypeOfFeed(value: any) {
    console.log('--------------------------------');
    console.log(value);
    for (let item of this.listTypeOfFeed) {
      if (item.id != value.id) {
        item.checked = null;
        let getIndex = this.idFiltersPost.indexOf(item.id);
        if (getIndex != -1) {
          this.idFiltersPost.splice(getIndex, 1);
        }
      }
    }
    for (let item of this.listTypeOfFeed) {
      if (item.id == value.id) {
        item.checked = true;
        this.idFiltersPost.push(item.id);
      }
    }
    console.log(this.idFiltersPost);
  }

  desactiveTypeOfFeed(value: any) {
    for (let item of this.listTypeOfFeed) {
      if (item.id == value.id) {
        item.checked = null;
        let getIndex = this.idFiltersPost.indexOf(value.id);
        this.idFiltersPost.splice(getIndex, 1);
        console.log('-->', this.idFiltersPost);
        return;
      }
    }
  }


  //Date
  activeDate(value: any) {
    console.log('--------------------------------');
    console.log("?????", value);
    console.log("LIST DATE", this.listDate);

    for (let item of this.listDate) {
      if (item.id != value.id) {
        item.checked = null;
        let getIndex = this.idFiltersPost.indexOf(item.id);
        if (getIndex != -1) {
          this.idFiltersPost.splice(getIndex, 1);
        }
      }
    }

    for (let item of this.listDate) {
      // this.customDataValue = true;
      if (item.id == value.id) {
        item.checked = true;
        if (item.option_en == 'Custom date') {
          console.log("IF");
          // this.customDataValue = true;

          let m = this.idFiltersPost.indexOf(item.id);
          if (m > -1) {
            let getIndex = this.idFiltersPost.indexOf(item.id);
            this.idFiltersPost.splice(getIndex, 1);
            console.log('-->', this.idFiltersPost);
          } else {
            this.idFiltersPost.push(item.id);
          }
        }
        else {
          this.idFiltersPost.push(item.id);
        }
      }
    }
    console.log(this.idFiltersPost);
  }

  desactiveDate(value: any) {
    console.log("///", value);
    for (let item of this.listDate) {
      if (item.id == value.id) {
        item.checked = null;
        let getIndex = this.idFiltersPost.indexOf(value.id);
        this.idFiltersPost.splice(getIndex, 1);
        console.log('-->', this.idFiltersPost);
        return;
      }

    }

  }

  //city
  activeCity(value: any) {
    console.log('--------------------------------');
    console.log(value);
    for (let item of this.listCity) {
      if (item.id != value.id) {
        item.checked = null;
        let getIndex = this.idFiltersPost.indexOf(item.id);
        if (getIndex != -1) {
          this.idFiltersPost.splice(getIndex, 1);
        }
      }
    }
    for (let item of this.listCity) {
      if (item.id == value.id) {
        item.checked = true;
        this.idFiltersPost.push(item.id);
      }
    }
    console.log(this.idFiltersPost);
  }

  desactiveCity(value: any) {
    for (let item of this.listCity) {
      if (item.id == value.id) {
        item.checked = null;
        let getIndex = this.idFiltersPost.indexOf(value.id);
        this.idFiltersPost.splice(getIndex, 1);
        console.log('-->', this.idFiltersPost);
        return;
      }
    }
  }

  saveFilters() {
    let filtersArray = null;
    let customDate = this.mydate;
    if (this.idFiltersPost != null) {
      filtersArray = this.idFiltersPost.toString();
      this._globalServ.filtersArry = filtersArray;
      console.log(this._globalServ.filtersArry)
    }
    let data;
    console.log("Custom_date: ", this.mydate);
    if (this.mydate) {
      customDate = this.datepipe.transform(this.mydate, 'yyyy-MM-dd');//this.customDataValue.toString();
      data = { 'id': this._globalServ.idUser, 'filterIds': filtersArray, 'customdate': customDate }
    } else {
      data = { 'id': this._globalServ.idUser, 'filterIds': filtersArray }
    }
    console.log(data);
    this._filterServ.saveFilters(this._globalServ.idUser, data)
      .subscribe(data => {
        data;
        console.log("filter saved =>", data);
        this.reload();
         this.location.back();
      }, error => {
        console.log(error);
         this.location.back();
      })
   
  }

  resetFilter() {
    this._filterServ.saveFilters(this._globalServ.idUser, { 'id': this._globalServ.idUser, 'filterIds': '4,7,10,16', 'customdate': null })
    .subscribe(data => {
        console.log(data);
        this.ngOnInit();
        this.reload();
        this.mydate = undefined;
      }, error => {
        console.log(error);
      })
    // this.location.back();
  }

  reload() {
    this._planServ.getDataPlanList();
    this._cityGuideServ.getDataCityGuideList();
    this._cityAgendaServ.getDataCityAgendaList();
    this._offerDiscountServ.getDataOfferDiscountList();
  }

  closeChip() {
    console.log("CLOSE");
    // this.customDataValue = false;
    // this.resetdate(10);
    this.mydate = undefined;
    this.resetdate(10);
  }

  resetdate(id) {
    for (let item of this.listDate) {
      if (item.id != id) {
        item.checked = null;
      }
      else {
        item.checked = true;
        this.idFiltersPost.push(item.id);
        console.log('----->', this.idFiltersPost);
      }
    }
    let getIndex = this.idFiltersPost.indexOf(21);
    this.idFiltersPost.splice(getIndex, 1);
    console.log('-->', this.idFiltersPost);
  }




}
