import { Component, ViewChild, OnInit, NgZone } from '@angular/core';
import { IonSlides, LoadingController, ToastController } from '@ionic/angular';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MSAdal, AuthenticationContext, AuthenticationResult } from '@ionic-native/ms-adal/ngx';
import { Location } from '@angular/common';
import { Storage } from '@ionic/storage';
import { UserService } from '../../services/user.service';
import { GlobalService } from '../../services/global.service';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { TranslateService } from '@ngx-translate/core';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.page.html',
  styleUrls: ['./tutorial.page.scss'],
})
export class TutorialPage implements OnInit {
  @ViewChild('slides') slides: IonSlides;
  last: boolean = false;
  pager: boolean = true;
  myForm: FormGroup;
  getDataInternalLogin: any;
  getDataInternalRegister: any;
  nonExistentUser: boolean;
  getDataLanguage: any;
  loading: HTMLIonLoadingElement;
  constructor(fb: FormBuilder,
    private msAdal: MSAdal,
    private location: Location,
    private storage: Storage,
    private _userServ: UserService,
    private _globalServ: GlobalService,
    private router: Router,
    private toastController: ToastController,
    public loadingController: LoadingController,
    private splashScreen: SplashScreen,
    private translate: TranslateService,
    private ga: GoogleAnalytics,
    private zone: NgZone) { }

  ngOnInit() {
  }
  next() {
    this.slides.slideNext();
    this.slides.isEnd().then(res => {
      if (res == true) {
        this.last = true;
        this.pager = false;
      }
    })
  }
  skip() {
    this.router.navigate(['login']);
  }
  check() {
    this.slides.isEnd().then(res => {
      if (res == true) {
        this.last = true;
        this.pager = false;
      }
    })
  }
  login() {
    this.router.navigate(['login']);
  }

  async loginOffice() {
    await this.presentLoadingWithOptions();
    //******* ceu production *******
    //f9eb8c3b-e8d4-4e8c-9351-195297be3601
    //msalf9eb8c3b-e8d4-4e8c-9351-195297be3601://auth
    //******* ie production *******
    //df76dcce-035b-46c7-a680-2cb89d36b6d1
    //msaldf76dcce-035b-46c7-a680-2cb89d36b6d1://auth
    let clientId;
    let redirectUrl;
    if (this._globalServ.org == 'CEU') {
      clientId = 'f9eb8c3b-e8d4-4e8c-9351-195297be3601';
      redirectUrl = 'msalf9eb8c3b-e8d4-4e8c-9351-195297be3601://auth';
    }
    if (this._globalServ.org == 'IE') {
      clientId = 'df76dcce-035b-46c7-a680-2cb89d36b6d1';
      // redirectUrl='msalf9eb8c3b-e8d4-4e8c-9351-195297be3601://auth';
      redirectUrl = 'msaldf76dcce-035b-46c7-a680-2cb89d36b6d1://auth';
    }
    let authContext: AuthenticationContext = this.msAdal.createAuthenticationContext('https://login.windows.net/common');

    await authContext.acquireTokenAsync('https://graph.windows.net', clientId, redirectUrl, null, null)
      .then(async (authResponse: AuthenticationResult) => {

        //365 all data
        console.log(authResponse);
        //login internal system.
        await this._userServ.login({ 'email': authResponse.userInfo.uniqueId, 'entity': this._globalServ.org }).subscribe(async data => {
          this.getDataInternalLogin = data;
          console.log(this.getDataInternalLogin);
          this.storage.set('token', this.getDataInternalLogin.token);
          this.storage.set('idUser', this.getDataInternalLogin.user.id);
          this._globalServ.idUser = this.getDataInternalLogin.user.id;
          this.ga.setUserId(this._globalServ.idUser.toString());
          //get language
          await this._userServ.getSettings(this._globalServ.idUser).subscribe(dataLan => {
            this.getDataLanguage = dataLan;
            this._globalServ.language = this.getDataLanguage.user_settings[0].value;
            this.storage.set('language', this._globalServ.language);
            this.translate.setDefaultLang(this._globalServ.language);
          }, error => {
            console.log(error);
          })
          //redirect
          this._globalServ.timesLogin = this.getDataInternalLogin.user.login_times;
          if (this.getDataInternalLogin.user.login_times <= 1) {
            let navigationExtras: NavigationExtras = {
              queryParams: {
                data: JSON.stringify({ 'profile': true/*'userName': this.getDataInternalLogin.user.email, 'lastName': this.getDataInternalLogin.user.last_name, 'firstName': this.getDataInternalLogin.user.first_name, 'firstTime': true*/ })
              }, replaceUrl: true
            };
            this.loading.dismiss()
            this.router.navigate(['profile-first'], navigationExtras)
          } else {
            let navigationExtras: NavigationExtras = {
              queryParams: {
                data: JSON.stringify({})
              }, replaceUrl: true
            };
            this.loading.dismiss();
            this.router.navigate(['tabs/plan-list'], navigationExtras)
          }
        }, async error => {
          console.log(error);
          if (error.error.message = "El usuario no existe.") {
            await this._userServ.register({ 'email': authResponse.userInfo.uniqueId, 'entity': this._globalServ.org }).subscribe(data => {
              this.getDataInternalRegister = data;
              this.storage.set('token', this.getDataInternalRegister.token);
              this.storage.set('idUser', this.getDataInternalRegister.user.id);
              this._globalServ.idUser = this.getDataInternalRegister.user.id;
              let navigationExtras: NavigationExtras = {
                queryParams: {
                  data: JSON.stringify({ 'profile': true, 'userName': this.getDataInternalRegister.user.email })
                }, replaceUrl: true
              };
              this.loading.dismiss();
              this.router.navigate(['profile-first'], navigationExtras)
            });
          }
        })
      }, error => {

        this.loading.dismiss();
        this.showAlert("Authentication failed")
        console.log(error)
      })
      .catch((e: any) => {

        this.loading.dismiss();
        this.showAlert("Authentication failed")
        console.log('Authentication failed', e)

      }
      );
  }

  async loginIE() {
    await this.presentLoadingWithOptions();

    // Uncomment on iOS release
    let browserRef = window.open('https://vistingo.es/api/login/ie', '_blank', 'location=no,toolbar=no'); //clearcache=yes,clearsessioncache=yes,
    await browserRef.addEventListener("loadstart", async (event:any) => {
    // Uncomment on Android release
    // let browserRef = window.open('https://vistingo.es/api/login/ie', 'blank', 'location=no,toolbar=no');
    // await browserRef.addEventListener("loadstop", async (event:any) => {
      console.log('URL: ', event.url);
      if((event.url).indexOf('auth') !== -1 && (event.url).indexOf('email') !== -1) {
        let auth_token = event.url.split('/')[4];
        let auth_email = event.url.split('?')[1].replace('email=','');
        console.log('Token: ', auth_token);
        console.log('Email: ', auth_email);

        browserRef.close(); 
        if(auth_token && auth_email) {
          await this._userServ.login({ 'email': auth_email, 'entity': this._globalServ.org }).subscribe(async data => {
            this.getDataInternalLogin = data;
            console.log(this.getDataInternalLogin);
            this.storage.set('token', this.getDataInternalLogin.token);
            this.storage.set('idUser', this.getDataInternalLogin.user.id);
            this._globalServ.idUser = this.getDataInternalLogin.user.id;
            this.ga.setUserId(this._globalServ.idUser.toString());
            //get language
            await this._userServ.getSettings(this._globalServ.idUser).subscribe(dataLan => {
              this.getDataLanguage = dataLan;
              this._globalServ.language = this.getDataLanguage.user_settings[0].value;
              this.storage.set('language', this._globalServ.language);
              this.translate.setDefaultLang(this._globalServ.language);

              //redirect
              console.log('Login Times 123: ', this.getDataInternalLogin.user.login_times);
              this._globalServ.timesLogin = this.getDataInternalLogin.user.login_times;
              if (!this.getDataInternalLogin.user.login_times || this.getDataInternalLogin.user.login_times <= 1) {
                let navigationExtras: NavigationExtras = {
                  queryParams: {
                    data: JSON.stringify({ 'profile': true, 'userName': this.getDataInternalLogin.user.email, 'lastName': this.getDataInternalLogin.user.last_name, 'firstName': this.getDataInternalLogin.user.first_name, 'firstTime': true })
                  }, replaceUrl: true
                };
                this.loading.dismiss();
                console.log('Navigation Extras', navigationExtras);
                this.zone.run(() => {
                  this.router.navigate(['profile-first'], navigationExtras);
                })
              } else {
                let navigationExtras: NavigationExtras = {
                  queryParams: {
                    data: JSON.stringify({})
                  }, replaceUrl: true
                };
                this.loading.dismiss();
                this.router.navigate(['tabs/plan-list'], navigationExtras)
              }
            }, error => {
              console.log(error);
            });
          }, async (error) => {
            console.log(error);
            if (error.error.message = "El usuario no existe.") {
              this._userServ.register({ 'email': auth_email, 'entity': this._globalServ.org }).subscribe(data => {
                this.getDataInternalRegister = data;
                this.storage.set('token', this.getDataInternalRegister.token);
                this.storage.set('idUser', this.getDataInternalRegister.user.id);
                this._globalServ.idUser = this.getDataInternalRegister.user.id;
                let navigationExtras: NavigationExtras = {
                  queryParams: {
                    data: JSON.stringify({ 'profile': true, 'userName': this.getDataInternalRegister.user.email })
                  }, replaceUrl: true
                };
                this.loading.dismiss();
                this.router.navigate(['profile-first'], navigationExtras);
              });
            }
          })
        } else {
          this.loading.dismiss();
          this.showAlert("Authentication failed")
          console.log("Authentication failed")
        }
      }
    });
  }

  async presentLoadingWithOptions() {
    this.loading = await this.loadingController.create({

      message: 'Logging in...',

      cssClass: 'my-loading-class'
    });
    return await this.loading.present();
  }
  onDirectLogin() {
    console.log(this.getDataInternalLogin);
    this.storage.set('token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6NDQsIm5hbWUiOm51bGwsImVtYWlsIjoidmlzdGluZ29hbHVtbmlAYWx1bW5pLmllLmVkdSIsInJvbGUiOiJFIiwiaWF0IjoxNTY2NTM4OTAxLCJleHAiOjE1NjkyMTczMDF9.V704aTCUjTqYsAgiA_Z1pAQinOrRoHh9AFr259911c0');
    this.storage.set('idUser', '45');
    this._globalServ.idUser = '45';
    this.ga.setUserId(this._globalServ.idUser.toString());
    //get language
    this._userServ.getSettings(this._globalServ.idUser).subscribe(dataLan => {
      this.getDataLanguage = dataLan;
      this._globalServ.language = this.getDataLanguage.user_settings[0].value;
      this.storage.set('language', this._globalServ.language);
      this.translate.setDefaultLang(this._globalServ.language);
    }, error => {
      console.log(error);
    })


    // let navigationExtras: NavigationExtras = {
    //   queryParams: {
    //     data: JSON.stringify({ 'profile': true/*'userName': this.getDataInternalLogin.user.email, 'lastName': this.getDataInternalLogin.user.last_name, 'firstName': this.getDataInternalLogin.user.first_name, 'firstTime': true*/ })
    //   }, replaceUrl: true
    // };
    // this._globalServ.timesLogin = 0;

    // this.router.navigate(['profile-first'], navigationExtras)
let navigationExtras: NavigationExtras = {
              queryParams: {
                data: JSON.stringify({})
              }, replaceUrl: true
            };
            this.router.navigate(['tabs/plan-list'], navigationExtras)

  }
  async showAlert(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 3000
    });
    toast.present();
  }
}
