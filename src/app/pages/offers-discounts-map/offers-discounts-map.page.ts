import { Component, OnInit, ViewChild } from '@angular/core';
import { } from 'googlemaps';
import { Platform, NavController } from '@ionic/angular';
import { GlobalService } from '../../services/global.service';
import { OfferDiscountService } from '../../services/offer-discount.service';
import { Router, NavigationExtras } from '@angular/router';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { AlertController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ModalController } from '@ionic/angular';
// import { OffersDiscountsCodePage } from '../offers-discounts-code/offers-discounts-code.page';
import { Location } from '@angular/common';
import { NotificationService } from '../../services/notification.service';
import * as moment from 'moment';

@Component({
  selector: 'app-offers-discounts-map',
  templateUrl: './offers-discounts-map.page.html',
  styleUrls: ['./offers-discounts-map.page.scss'],
})
export class OffersDiscountsMapPage implements OnInit {

  latitudeInit;
  longitudeInit;
  zoom: any = 14;
  distanceTop: any = 0;
  code: any;
  map: any;
  listOD: any;
  markerUser: google.maps.Marker;
  positionsPermission: boolean;
  mapOptions: google.maps.MapOptions;
  state: any = 0;

  @ViewChild("map") mapElement;

  constructor(
    public platform: Platform,
    public _globalServ: GlobalService,
    private _offerDiscountServ: OfferDiscountService,
    private router: Router,
    private launchNavigator: LaunchNavigator,
    private alertController: AlertController,
    private geolocation: Geolocation,
    public location: Location,
    public modalController: ModalController,
    private _notificationServ: NotificationService,
    private navCtrl: NavController
  ) {
    var options = {
      enableHighAccuracy: true,
      timeout: 725000,
      maximumAge: 0
    };
    navigator.geolocation.getCurrentPosition(function (position) {
      console.log('*********', position);
      (<HTMLInputElement>document.getElementById('latInit')).value = String(position.coords.latitude);
      (<HTMLInputElement>document.getElementById('longInit')).value = String(position.coords.longitude);
    });

    if (this._globalServ.iosDivice) {
      this.distanceTop = platform.height() / 1.5;
    } else {
      if (platform.height() < 800) {
        this.distanceTop = 400;
      }
      if (platform.height() > 800) {
        this.distanceTop = 900;
      }
    }
  }

  ngOnInit() {
    document.getElementById("drawer-content").style.display = 'none';
    // document.getElementById("drawer").style.transform = 'unset';
    document.getElementById("drawer").style.display = 'none';


    // document.getElementById("drawer").style.transform = 'translateY(550px)';

    document.getElementById("fab").style.transform = 'translateY(-50px)';

    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitudeInit = resp.coords.latitude;
      this.longitudeInit = resp.coords.longitude;

    }).catch((error) => {
      console.log('Error getting location', error);
    });

    console.log("INIT");
    document.getElementById("drawer-content").style.display = 'none';

    this._offerDiscountServ.getListFiltered(this._globalServ.idUser).subscribe(data => {
      this.listOD = data;
      //set map
      let coords = new google.maps.LatLng(this.latitudeInit, this.longitudeInit);
      this.mapOptions = {
        center: coords,
        zoom: this.zoom,
        maxZoom: 17,
        minZoom: 3,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        disableDefaultUI: true,
        styles: [
          {
            "featureType": "landscape",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "poi",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "road",
            "elementType": "labels",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          },
          {
            "featureType": "transit",
            "stylers": [
              {
                "visibility": "off"
              }
            ]
          }
        ]
      }

      let iconPath = {
        url: "assets/img/marker_percent.png",
        scaledSize: new google.maps.Size(40, 50),
      };

      this.map = new google.maps.Map(this.mapElement.nativeElement, this.mapOptions);

      // var userOuter = new google.maps.Circle({
      //   strokeColor: '#004d7e',
      //   strokeOpacity: 0.8,
      //   strokeWeight: 0.3,
      //   fillColor: '#004d7e',
      //   fillOpacity: 0.1,
      //   radius: 500,
      //   map: this.map,
      //   center: {lat: Number((<HTMLInputElement>document.getElementById('latInit')).value), lng: Number((<HTMLInputElement>document.getElementById('longInit')).value) },
      // });

      // var userInner = new google.maps.Circle({
      //   strokeColor: '#ffffff',
      //   strokeOpacity: 2,
      //   strokeWeight: 2,
      //   fillColor: '#004d7e',
      //   fillOpacity: 1,
      //   radius: 125,
      //   map: this.map,
      //   center: {lat: Number((<HTMLInputElement>document.getElementById('latInit')).value), lng: Number((<HTMLInputElement>document.getElementById('longInit')).value) },
      // });

      // this.markerUser = new google.maps.Marker({
      //   map: this.map,
      //   position: { lat: Number((<HTMLInputElement>document.getElementById('latInit')).value), lng: Number((<HTMLInputElement>document.getElementById('longInit')).value) },
      // });
      var marker = new google.maps.Marker({
        position: { lat: Number((<HTMLInputElement>document.getElementById('latInit')).value), lng: Number((<HTMLInputElement>document.getElementById('longInit')).value) },
        map: this.map,
        icon: 'assets/img/current-location.png'
      });
      marker.setMap(this.map);
      this.map.setCenter({ lat: Number((<HTMLInputElement>document.getElementById('latInit')).value), lng: Number((<HTMLInputElement>document.getElementById('longInit')).value) });


      for (let item of this.listOD.discounts) {
        var marker: google.maps.Marker = new google.maps.Marker({
          map: this.map,
          position: { lat: Number(item.latitude), lng: Number(item.longitude) },
          icon: iconPath
        });

        /* google.maps.event.addListener(marker, 'click', function() {
          console.log('clicked marker!1');
          }
          ); */
        let self = this;
        google.maps.event.addListener(this.map, 'click', (function (marker) {
          self.state = 0;
          document.getElementById("drawer-content").style.display = 'none';
          document.getElementById("drawer").style.display = 'none';
          document.getElementById("fab").style.transform = 'translateY(-50px)';
        }))
        console.log(this.state);

        google.maps.event.addListener(marker, 'click', (function (marker) {

          return function () {
            let iconPath = {
              url: "assets/img/marker_percent.png",
              scaledSize: new google.maps.Size(50, 60),
            };
            marker.setIcon(iconPath);
            console.log('clicked marker!2', marker);
            document.getElementById("drawer").style.display = 'block';
            document.getElementById("drawer-content").style.display = 'block';
            if (self.platform.is('ios')) {
              document.getElementById("fab").style.transform = 'translateY(-300px)';
            } else {
              document.getElementById("fab").style.transform = 'translateY(-250px)';
            }
            console.log("--------------", document.getElementById('fab'));

            let state: HTMLElement = document.getElementById('state') as HTMLElement;
            state.click();
            (<HTMLInputElement>document.getElementById('all')).value = JSON.stringify(item);
            document.getElementById('avatar').innerHTML = item.company;

            let title;
            console.log(item.title.length)
            if (item.title.length > 35) {
              title = item.title.slice(0, 30) + ' ...'
            } else {
              title = item.title
            }
            document.getElementById('title').innerHTML = title;

            document.getElementById('validUntil').innerHTML = 'Valid until ' + moment(item.validUntil).format('DD MMM YYYY')
            // new Date(item.validUntil).toString();
            document.getElementById('address').innerHTML = item.address;
            (<HTMLInputElement>document.getElementById('imgDesc')).src = item.image;
            (<HTMLInputElement>document.getElementById('imgProfile')).src = item.image;
          }
        })(marker));
      }

    });
  }

  ionViewDidEnter() {
    // this._offerDiscountServ.getListFiltered(this._globalServ.idUser).subscribe(data => {
    //   this.listOD = data;
    //   //set map
    //   let coords = new google.maps.LatLng(this.latitudeInit, this.longitudeInit);
    //   this.mapOptions = {
    //     center: coords,
    //     zoom: this.zoom,
    //     maxZoom: 17,
    //     minZoom: 3,
    //     mapTypeId: google.maps.MapTypeId.ROADMAP,
    //     disableDefaultUI: true,
    //     styles: [
    //     {
    //       "featureType": "landscape",
    //       "stylers": [
    //       {
    //         "visibility": "off"
    //       }
    //       ]
    //     },
    //     {
    //       "featureType": "poi",
    //       "stylers": [
    //       {
    //         "visibility": "off"
    //       }
    //       ]
    //     },
    //     {
    //       "featureType": "road",
    //       "elementType": "labels",
    //       "stylers": [
    //       {
    //         "visibility": "off"
    //       }
    //       ]
    //     },
    //     {
    //       "featureType": "transit",
    //       "stylers": [
    //       {
    //         "visibility": "off"
    //       }
    //       ]
    //     }
    //     ]
    //   }

    //   let iconPath = {
    //     url: "assets/img/marker_percent.png",
    //     scaledSize: new google.maps.Size(40, 50),
    //   };

    //   this.map = new google.maps.Map(this.mapElement.nativeElement, this.mapOptions);

    //   var userOuter = new google.maps.Circle({
    //     strokeColor: '#0000FF',
    //     strokeOpacity: 0.8,
    //     strokeWeight: 2,
    //     fillColor: '#0000FF',
    //     fillOpacity: 0.35,
    //     radius: 500,
    //     map: this.map,
    //     center: {lat: Number((<HTMLInputElement>document.getElementById('latInit')).value), lng: Number((<HTMLInputElement>document.getElementById('longInit')).value) },
    //   });

    //   var userInner = new google.maps.Circle({
    //     strokeColor: '#0000FF',
    //     strokeOpacity: 1,
    //     strokeWeight: 1,
    //     fillColor: '#0000FF',
    //     fillOpacity: 1,
    //     radius: 150,
    //     map: this.map,
    //     center: {lat: Number((<HTMLInputElement>document.getElementById('latInit')).value), lng: Number((<HTMLInputElement>document.getElementById('longInit')).value) },
    //   });

    //   this.markerUser = new google.maps.Marker({
    //     map: this.map,
    //     position: { lat: Number((<HTMLInputElement>document.getElementById('latInit')).value), lng: Number((<HTMLInputElement>document.getElementById('longInit')).value) },
    //   });
    //   this.map.setCenter({ lat: Number((<HTMLInputElement>document.getElementById('latInit')).value), lng: Number((<HTMLInputElement>document.getElementById('longInit')).value) });


    //   for (let item of this.listOD.discounts) {
    //     var marker: google.maps.Marker = new google.maps.Marker({
    //       map: this.map,
    //       position: { lat: Number(item.latitude), lng: Number(item.longitude) },
    //       icon: iconPath
    //     });

    //     /* google.maps.event.addListener(marker, 'click', function() {
    //       console.log('clicked marker!1');
    //       }
    //       ); */
    //       let self = this;
    //       google.maps.event.addListener(this.map, 'click', (function(marker){
    //         self.state = 0;
    //         document.getElementById("drawer-content").style.display = 'none';
    //         document.getElementById("drawer").style.transform = 'unset';
    //         document.getElementById("fab").style.transform = 'unset';
    //       }))
    //       console.log(this.state);

    //       google.maps.event.addListener(marker, 'click', (function (marker) {

    //         return function () {
    //           let iconPath = {
    //             url: "assets/img/marker_percent.png",
    //             scaledSize: new google.maps.Size(50, 60),
    //           };
    //           marker.setIcon(iconPath);
    //           console.log('clicked marker!2', marker);
    //           document.getElementById("drawer-content").style.display = 'block';
    //           if(self.platform.is('ios')){
    //             document.getElementById("fab").style.transform = 'translateY(-300px)';
    //           }else{
    //             document.getElementById("fab").style.transform = 'translateY(-250px)';
    //           }
    //           console.log("--------------",document.getElementById('fab'));

    //           let state: HTMLElement = document.getElementById('state') as HTMLElement;
    //           state.click();
    //           (<HTMLInputElement>document.getElementById('all')).value = JSON.stringify(item);
    //           document.getElementById('avatar').innerHTML = item.company;

    //           let title;
    //           console.log(item.title.length)
    //           if(item.title.length > 35) {
    //             title = item.title.slice(0,30) + ' ...' 
    //           }else{
    //             title = item.title
    //           }
    //           document.getElementById('title').innerHTML = title;

    //           document.getElementById('validUntil').innerHTML = 'Valid until ' + moment(item.validUntil).format('DD MMM YYYY') 
    //           // new Date(item.validUntil).toString();
    //           document.getElementById('address').innerHTML = itemthis.markerUser = new google.maps.Marker({
    //   map: this.map,
    //   position: { lat: Number((<HTMLInputElement>document.getElementById('latInit')).value), lng: Number((<HTMLInputElement>document.getElementById('longInit')).value) },
    // });
    // this.address;
    //           (<HTMLInputElement>document.getElementById('imgDesc')).src = item.image;
    //           (<HTMLInputElement>document.getElementById('imgProfile')).src = item.image;
    //         }
    //       })(marker));
    //     }

    //   });
  }

  async goToCode() {
    this.code = JSON.parse((<HTMLInputElement>document.getElementById('all')).value);
    // const modal = await this.modalController.create({
    //   component: OffersDiscountsCodePage,
    //   componentProps: {
    //     'code': this.code.discountCode
    //   }
    // });
    // return await modal.present();

    console.log("MMMMMM", this.code);
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'code': this.code })
      },
    };
    this.router.navigate(['/offers-discounts-code'], navigationExtras);
  }

  goToDirections() {
    let code = JSON.parse((<HTMLInputElement>document.getElementById('all')).value);
    let options: LaunchNavigatorOptions = {
      app: this.launchNavigator.APP.GOOGLE_MAPS
    }

    this.launchNavigator.navigate(code.address, options)
      .then(
        success => console.log('Launched navigator'),
        error => {
          console.log('Error launching navigator', error);
          this.messageMap();
        }
      );
  }

  goToOfferAndDiscount() {
    let code = JSON.parse((<HTMLInputElement>document.getElementById('all')).value);
    var discData;
    this._notificationServ.getOfferAnddiscount(code.id).subscribe(data => {
      discData = data;
      let navigationExtras: NavigationExtras = {
        queryParams: {
          data: JSON.stringify({ 'item': discData.discount })
        }
      };
      this.router.navigate(['/offers-discoutns-item'], navigationExtras);
    });
  }

  stateFunc() {
    this.state = 0;
    setTimeout(() => {
      this.state = 2;
    }, 500);
  }

  async messageMap() {
    let titleLabel;
    let message;
    let okLabel;
    if (this._globalServ.language == 'en') {
      titleLabel = 'Message';
      okLabel = 'Ok';
      message = 'Install or Activate Google maps';
    }
    if (this._globalServ.language == 'es') {
      titleLabel = 'mensage';
      okLabel = 'Aceptar';
      message = 'Instalar o Activar Google maps';
    }
    const alert = await this.alertController.create({
      header: titleLabel,
      message: message,
      buttons: [
        {
          text: okLabel,
          handler: () => {
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();
  }

  refreshLatLong() {
    this.ionViewDidEnter();
  }


  goBack() {
    this.navCtrl.back();
  }

  getMyLocation() {
    console.log("getMyLocation()");
    this.ngOnInit();

  }

}
