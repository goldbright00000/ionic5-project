import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {  RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ImageModalPage } from './image-modal.page';



@NgModule({
	declarations: [ImageModalPage],
	imports: [
		CommonModule,
		FormsModule,
		IonicModule,
	],
	entryComponents: [
       ImageModalPage
     ]
})
export class ImageModalPageModule { }
