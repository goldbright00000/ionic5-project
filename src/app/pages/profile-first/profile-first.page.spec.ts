import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileFirstPage } from './profile-first.page';

describe('ProfileFirstPage', () => {
  let component: ProfileFirstPage;
  let fixture: ComponentFixture<ProfileFirstPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileFirstPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileFirstPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
