import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router, NavigationExtras } from '@angular/router';
import { GlobalService } from '../../services/global.service';
import { UserService } from '../../services/user.service';
import { ModalController, ActionSheetController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, Platform, IonContent } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../../../environments/environment';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Crop } from '@ionic-native/crop/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { AppLauncher, AppLauncherOptions } from '@ionic-native/app-launcher/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
@Component({
  selector: 'app-profile-first',
  templateUrl: './profile-first.page.html',
  styleUrls: ['./profile-first.page.scss'],
})
export class ProfileFirstPage implements OnInit {
  userName: any;
  firstName: any;
  lastName: any;
  city: any;
  faculty: any;
  from: any;
  birthday: any;
  myForm: FormGroup;
  getDataProfile: any;
  profileBool: Boolean;
  settingBool: Boolean;
  image: any;
  url: any = environment.apiUrl;
  pageTitleName: any = false;
  pathImage: any;
  pathImageCropper: any;
  pathImagePost: any;
  public saved: boolean = false;
  @ViewChild('scrollContent') scroll: IonContent;
  connected: boolean;
  constructor(fb: FormBuilder,
    private actRoute: ActivatedRoute,
    private router: Router,
    public _globalServ: GlobalService,
    private _userServ: UserService,
    private storage: Storage,
    public modalController: ModalController,
    public alertController: AlertController,
    private translate: TranslateService,
    public actionSheetController: ActionSheetController,
    private camera: Camera,
    private sanitizer: DomSanitizer,
    private webview: WebView,
    private crop: Crop,
    private platform: Platform,
    private appLauncher: AppLauncher,
    public emailComposer: EmailComposer,
    private transfer: FileTransfer) {
    this.storage.get('instaConnected').then(res => {
      if (res == true) {
        this.connected = true;
      }
    })
    if (this.platform.is('android')) {
      window.addEventListener('keyboardWillShow', (event: any) => {
        console.log("keyboard show", event)
        document.getElementById('container').style.marginBottom = (90 + event.keyboardHeight) + 'px';
        this.scroll.scrollToPoint(0, event.path[0].document.activeElement.offsetParent.offsetParent.offsetParent.offsetParent.offsetTop, 500)
      })

      window.addEventListener('keyboardDidHide', (event: any) => {
        console.log("keyboard show", event)
        document.getElementById('container').style.marginBottom = 'unset';
      })
    }


    if (this.actRoute.snapshot.queryParams.data != null) {
      let getParams = JSON.parse(this.actRoute.snapshot.queryParams.data);
      this.profileBool = getParams.profile;
      this.settingBool = getParams.setting;
    }
    this.getDataProfileFunc();
    this.myForm = fb.group({
      'userName': ['', Validators.compose([])],
      'firstName': ['', Validators.compose([])],
      'lastName': ['', Validators.compose([])],
      'city': ['', Validators.compose([])],
      'faculty': ['', Validators.compose([])],
      'from': ['', Validators.compose([])],
      'birthday': ['', Validators.compose([])],
      'myForm': ['', Validators.compose([])],
    });

    if (this._globalServ.profileImagePath != null) {
      this.image = null;
    }
  }

  ngOnInit() {
  }

  gotoPromoCode() {
    this.router.navigate(['promo-code'], {});
  }

  logScrolling(e) {
    if (e.detail.scrollTop > 50) {
      this.pageTitleName = true;
    } else {
      this.pageTitleName = false;
    }
  }
  enterKey() {
    console.log("Enter key pressed");
  }
  save(value: any) {
    console.log("enter key =>", value);

    this.saved = true;
    if (this.birthday == '') { this.birthday = null; }
    this._userServ.saveProfile({
      'id': this._globalServ.idUser,
      'entity': this._globalServ.org,
      'firstname': this.firstName,
      'lastname': this.lastName,
      'city': this.city,
      'faculty': this.faculty,
      'from': this.from,
      'birthday': this.birthday,
    }).subscribe(data => {
      data;
      this._userServ.saveSettings({ 'id': this._globalServ.idUser, 'settings': 'language', 'value': this._globalServ.language })
        .subscribe(dataPref => {
          dataPref;
          this.storage.set('language', this._globalServ.language);
        }, error => { console.log(error); })
      if (this.profileBool == true) {
        let navigationExtras: NavigationExtras = {
          queryParams: {
            data: JSON.stringify({ 'showText': true, 'showTitle': true, 'save': true })
          }
        };
        console.log('Preference Navigation Extras', navigationExtras);
        this.router.navigate(['preferences'], navigationExtras)
      }
      if (this.settingBool == true) {
        let navigationExtras: NavigationExtras = {
          queryParams: {
            data: JSON.stringify({})
          }, replaceUrl: true
        };
        // this.router.navigate(['/tabs/more'], navigationExtras)
      }
    }, error => { console.log(error); });

  }

  async goToUpload() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'profile': true })
      }
    };
    // this.router.navigate(['image-cropper'], navigationExtras);
    let titleLabel;
    let cancelLabel;
    let EditLabel;
    let ChangeLabel;
    let RemoveLabel;
    if (this._globalServ.language == 'en') {
      titleLabel = 'Photo';
      cancelLabel = 'Cancel';
      EditLabel = 'Edit Photo';
      ChangeLabel = 'Change Photo';
      RemoveLabel = 'Remove Photo';
    }
    if (this._globalServ.language == 'es') {
      titleLabel = '';
      cancelLabel = 'Cancel';
      EditLabel = 'Edit Photo';
      ChangeLabel = 'Change Photo';
      RemoveLabel = 'Remove Photo'
    }
    const actionSheetForCancel = await this.actionSheetController.create({
      cssClass: 'actionsheet2',
      buttons: [{
        text: cancelLabel,
        handler: () => {
          actionSheetForPlan_group.dismiss()
        }
      }
      ]
    });


    const actionSheetForPlan_group = await this.actionSheetController.create({
      header: titleLabel,
      cssClass: 'actionSheetForPlan_group',



      buttons: [
        // 	{
        // 	text: EditLabel,

        // 	handler: async () => {
        // 		actionSheetForCancel.dismiss()
        // 		 this.cropFunc();



        // 	}
        // },
        {
          text: ChangeLabel,
          handler: () => {
            actionSheetForCancel.dismiss()
            this.selectFile();
          }
        },
        {
          text: RemoveLabel,
          handler: () => {
            this._globalServ.planImagePath = null;
            this.image = null;
            actionSheetForCancel.dismiss()

          }
        }
      ]
    });
    actionSheetForCancel.present();
    actionSheetForPlan_group.present();

    actionSheetForPlan_group.onDidDismiss().then(() => {
      actionSheetForCancel.dismiss();
    });
    actionSheetForCancel.onDidDismiss().then(() => {
      actionSheetForPlan_group.dismiss();
    });
  }
  selectFile() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: 2
    }

    this.camera.getPicture(options).then((imageData) => {
      this.pathImage = this.sanitizer.bypassSecurityTrustUrl(this.webview.convertFileSrc(imageData));
      // this.pathImage = this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + imageData);
      this.pathImageCropper = imageData;
      this.pathImagePost = imageData;

      this.cropFunc()

      console.log("Image path postt", this.pathImagePost);
    }, (err) => {
      console.log(err);
    });
  }
  cropFunc() {
    this.crop.crop(this.pathImageCropper, { quality: 75 })
      .then(
        newImage => {
          this.pathImage = this.sanitizer.bypassSecurityTrustUrl(this.webview.convertFileSrc(newImage));
          // this.pathImage = this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + newImage);
          this.pathImageCropper = newImage;
          this.pathImagePost = newImage;
          this.uploadProfile();
        },
        error => console.error('Error cropping image', error)
      );
  }

  uploadProfile() {
    // if (this.profileBool == true) {
    this._globalServ.profileImagePath = this.pathImage;
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({})
      }, replaceUrl: true
    };
    this.router.navigate(['tabs/profile'], navigationExtras);

    const fileTransfer: FileTransferObject = this.transfer.create();

    let filename = 'profileImage_' + Math.random() * 100000000000000000 + '.jpg';

    let options: FileUploadOptions = {
      fileKey: 'image',
      fileName: filename,
    }

    fileTransfer.upload(this.pathImagePost, environment.apiUrl + '/app-user-set-profile-image/' + this._globalServ.idUser, options)
      .then((data) => {
        // success
        console.log('success', data);
      }, (err) => {
        // error
        console.log('error', err);
      })
    // }
  }

  async openLanguageModal() {
    let titleLabel;
    let englisLabel;
    let spanishLabel;
    let cancelLabel;
    let okLabel;
    let englishChecked;
    let spanishChecked;
    if (this._globalServ.language == 'en') {
      titleLabel = 'Language';
      englisLabel = 'English';
      spanishLabel = 'Spanish';
      cancelLabel = 'Cancel';
      okLabel = 'Ok';
      englishChecked = true;
      spanishChecked = false;
    }
    if (this._globalServ.language == 'es') {
      titleLabel = 'Idioma';
      englisLabel = 'Ingles';
      spanishLabel = 'Español';
      cancelLabel = 'Cancelar';
      okLabel = 'Confirmar';
      englishChecked = false;
      spanishChecked = true;
    }
    const alert = await this.alertController.create({
      header: titleLabel,
      inputs: [
        {
          name: 'english',
          type: 'radio',
          label: englisLabel,
          value: 'en',
          checked: englishChecked
        },
        {
          name: 'spanish',
          type: 'radio',
          label: spanishLabel,
          value: 'es',
          checked: spanishChecked
        }
      ],
      buttons: [
        {
          text: cancelLabel,
          role: 'cancel',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: okLabel,
          handler: (value: any) => {
            console.log('Confirm Ok', value);
            this._globalServ.language = value;
            this.translate.setDefaultLang(value);
          }
        }
      ]
    });

    await alert.present();
  }

  getDataProfileFunc() {
    this._userServ.getProfile(this._globalServ.idUser, { 'entity': this._globalServ.org })
      .subscribe(dataProf => {
        this.getDataProfile = dataProf;
        this.firstName = this.getDataProfile.user.first_name;
        this.lastName = this.getDataProfile.user.last_name;
        this.userName = this.getDataProfile.user.email;
        this.city = this.getDataProfile.user.city;
        this.faculty = this.getDataProfile.user.faculty;
        this.from = this.getDataProfile.user.from;
        this.birthday = this.getDataProfile.user.birthday;
        this.image = this.getDataProfile.user.image;

      }, error => { console.log(error); })
  }

  goToJoined() {
    this.router.navigate(['joined'], {});
  }

  goToPreferences() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'fromSetting': true, 'save': true })
      }
    };
    this.router.navigate(['preferences'], navigationExtras)
  }

  async goToPassword() {

    //this.router.navigate(['change-password'], {});
    let titleLabel;
    let message;
    let cancelLabel;

    if (this._globalServ.language == 'en') {
      titleLabel = 'Settings';
      message = 'If you want to change your e-mail or password please drop us an email';

      cancelLabel = 'Cancel';

    }
    if (this._globalServ.language == 'es') {
      titleLabel = 'Configuraciones';
      message = 'Si desea cambiar su correo electr�nico o contrase�a, cont�ctenos por correo electr�nico'
      cancelLabel = 'Cancelar';

    }
    const alert = await this.alertController.create({
      header: titleLabel,
      subHeader: message,
      //message: '123demo@gmail.com',
      buttons: [
        {
          text: cancelLabel,
          role: 'cancel',
          handler: () => {
            console.log('Confirm Cancel');
          }
        },
        {
          text: 'OK',

          handler: () => {


            let email = {
              to: 'info@vistingo.com',
              cc: '',


              subject: '',
              body: '',

            };

            // Send a text message using default options
            this.emailComposer.open(email);
            console.log('Confirm Cancel');
            //Now we know we can send




          }
        },
      ]

    });

    await alert.present();
  }
  openInstagram() {
    const options: AppLauncherOptions = {
    }

    if (this.platform.is('ios')) {
      options.uri = 'fb://'
    } else {
      options.packageName = 'com.instagram.android'
    }

    this.appLauncher.canLaunch(options)
      .then((canLaunch: boolean) => {
        this.storage.set('instaConnected', true);
        console.log('Facebook is available')
      })
      .catch((error: any) => console.error('Facebook is not available'));
  }

  ionViewWillLeave() {
    let value = {
      keyCode: 13
    };
    if (!this.saved) {
      this.save(value);
    }
  }

}
