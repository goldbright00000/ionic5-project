import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/services/global.service';
import { Router } from '@angular/router';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';

@Component({
  selector: 'app-start',
  templateUrl: './start.page.html',
  styleUrls: ['./start.page.scss'],
})
export class StartPage implements OnInit {

  constructor(private router: Router, private splashScreen: SplashScreen,public _globalServ: GlobalService,) { }

  ngOnInit() {
  }
tutorial(){
   this.router.navigate(['tutorial']);
}
ionViewDidEnter() {
    this.splashScreen.hide();
  }
}
