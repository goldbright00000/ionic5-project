import { Component, OnInit, NgZone } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { PreferencesService } from '../../services/preferences.service';
import { GlobalService } from '../../services/global.service';
import { Location } from '@angular/common';
import { IfStmt } from '@angular/compiler';
import { ToastController } from '@ionic/angular';

@Component({
	selector: 'app-preferences',
	templateUrl: './preferences.page.html',
	styleUrls: ['./preferences.page.scss'],
})
export class PreferencesPage implements OnInit {

	showText: boolean;
	showTitle: boolean;
	save: boolean;
	selectBtn: boolean;
	pref: boolean;
	getSupercategories: any;
	getSupCatById: any;
	idsSupCat: any[] = [];
	fromSetting: any;
	chkAll: boolean;
	event_chk: boolean;
	cat_id: any;
	plan_create = false;
	all_check_flag: boolean;
	constructor(
		private router: Router,
		private actRoute: ActivatedRoute,
		private _preferencesServ: PreferencesService,
		public _globalServ: GlobalService,
		public location: Location,
		private toastController: ToastController,
		private zone: NgZone
	) {}

	ngOnInit() {
		if (this.actRoute.snapshot.queryParams.data != null) {

			this.all_check_flag = false;

			let getParams = JSON.parse(this.actRoute.snapshot.queryParams.data);
			console.log('Get Params', getParams);
			this.save = getParams.save;
			this.showText = getParams.showText;
			this.showTitle = getParams.showTitle;
			this.pref = getParams.ispref;
			console.log('Pref My',this.pref)
			this.fromSetting = getParams.fromSetting;
			// this.save = getParams.save === false ? getParams.save : true;
			this.selectBtn = getParams.select;
			if (getParams.hasOwnProperty('cat_id')) {
				this.cat_id = getParams.cat_id;
				console.log(this.cat_id);
			}
			if (getParams.hasOwnProperty('plan_create')) {
				this.plan_create = true;
			}
		}
		// if(this.plan_create){
		// }else{
		if (this.cat_id) {
			console.log('If Cat', this.cat_id)
			this._preferencesServ.getPrefencesByCategory(this.cat_id).subscribe((data: any) => {
				console.log("prefernces =>", data);
				this.getSupercategories = data;
				console.log(this.getSupercategories);
			})
		} else {
			this._preferencesServ.getSupercategories({ 'active': 1 }).subscribe(data => {
				console.log('Else Data', data);
				this.getSupercategories = data;
				if (this.selectBtn != true) {
					console.log("select btn if=>", this.selectBtn)
					this._preferencesServ.getSupercategoriesByUser(this._globalServ.idUser).subscribe(dataSupById => {
						this.getSupCatById = dataSupById;
						if (this.plan_create) {
							console.log(this._globalServ.idsCategories);
							for (let itemOne of this.getSupercategories) {
								for (let itemTwo of this._globalServ.idsCategories) {
									if (itemOne.id == itemTwo) {
										itemOne.checked = true;
										this._globalServ.categoryCount++;
										this.idsSupCat.push(itemOne.id);
									}
								}
							}
						} else {
							for (let itemOne of this.getSupercategories) {
								for (let itemTwo of this.getSupCatById.user_preferences) {
									console.log('Second Loop')
									if (itemOne.id == itemTwo.fk_supercategory_id) {
										itemOne.checked = true;
										this._globalServ.categoryCount++;
										this.idsSupCat.push(itemOne.id);
									}
								}
							}
						}
					}, error => {
						console.log(error);
					});
				} else if (this.selectBtn == true) {
					console.log("select btn else=>", this.selectBtn)
					if (this._globalServ.idsCategories != null) {
						for (let itemOne of this.getSupercategories) {
							for (let itemTwo of this._globalServ.idsCategories) {
								if (itemOne.id == itemTwo) {
									itemOne.checked = true;
									this.idsSupCat.push(itemOne.id);
								}
							}
						}
					}
				}
			}, error => {
				console.log(error);
			})
		}
	}

	async presentToast() {
		const toast = await this.toastController.create({
			message: "It's limited to choose 3 categories",
			duration: 2000
		});
		toast.present();
	}

	changeStatus(value: any, index: any) {
		console.log(value)

		if (this.cat_id) {
			for (let item of this.getSupercategories) {
				if (value.checked == null) {
					if (item.id == value.id) {
						item.checked = true;
						this.idsSupCat.push(item.id);
						this._globalServ.categoryCount++;
						this._globalServ.category.push(value.id);
						return;
					}
				}
				if (value.checked == true) {
					if (item.id == value.id) {
						item.checked = null;
						let getIndex = this.idsSupCat.indexOf(value.id);
						this.idsSupCat.splice(getIndex, 1);
						this._globalServ.categoryCount--;
						for (let i = 0; i <= this._globalServ.category.length; i++) {
							if (value.id === this._globalServ.category[i]) {
								this._globalServ.category.splice(i);
							}
						}
						return;
					}
				}
			}
		} else {
			for (let item of this.getSupercategories) {
				if (value.checked == null) {
					if (item.id == value.id) {
						item.checked = true;
						this.idsSupCat.push(item.id);
						this._globalServ.categoryCount++;
						this._globalServ.category.push(value.id);
						return;
					}
				}
				if (value.checked == true) {
					if (item.id == value.id) {
						item.checked = null;
						let getIndex = this.idsSupCat.indexOf(value.id);
						this.idsSupCat.splice(getIndex, 1);
						this._globalServ.categoryCount--;
						for (let i = 0; i <= this._globalServ.category.length; i++) {
							if (value.id === this._globalServ.category[i]) {
								this._globalServ.category.splice(i);
							}
						}
						return;
					}
				}
			}
		}
	}

	changeChk(event) {
		console.log("event : ", event.detail.checked);
		console.log('Show Title', this.showTitle, 'Pref', this.pref);
		this.event_chk = event.detail.checked;
		if (this.event_chk == true) {
			for (let item of this.getSupercategories) {
				item.checked = true;
				this.idsSupCat.push(item.id);
			}
		} else if (this.event_chk == false) {
			for (let item of this.getSupercategories) {
				item.checked = null;
				let getIndex = this.idsSupCat.indexOf(item.id);
				this.idsSupCat.splice(getIndex, 1);
			}
		}
	}

	resetPreferences() {
		console.log("preferensces");
		this.event_chk = false;
		this._globalServ.category = [];
		this.idsSupCat = [];
		console.log(this.idsSupCat);
		for (let item of this.getSupercategories) {
			item.checked = null;
			// let getIndex = this.idsSupCat.indexOf(item.id);
			// this.idsSupCat.splice(getIndex, 1);
			// console.log(this.idsSupCat);	
		}
	}

	saveFirstTime() {
		console.log("dave first time call")
		this._preferencesServ.saveSupercategories({ 'id': this._globalServ.idUser, 'supercategoryids': this.idsSupCat.toString() })
			.subscribe(data => {
				data;
				let navigationExtras: NavigationExtras = {
					queryParams: {
						data: JSON.stringify({})
					}, replaceUrl: true
				};
				if (this.fromSetting == true) {
					this.location.back();
				} else {
					this.router.navigate(['preferences-ok'], navigationExtras)
				}
			}, error => {
				console.log(error);
			})
	}

	select() {

		this._globalServ.idsCategories = this.idsSupCat;
		this.location.back();

	}

	ionViewWillLeave() {
		this._globalServ.currentPreferencesForAgenda = this.idsSupCat.toString();
		console.log("dave first time call")
		this._preferencesServ.saveSupercategories({ 'id': this._globalServ.idUser, 'supercategoryids': this.idsSupCat.toString() })
			.subscribe(data => {
				data;
				let navigationExtras: NavigationExtras = {
					queryParams: {
						data: JSON.stringify({})
					}, replaceUrl: true
				};
				if (this.fromSetting == true) {
					this.location.back();
				} else {
					//this.router.navigate(['preferences-ok'], navigationExtras)
				}
			}, error => {
				console.log(error);
			})
		if (this.plan_create) {
			this._globalServ.idsCategories = this.idsSupCat;
		}
	}

}