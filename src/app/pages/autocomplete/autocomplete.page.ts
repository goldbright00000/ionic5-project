import { Component, OnInit, NgZone, Input } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
	selector: 'app-autocomplete',
	templateUrl: './autocomplete.page.html',
	styleUrls: ['./autocomplete.page.scss'],
})
export class AutocompletePage {
	autocompleteItems;
	autocomplete;
	@Input() text: string;
	latitude: number = 0;
	longitude: number = 0;
	geo: any

	service = new google.maps.places.AutocompleteService();

	constructor(private zone: NgZone, public modalCtrl: ModalController, public navParams: NavParams, ) {
		this.autocompleteItems = [];
		this.autocomplete = {
			query: ''
		};
		this.autocomplete.query = this.navParams.get('text');
		console.log(this.autocomplete.query)
		this.updateSearch()
	}

	dismiss() {
		this.modalCtrl.dismiss({
			'dismissed': true
		});
	}

	chooseItem(item: any) {
		this.modalCtrl.dismiss({
			item: item,
			'dismissed': true
		});
		// this.geo = item;
		// this.geoCode(this.geo);//convert Address to lat and long
	}

	updateSearch() {

		if (this.autocomplete.query == '') {
			this.autocompleteItems = [];
			return;
		}

		let me = this;
		this.service.getPlacePredictions({
			input: this.autocomplete.query,
			componentRestrictions: {
				country: 'de'
			}
		}, (predictions, status) => {
			me.autocompleteItems = [];

			me.zone.run(() => {
				if (predictions != null) {
					predictions.forEach((prediction) => {
						me.autocompleteItems.push(prediction.description);
					});
				}
			});
		});
	}

	//convert Address string to lat and long
	geoCode(address: any) {
		let geocoder = new google.maps.Geocoder();
		geocoder.geocode({ 'address': address }, (results, status) => {
			this.latitude = results[0].geometry.location.lat();
			this.longitude = results[0].geometry.location.lng();
			alert("lat: " + this.latitude + ", long: " + this.longitude);
		});
	}
}