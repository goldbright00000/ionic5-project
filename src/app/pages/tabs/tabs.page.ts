import { Component, OnInit, ViewChild, ApplicationRef  } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { GlobalService } from '../../services/global.service';
import { ActionSheetController, IonTabs } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import {Keyboard} from "@ionic-native/keyboard/ngx";

@Component({
	selector: 'app-tabs',
	templateUrl: './tabs.page.html',
	styleUrls: ['./tabs.page.scss'],
})
export class TabsPage implements OnInit {
	@ViewChild(IonTabs) tabs: IonTabs;
	constructor(
		private router: Router,
		public _globalServ: GlobalService,
		public actionSheetController: ActionSheetController,
		private storage: Storage,
		public detectorRef: ApplicationRef,
		private keyboard: Keyboard
	) {
		this._globalServ.showIcon = true;
	}
	goto(page) {
		this.tabs.select(page).then(res => {
			console.log(res)
			this.detectorRef.tick();
		})
	}
	ngOnInit() {
	}

	goToOffersAndDiscount() {
		this.router.navigate(['tabs/offers-discounts-list'], {})
	}

	goToCityAgenda() {
		this.router.navigate(['tabs/city-agenda-list'], {})
	}

	goToCityGuide() {
		this.router.navigate(['tabs/city-guide-list'], {})
	}

	goToGroups() {
		this.router.navigate(['tabs/groups'], {})
	}

	goToPlanList() {
		this.router.navigate(['tabs/plan-list'], {})
	}

	async showMore() {
        this._globalServ.select = '';
		let titleLabel;
		let cancelLabel;
		let planLabel;
		let groupLabel;

		if (this._globalServ.language == 'en') {
			titleLabel = 'Create';
			cancelLabel = 'Cancel';
			planLabel = 'Plan';
			groupLabel = 'Group';
		}
		if (this._globalServ.language == 'es') {
			titleLabel = 'Crear';
			cancelLabel = 'Cancelar';
			planLabel = 'Plan';
			groupLabel = 'Grupo';
		}
		


		const actionSheetForPlan_group = await this.actionSheetController.create({
			header: titleLabel,
			buttons: [{
				text: planLabel,

				handler: () => {
					
					this._globalServ.planImagePath = null;
					this._globalServ.planImagePost = null;
					this.storage.set('plan-create-seats', -1);
					this.storage.set('plan-create-groups', -2);
					this.router.navigate(['plan-create'], {});
					this.storage.set('planType', '')

				}
			}, {
				text: groupLabel,
				handler: () => {
					
					this.router.navigate(['group-create'], {});
					this.storage.set('group-create-category', []);
					this.storage.set('group-create-settings', -1); // null or -1
					this.storage.set('group-create-members', []);
					this._globalServ.groupImagePath = null;
					this._globalServ.groupImagePost = null;
				}
			},
			{
				text: cancelLabel,

				role: 'cancel',
				handler: () => {
					console.log('Cancel clicked');
				}
			}
			]
		}).then(actionsheet => {
			actionsheet.present();
		});
		
	}

}
