import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children:
      [
        {
          path: 'plan-list',
          children:
            [
              {
                path: '',
                // loadChildren: () => import('../plan-list/plan-list.module').then(m => m.PlanListPageModule)
                loadChildren: '../plan-list/plan-list.module#PlanListPageModule'

              }
            ]
        }, {
          path: 'city-guide-list',
          children:
            [
              {
                path: '',
                // loadChildren: () => import('../city-guide-list/city-guide-list.module').then(m => m.CityGuideListPageModule)
                loadChildren: '../city-guide-list/city-guide-list.module#CityGuideListPageModule'

              }
            ]
        },

        {
          path: 'city-agenda-list',
          children:
            [
              {
                path: '',
                // loadChildren: () => import('../city-agenda-list/city-agenda-list.module').then(m => m.CityAgendaListPageModule)
                loadChildren: '../city-agenda-list/city-agenda-list.module#CityAgendaListPageModule'

              }
            ]
        }, {
          path: 'offers-discounts-list',
          children:
            [
              {
                path: '',
                //      loadChildren: () => import('../offers-discounts-list/offers-discounts-list.module').then(m => m.OffersDiscountsListPageModule)
                loadChildren: '../offers-discounts-list/offers-discounts-list.module#OffersDiscountsListPageModule'

              }
            ]
        }, {
          path: 'more',
          children:
            [
              {
                path: '',
                // loadChildren: () => import('../more/more.module').then(m => m.MorePageModule)
                loadChildren: '../more/more.module#MorePageModule'

              }
            ]
        },
        {
          path: 'clubs',
          children:
            [
              {
                path: '',
                //  loadChildren: () => import('../clubs/clubs.module').then(m => m.ClubsPageModule)
                loadChildren: '../clubs/clubs.module#ClubsPageModule'
              }
            ]
        },
        {
          path: 'groups',
          children:
            [
              {
                path: '',
                // loadChildren: () => import('../groups/groups.module').then(m => m.GroupsPageModule)
                loadChildren: '../groups/groups.module#GroupsPageModule'

              }
            ]
        }, {
          path: 'profile',
          children:
            [
              {
                path: '',
                // loadChildren: () => import('../profile/profile.module').then(m => m.ProfilePageModule)
                loadChildren: '../profile/profile.module#ProfilePageModule'
              }
            ]
        },
        {
          path: 'city-guide-item-list',
          children:
            [
              {
                path: '',
                // loadChildren: () => import('../city-guide-item-list/city-guide-item-list.module').then(m => m.CityGuideItemListPageModule)
                loadChildren: '../city-guide-item-list/city-guide-item-list.module#CityGuideItemListPageModule'

              }
            ]
        },
        {
          path: 'search',
          children:
            [
              {
                path: '',
                // loadChildren: () => import('../search/search.module').then(m => m.SearchPageModule)
                loadChildren: '../search/search.module#SearchPageModule'

              }
            ]
        }, {
          path: 'notifications',
          children:
            [
              {
                path: '',
                //loadChildren: () => import('../notifications/notifications.module').then(m => m.NotificationsPageModule)
                loadChildren: '../notifications/notifications.module#NotificationsPageModule'
              }
            ]
        }, {
          path: 'about',
          children:
            [
              {
                path: '',
                // loadChildren: () => import('../about/about.module').then(m => m.AboutPageModule)
                loadChildren: '../about/about.module#AboutPageModule'
              }
            ]
        },
        {
          path: '',
          redirectTo: '/tabs/plan-list',
          pathMatch: 'full'
        }
      ]
  },
  {
    path: '',
    redirectTo: '/tabs/plan-list',
    pathMatch: 'full'
  }
];

@NgModule({
  imports:
    [
      RouterModule.forChild(routes)
    ],
  exports:
    [
      RouterModule
    ]
})
export class TabsPageRoutingModule { }