import { Component, OnInit, HostListener } from '@angular/core';
import { Location } from '@angular/common';
import { GlobalService } from '../../services/global.service';
import { } from 'googlemaps';
import { GroupService } from '../../services/group.service';
import {ActivatedRoute, Router} from '@angular/router';
import { environment } from '../../../environments/environment';
import {NavController, ToastController} from '@ionic/angular';
import { get } from 'lodash';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-group-create-category',
  templateUrl: './group-create-category.page.html',
  styleUrls: ['./group-create-category.page.scss'],
})
export class GroupCreateCategoryPage implements OnInit {

  id: any;
  url: any = environment.apiUrl;
  isValidGroupImage: any = true;
  groupCreateCategory: any = [];

  innerWidth: any;
  math = Math;

  groupSuperCategoryIds: any = [];
  disable: boolean;
  groupCreate: boolean = false;
  planCreate: boolean = false;

  constructor(
    private location: Location,
    public _globalServ: GlobalService,
    public _groupServ: GroupService,
    private actRoute: ActivatedRoute,
    private toastController: ToastController,
    private storage: Storage, private router: Router,
    public navCtrl: NavController
    ) {
     if (this.actRoute.snapshot.queryParams.data != null) {
      let getParams = JSON.parse(this.actRoute.snapshot.queryParams.data);
         if (getParams.hasOwnProperty('group_create')) {
             this.groupCreate = true;
         }
         if (getParams.hasOwnProperty('plan_create')) {
             this.planCreate = true;
         }
    }
  }

  ngOnInit() {
    this.innerWidth = window.innerWidth;
      if(this.groupCreate) {
          this.storage.get('group-create-category').then((val) => {
              this.groupSuperCategoryIds = val;
          });
      }
      if(this.planCreate) {
          this.groupSuperCategoryIds = this._globalServ.idsCategories ? this._globalServ.idsCategories : [];
      }
      this.getAllCategories();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }

  getAllCategories() {
    this._groupServ.getGroupCategories().subscribe(data => {
      this.groupCreateCategory = data;
      this.groupCreateCategory = this.groupCreateCategory.map(o => {
        let value = 0;
        this.groupSuperCategoryIds.forEach(category => {
            if (category === o.id || category.id === o.id) {
                value = 1;
            }
        });
        return {
          ...o,
          value
        }
      });
        if(this.groupSuperCategoryIds.length) {
            this.checkCategories();
        }
      console.log('this.groupGroupCreateCategory: ', this.groupCreateCategory);
    }, err => {
      console.log('catetgories err: ', err);
    })
  }

  async changeStatus(idx, item) {
    console.log("--->",item);
    if (this.groupCreateCategory.length > 0) {

      if(this.groupSuperCategoryIds.length === 3) {
          item.value = 0;
          this.groupCreateCategory[idx].value = 0;
          this.groupCreateCategory.forEach(group => {
              this.groupSuperCategoryIds.forEach(value => {
                  group.disable = value !== group.id && !this.groupSuperCategoryIds.includes(group.id);
              });
          })
      }else {
          this.groupCreateCategory[idx].value = (this.groupCreateCategory[idx].value + 1) % 2;
          this.groupCreateCategory.forEach(group => {
              group.disable = false;
          });
      }
      console.log(this.groupCreateCategory[idx].value);
      this.groupSuperCategoryIds = this.groupCreateCategory.filter(o => o.value === 1).map(o => {

        console.log("Hello",o);
        return o.id;
      });
      this.checkCategories();
      if (this.groupSuperCategoryIds.length > 3) {

        this.groupSuperCategoryIds.pop();
        item.value = 0;
        const toast = await this.toastController.create({
          message: `It's limited to choose 3 categories.`,
          duration: 2000
        });
        toast.present();
      }
    }
    if(this.groupCreate) {
        this.storage.set('group-create-category', this.groupSuperCategoryIds);
    }else if(this.planCreate) {
        this._globalServ.idsCategories = this.groupSuperCategoryIds;
    }
  }

  checkCategories() {
      if(this.groupSuperCategoryIds.length === 3) {
          this.groupCreateCategory.forEach(group => {
              this.groupSuperCategoryIds.forEach(value => {
                  group.disable = value !== group.id && !this.groupSuperCategoryIds.includes(group.id);
              });
          })
      }else {
          this.groupCreateCategory.forEach(group => {
              group.disable = false;
          });
      }
  }


  goBack() {
      if(this.groupCreate) {
          this._groupServ.backToEdit = true;
      }
      this.navCtrl.back();

  }

}
