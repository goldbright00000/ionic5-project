import { Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { Location } from '@angular/common';
import { GlobalService } from '../../services/global.service';
import { } from 'googlemaps';
import { Router, NavigationExtras } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { PlanService } from '../../services/plan.service';
import { environment } from '../../../environments/environment';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { get } from 'lodash';
import { UserService } from '../../services/user.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Calendar } from '@ionic-native/calendar/ngx';
import { ModalController, Platform, ToastController, NavController } from '@ionic/angular';
import { ImageModalPage } from '../image-modal/image-modal.page';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import * as moment from 'moment';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { BranchIOService } from 'src/app/services/branch-io.service';


@Component({
	selector: 'app-plan-item',
	templateUrl: './plan-item.page.html',
	styleUrls: ['./plan-item.page.scss'],
})
export class PlanItemPage implements OnInit {

	latitudeInit: any;
	longitudeInit: any;
	zoom: any = 14;
	id: any;
	getItem: any;
	url: any = environment.apiUrl;
	urlImageProfile: any = environment.apiUrl;
	remainingUsers: number = 0;
	getParticipants: any;
	type: any;
	map: any;
	mapOptions: google.maps.MapOptions;
	joined: any = false;
	getUserLoggedin: any;
	comments = [];
	@ViewChild('map') mapElement;
	AddedToCalender: boolean = false;

	read_flag: boolean;
	comment_flag: boolean;
	tempDate;

	constructor(
		public location: Location,
		public _globalServ: GlobalService,
		private router: Router,
		private actRoute: ActivatedRoute,
		private _planServ: PlanService,
		public nativeGeocoder: NativeGeocoder,
		private _userServ: UserService,
		private iab: InAppBrowser,
		private calendar: Calendar,
		public modalController: ModalController,
		private platform: Platform,
		private socialShare: SocialSharing,
		private toastCtrl: ToastController,
		private navCtrl: NavController,
		private ga: GoogleAnalytics,
		private branch: BranchIOService,
		) {

			this.read_flag = false;
			this.comment_flag = false;
		if (this.actRoute.snapshot.queryParams.data != null) {
			let getParams = JSON.parse(this.actRoute.snapshot.queryParams.data);
			this.id = getParams.id;
			this.type = getParams.type;
			if (this.type == 'university_plan' || this.type == 'student_plan' || this.type == 'my_plan') {
				if (this._globalServ.org == 'CEU') {
					this.url = this.url + '/get-image-plan/';
				} else if (this._globalServ.org == 'IE') {
					this.url = this.url + '/get-ie-image-plan/';
				}
			}
			if (this.type == 'club_plan') {
				this.url = this.url + '/get-club-plan-image/';
			}
		}
	}

	ngOnInit() {
		this.tempDate = moment();
	}

	ionViewDidEnter() {
		this.getParticipantsData();
	}

	goBack() {
		this.navCtrl.back();
	}

	checkingJoinStatus() {
		this._planServ.getParticipations(this._globalServ.idUser).subscribe(data => {
			let listParticipations = JSON.parse(JSON.stringify(data));
			for (let item2 of listParticipations) {
				let itemParticipants = this._globalServ.org == 'CEU' ? item2.Participants : item2.IE_Participants;
				for (let item3 of itemParticipants) {
					let planId = this._globalServ.org == 'CEU' ? item3.fk_plan_id : item3.fk_ie_plan_id;
					if (planId == this.getItem.id) {
						this.getItem.join = true;
						this.joined = true;
						this.getItem.idParticipation = item3.id;
					}
				}
			}
		});
	}

	joinPlan() {
		if (this.type == 'club_plan') {
			if (this.joined == true) {
				this._planServ.leavePlanClub(this.getItem.idParticipation).subscribe(data => {
					console.log(data);
					this.joined = false;
					this.ga.trackEvent("Club Plan - "+this.getItem.title, "Left");
                    this.getParticipantsData();
				}, error => {
					console.log(error);
				})
			} else {
				this._userServ.getProfile(this._globalServ.idUser, {}).subscribe(data => {
					this.getUserLoggedin = data;
					this._planServ.joinPlanClub({ 'fk_plan_id': this.getItem.id, 'ceu_user_id': this._globalServ.idUser, 'name': this.getUserLoggedin.user.first_name + ' ' + this.getUserLoggedin.user.last_name, 'email': this.getUserLoggedin.user.email, 'fk_club_plan_id': this.getItem.id }).subscribe(data => {
						console.log(data);
						this.joined = true;
						this.ga.trackEvent("Club Plan - "+this.getItem.title, "Joined");
                        this.getParticipantsData();
					}, error => {
						console.log(error);
					})
				});
			}
		}
		if (this.type == 'university_plan' || this.type == 'student_plan' || this.type == 'my_plan') {
			if (this.joined == true) {
				this._planServ.leavePlanUniversity(this.getItem.idParticipation).subscribe(data => {
					console.log(data);
					this.joined = false;
					this.getParticipantsData();
					let plan = this.type == 'university_plan' ? 'University Plan':(this.type == 'my_plan'?'My Plan':'Student Plan');
					this.ga.trackEvent(plan+' - '+this.getItem.title, "Left");					
				}, error => {
					console.log(error);
				})
			} else {
				this._userServ.getProfile(this._globalServ.idUser, {}).subscribe(data => {
					this.getUserLoggedin = data;
					if (this._globalServ.org == 'CEU') {
						this._planServ.joinPlanUniversity({
							'fk_plan_id': this.getItem.id,
							'ceu_user_id': this._globalServ.idUser,
							'name': this.getUserLoggedin.user.first_name + ' ' + this.getUserLoggedin.user.last_name,
							'email': this.getUserLoggedin.user.email
						}).subscribe(data => {
							console.log(data);
							this.joined = true;
							this.getParticipantsData();
							let plan = this.type == 'university_plan' ? 'University Plan':(this.type == 'my_plan'?'My Plan':'Student Plan');
							this.ga.trackEvent(plan+' - '+this.getItem.title, "Joined");							
						}, error => {
							console.log(error);
						})
					} else if (this._globalServ.org == 'IE') {
						this._planServ.joinPlanUniversity({
							'fk_ie_plan_id': this.getItem.id,
							'ie_user_id': this._globalServ.idUser,
							'name': this.getUserLoggedin.user.first_name + ' ' + this.getUserLoggedin.user.last_name,
							'email': this.getUserLoggedin.user.email
						}).subscribe(data => {
							console.log(data);
							this.joined = true;
							let plan = this.type == 'university_plan' ? 'University Plan':(this.type == 'my_plan'?'My Plan':'Student Plan');
							this.ga.trackEvent(plan+' - '+this.getItem.title, "Joined");
							this.getParticipantsData();
						}, error => {
							console.log(error);
						})
					}
				});
			}
		}
	}

	goToViewProfile() {
		let navigationExtras: NavigationExtras = {
			queryParams: {
				data: JSON.stringify({})
			}
		};
		this.router.navigate(['view-profile'], navigationExtras)
	}

	mapFunc(lat: number, long: number) {
		//set map
		let coords = new google.maps.LatLng(this.latitudeInit, this.longitudeInit);
		this.mapOptions = {
			center: coords,
			zoom: this.zoom,
			maxZoom: 17,
			minZoom: 3,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: true,
			styles: [
			{
				"featureType": "landscape",
				"stylers": [
				{
					"visibility": "off"
				}
				]
			},
			{
				"featureType": "poi",
				"stylers": [
				{
					"visibility": "off"
				}
				]
			},
			{
				"featureType": "road",
				"elementType": "labels",
				"stylers": [
				{
					"visibility": "off"
				}
				]
			},
			{
				"featureType": "transit",
				"stylers": [
				{
					"visibility": "off"
				}
				]
			}
			]
		}

		console.log('this.mapele: ', this.mapElement);

		this.map = new google.maps.Map(this.mapElement.nativeElement, this.mapOptions);
		var marker: google.maps.Marker = new google.maps.Marker({
			map: this.map,
			position: { lat: lat, lng: long },
		});
	}

	goToLink() {
		if (get(this.getItem, 'link', '') !== '') {
			this.iab.create(get(this.getItem, 'link', ''), '_system');
		}
	}

	getControlsOnStyleDisplay() {
		if (get(this.getItem, 'Place', '') !== '' || (get(this.getItem, 'latitude', '') !== '' && get(this.getItem, 'longitude', '') != '')) {
			return "block";
		} else {
			return "none";
		}
	}
	AddToCalender(item) {
		console.log(item);
		this.calendar.createEvent(item.title, item.address, item.description, new Date(), new Date(item.plan_date)).then(
			(msg) => {
				this.AddedToCalender = true;
				console.log(msg);
				this.showToast('Plan was added to calender.');				
			},
			(err) => { console.log(err); }
			);
	}

	async presentModal() {
		const modal = await this.modalController.create({
			component: ImageModalPage,
			cssClass: 'imageModal',
			backdropDismiss: true,
			componentProps: {
				image: this.url + this.getItem.image
			}
		});
		return await modal.present();
	}

	openGoogleMaps(lat, lng){
		let destination = lat + ',' + lng;
		if(this.platform.is('ios')){
			window.open('maps://?q=' + destination, '_system');
		} else {
			let label = encodeURI('My Label');
			window.open('geo:0,0?q=' + destination + '(' + label + ')', '_system');
		}
	}

	mailShare(){
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaEmail("Hi! Download Vive IE app and join my plan "+this.getItem.title+" "+res.url, "Vive IE app", []).then(()=>{
			let plan = this.type == 'university_plan' ? 'University Plan':(this.type == 'my_plan'?'My Plan':(this.type == 'club_plan'?'Club Plan':'Student Plan'));
			this.ga.trackEvent(plan+' - '+this.getItem.title, "Shared via Email");			
        	}).catch(()=>{
            	//this.showToast('App not found.');
        	});
		}).catch((err)=>{
			console.log(err);
		});		
	}

	facebookShare(){
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaFacebookWithPasteMessageHint("", null,res.url, "Hi! Download Vive IE app and join my plan "+this.getItem.title+" ").then(()=>{
			let plan = this.type == 'university_plan' ? 'University Plan':(this.type == 'my_plan'?'My Plan':(this.type == 'club_plan'?'Club Plan':'Student Plan'));
			this.ga.trackEvent(plan+' - '+this.getItem.title, "Shared via Facebook");
        	}).catch(()=>{
            	//this.showToast('App not found.');
        	});
		}).catch((err)=>{
			console.log(err);
		});        
	}
	instagramShare(){
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaInstagram("Hi! Download Vive IE app and join my plan "+this.getItem.title+" "+res.url, null).then(()=>{
			let plan = this.type == 'university_plan' ? 'University Plan':(this.type == 'my_plan'?'My Plan':(this.type == 'club_plan'?'Club Plan':'Student Plan'));
			this.ga.trackEvent(plan+' - '+this.getItem.title, "Shared via Instagram");
        	}).catch(()=>{
           		// this.showToast('App not found.');
        	});
		}).catch((err)=>{
			console.log(err);
		});         
	}
    whatsappShare(){	
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaWhatsApp("Hi! Download Vive IE app and join my plan "+this.getItem.title+" ",null,res.url).then(()=>{
				let plan = this.type == 'university_plan' ? 'University Plan':(this.type == 'my_plan'?'My Plan':(this.type == 'club_plan'?'Club Plan':'Student Plan'));
				this.ga.trackEvent(plan+' - '+this.getItem.title, "Shared via Whatsapp");
			}).catch(()=>{
			   // this.showToast('App not found.');
			});
		}).catch((err)=>{
			console.log(err);
		});        
    }
    twitterShare(){
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaTwitter("Hi! Download Vive IE app and join my plan "+this.getItem.title+" ",null,res.url).then(()=>{
			let plan = this.type == 'university_plan' ? 'University Plan':(this.type == 'my_plan'?'My Plan':(this.type == 'club_plan'?'Club Plan':'Student Plan'));
			this.ga.trackEvent(plan+' - '+this.getItem.title, "Shared via Twitter");
        	}).catch(()=>{
         		//   this.showToast('App not found.');
        	});
		}).catch((err)=>{
			console.log(err);
		});        
	}
	async showToast(msg){
		const toast = await this.toastCtrl.create({
			message: msg,
			duration: 2000		
		})	
		await toast.present();
	}

	writeComment() {
		let navigationExtras: NavigationExtras = {
			queryParams: {
				data: JSON.stringify({ 'id': this.id }),
				type: JSON.stringify({ 'type': 'plan'})
			}
		};
		this.router.navigate(['write-comment'], navigationExtras)
	}

	readMore(){
		this.read_flag = true;
	}
	readLess(){
		this.read_flag = false;
	}

	view_all_comments(){
		this.comment_flag = true;
	}
	view_less_comments(){
	this.comment_flag = false;
	}

	timeDiff(mytime) {
		// console.log("---", mytime);
		var t2 = mytime;
		let time2 = moment(t2);
	
		var duration1 = this.tempDate.diff(time2, 'seconds');
	
		if (duration1 > 60) {
		  duration1 = this.tempDate.diff(time2, 'minutes');
		  if (duration1 > 60) {
			duration1 = this.tempDate.diff(time2, 'hours');
			if (duration1 > 24) {
			  duration1 = this.tempDate.diff(time2, 'days');
			  if (duration1 > 7) {
				duration1 = this.tempDate.diff(time2, 'weeks');
				if (duration1 > 5) {
				  duration1 = this.tempDate.diff(time2, 'month');
				  if (duration1 > 12) {
					duration1 = this.tempDate.diff(time2, 'years');
					return duration1 + "y";
				  } else {
					return duration1 + "m";
				  }
				} else {
				  return duration1 + "w";
				}
			  } else {
				return duration1 + "d";
			  }
			} else {
			  return duration1 + "h";
			}
		  } else {
			return duration1 + "min";
		  }
		}
		else {
		  if (duration1 < 0) {
			return "0s";
		  }
		  else {
			return duration1 + "s";
		  }
	
		}
	  }

	  getParticipantsData() {
          this._planServ.getParticipants(this.id, { 'participantlimit': 5, 'entity': this._globalServ.org })
              .subscribe(data => {
                  this.getParticipants = data;
                  this.checkingJoinStatus();
              });
          if (this.type === 'university_plan' || this.type === 'student_plan' || this.type === 'my_plan') {
              this._planServ.getItemUniv(this.id)
                  .subscribe(data => {
                      console.log("data ====>", data)
                      if(data['comments'].length != 0){
                          this.comments = data['comments']

                          console.log(">>>",this.comments);
                      }
                      if (this._globalServ.org == 'CEU') {
						  this.getItem = data;
						  let plan = this.type == 'university_plan' ? 'University Plan':(this.type == 'my_plan'?'My Plan':(this.type == 'club_plan'?'Club Plan':'Student Plan'));
						this.ga.trackEvent(plan+' - '+this.getItem.title, "Viewed");
                          if (this.getItem.Place != null) {
                              this.latitudeInit = Number(this.getItem.Place.latitude);
                              this.longitudeInit = Number(this.getItem.Place.longitude);
                          } else if (this.getItem.latitude != null && this.getItem.longitude != null) {
                              this.latitudeInit = Number(this.getItem.latitude);
                              this.longitudeInit = Number(this.getItem.longitude);
                          }

                          if (this.getItem.Participants != null) {
                              if (this.getItem.Participants.length < 6) {
                                  this.remainingUsers = 0;
                              } else {
                                  this.remainingUsers = this.getItem.Participants.length - 5;
                              }
                          }
                          console.log('map info1: ', this.getItem);

                          if (this.getItem.Place != null) {
                              this.mapFunc(Number(this.getItem.Place.latitude), Number(this.getItem.Place.longitude));
                          } else if (this.getItem.latitude != null && this.getItem.longitude != null) {
                              this.mapFunc(Number(this.getItem.latitude), Number(this.getItem.longitude));
                          }
                      } else if (this._globalServ.org === 'IE') {
                          this.getItem = data['ie_plan'];
						  let plan = this.type == 'university_plan' ? 'University Plan':(this.type == 'my_plan'?'My Plan':(this.type == 'club_plan'?'Club Plan':'Student Plan'));
						  this.ga.trackEvent(plan+' - '+this.getItem.title, "Viewed");
                          if (this.getItem.Place != null) {
                              this.latitudeInit = Number(this.getItem.Place.latitude);
                              this.longitudeInit = Number(this.getItem.Place.longitude);
                          } else if (this.getItem.latitude != null && this.getItem.longitude != null) {
                              this.latitudeInit = Number(this.getItem.latitude);
                              this.longitudeInit = Number(this.getItem.longitude);
                          }

                          if (this.getItem.IE_Participants != null) {
                              if (this.getItem.IE_Participants.length < 6) {
                                  this.remainingUsers = 0;
                              } else {
                                  this.remainingUsers = this.getItem.IE_Participants.length - 5;
                              }
                          }
                          console.log('map info2: ', this.getItem);

                          if (this.getItem.Place != null) {
                              this.mapFunc(Number(this.getItem.Place.latitude), Number(this.getItem.Place.longitude));
                          } else if (this.getItem.latitude != null && this.getItem.longitude != null) {
                              this.mapFunc(Number(this.getItem.latitude), Number(this.getItem.longitude));
                          }
                      }
                  }, error => {
                      console.log(error);
                  });
          }
          if (this.type === 'club_plan') {
              this._planServ.getItemClub(this.id)
                  .subscribe(data => {
					  this.getItem = data;
					  let plan = this.type == 'university_plan' ? 'University Plan':(this.type == 'my_plan'?'My Plan':(this.type == 'club_plan'?'Club Plan':'Student Plan'));
						this.ga.trackEvent(plan+' - '+this.getItem.title, "Viewed");
                      if (this.getItem.Place != null) {
                          this.latitudeInit = Number(this.getItem.Place.latitude);
                          this.longitudeInit = Number(this.getItem.Place.longitude);
                      } else if (this.getItem.latitude != null && this.getItem.longitude != null) {
                          this.latitudeInit = Number(this.getItem.latitude);
                          this.longitudeInit = Number(this.getItem.longitude);
                      }

                      if (this.getItem.Participants != null) {
                          if (this.getItem.Participants.length < 6) {
                              this.remainingUsers = 0;
                          } else {
                              this.remainingUsers = this.getItem.Participants.length - 5;
                          }
                      }

                      console.log('map info3: ', this.getItem);

                      if (this.getItem.Place != null) {
                          this.mapFunc(Number(this.getItem.Place.latitude), Number(this.getItem.Place.longitude));
                      } else if (this.getItem.latitude != null && this.getItem.longitude != null) {
                          this.mapFunc(Number(this.getItem.latitude), Number(this.getItem.longitude));
                      }
                  }, error => {
                      console.log(error);
                  });
		  }		  
	  }
	getDate(plandate) {
		if (plandate) {
			const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
				"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
			];
			let dateCommon = new Date(plandate).toDateString();

			let day = dateCommon.slice(0, dateCommon.indexOf(' '));
			let date = new Date(plandate).getDate();
			let month = monthNames[new Date(plandate).getMonth()];
			let year = new Date(plandate).getFullYear();
			let hours = new Date(plandate).getUTCHours();
			let minutes: any = new Date(plandate).getUTCMinutes();
			if (minutes < 10)
				minutes = '0' + minutes;
			else
				minutes = '' + minutes;
			return month + ' ' + year
		}
	}
	getTime(plandate) {
		if (plandate) {
			// const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
			// 	"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
			// ];
			// let dateCommon = new Date(plandate).toDateString();

			// let day = dateCommon.slice(0, dateCommon.indexOf(' '));
			// let date = new Date(plandate).getDate();
			// let month = monthNames[new Date(plandate).getMonth()];
			// let year = new Date(plandate).getFullYear();
			// let hours = new Date(plandate).getUTCHours();
			// let minutes: any = new Date(plandate).getUTCMinutes();
			// if (minutes < 10)
			// 	minutes = '0' + minutes;
			// else
			// 	minutes = '' + minutes;
			// return  hours + ':' + minutes
			let hours = new Date(plandate).getUTCHours();
			let minutes:any = new Date(plandate).getUTCMinutes();
			var ampm = hours >= 12 ? 'pm' : 'am';
			hours = hours % 12;
			hours = hours ? hours : 12; // the hour '0' should be '12'
			minutes = minutes < 10 ? '0' + minutes : minutes;
			var strTime = hours + ':' + minutes + ' ' + ampm;
			return strTime;
		}
	}
}
