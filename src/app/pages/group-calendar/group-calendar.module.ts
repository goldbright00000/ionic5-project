import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { Routes, RouterModule } from "@angular/router";

import { IonicModule } from "@ionic/angular";

import { GroupCalendarPage } from "./group-calendar.page";

import { ComponentModule } from "../../components/component.module";

const routes: Routes = [
  {
    path: "",
    component: GroupCalendarPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    RouterModule.forChild(routes)
  ],
  declarations: [GroupCalendarPage]
})
export class GroupCalendarPageModule {}
