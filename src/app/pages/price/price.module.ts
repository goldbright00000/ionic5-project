import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { ComponentModule } from '../../components/component.module';

import { PricePage } from './price.page';

const routes: Routes = [
  {
    path: '',
    component: PricePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
	IonicModule,
	ComponentModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PricePage]
})
export class PricePageModule {}
