import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { GroupDetailPage } from './group-detail.page'
import { TruncateModule } from '@yellowspot/ng-truncate';
import { ComponentModule } from '../../components/component.module';

const routes: Routes = [
{
  path: '',
  component: GroupDetailPage
}
];

@NgModule({
  imports: [
  CommonModule,
  ComponentModule,
  FormsModule,
  IonicModule,
  TruncateModule,
  RouterModule.forChild(routes)
  ],
  declarations: [GroupDetailPage]
})
export class GroupDetailPageModule {}
