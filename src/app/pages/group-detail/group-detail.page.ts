import { Component, OnInit, HostListener, Input } from "@angular/core";
import { Location } from "@angular/common";
import { GlobalService } from "../../services/global.service";
import {} from "googlemaps";
import { GroupService } from "../../services/group.service";
import { UserService } from "../../services/user.service";
import { ActivatedRoute } from "@angular/router";
import { environment } from "../../../environments/environment";
import {
  ToastController,
  NavController,
  AlertController
} from "@ionic/angular";
import { get, find } from "lodash";
import { Router, NavigationExtras } from "@angular/router";
import { Storage } from "@ionic/storage";
import { TruncateModule } from "@yellowspot/ng-truncate";
import { ActionSheetController } from "@ionic/angular";
import * as moment from "moment";
import { GoogleAnalytics } from "@ionic-native/google-analytics/ngx";

@Component({
  selector: "app-group-detail",
  templateUrl: "./group-detail.page.html",
  styleUrls: ["./group-detail.page.scss"]
})
export class GroupDetailPage implements OnInit {
  id: any;
  comp: any;
  url: any = environment.apiUrl;
  getItem: any = {};
  isValidGroupImage: any = true;
  groupMembers: any = [];
  groupAdmins: any = [];
  plans: any = [];
  comments: any = [];
  isCreator = false;
  innerWidth: any;
  math = Math;
  descLength: any;

  isJoined: any = false;
  tempDate;
  comment_flag: boolean;

  profile: any = {};

  @Input() text: string;
  @Input() limit: number = 250;
  truncating = true;
  createPlan: boolean;

  selectedType: any;

  creater_pic: any;
  creater_name: any;
  isPrivate: any = false;
  issent:  any = false;

  constructor(
    public location: Location,
    public _globalServ: GlobalService,
    public _groupServ: GroupService,
    private actRoute: ActivatedRoute,
    private toastController: ToastController,
    private router: Router,
    private _userServ: UserService,
    private storage: Storage,
    public actionSheetController: ActionSheetController,
    public navCtrl: NavController,
    public alertController: AlertController,
    public ga: GoogleAnalytics
  ) {
    if (this.actRoute.snapshot.queryParams.data != null) {
      let getParams = JSON.parse(this.actRoute.snapshot.queryParams.data);
      this.id = getParams.id;
      this.comp = getParams.type;
      console.log("id: ", this.id);
      this.storage.set("plan-create-groups", this.id);
      this.comment_flag = false;
    }

    this.selectedType = 'information';
  }

  ngOnInit() {
    this.innerWidth = window.innerWidth;
    this.tempDate = moment();
  }

  @HostListener("window:resize", ["$event"])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }

  segmentChanged(eve) {
    console.log(eve.detail.value)

    this.selectedType = eve.detail.value
  }

  ionViewDidEnter() {


    this.getProfile();
    this._userServ.canCreateGroupPlan(this._globalServ.idUser).subscribe(
      (data: any) => {
        console.log(data);
        for (let i = 0; i < data.length; i++) {
          if (this.id == data[i].id) {
            this.createPlan = true;
          }
        }
      },
      err => {
        console.log("err public profile: ", err);
      }
    );
    // this.timeDiff();
  }

  getProfile() {
    this._userServ.getPublicProfile(this._globalServ.idUser).subscribe(
      data => {
        this.profile = get(data, "[0]", {});
        console.log("this.profile: ", this.profile);

        this.getGroupDetail();
        this.getGroupComments();
        this.getGroupPlans();
      },
      err => {
        console.log("err public profile: ", err);
      }
    );
  }



  getGroupDetail() {
    this._groupServ.getGroupDetail(this.id).subscribe(
      data => {
        console.log("group detail data: ", data);
        this.getItem = get(data, "group", {});

        this.isPrivate = this.getItem.private == 1 ? true : false;

        this._userServ.getPublicProfile(this.getItem.fk_user_id).subscribe(
          data => {
            this.profile = get(data, "[0]", {});

            let first_name = this.profile.first_name ? this.profile.first_name : '';
            let last_name = this.profile.last_name ? this.profile.last_name : '';
            this.creater_name = first_name + ' ' + last_name;
            
            this.creater_pic = this.profile.image;
          });

        this.ga.trackEvent("Group - " + this.getItem.title, "Viewed");
        this.groupMembers = get(data, "members", []);
        this.groupAdmins = get(data, "groupAdmins", []);
        this.groupAdmins.forEach((element: any) => {
          this._userServ.getPublicProfile(element.fk_user_id).subscribe(
            data => {
              this.profile = get(data, "[0]", {});
              element.image = this.profile.image;
              element.name = this.profile.first_name + ' ' + this.profile.last_name;
            });

        });

        console.log("this is length =>" + this.getItem.description.length);
        this.descLength = this.getItem.description.length;

        this.isCreator =
          this.getItem.fk_user_id == this._globalServ.idUser ? true : false;
        console.log("creator =>", this.isCreator);
        console.log("user id: ", this._globalServ.idUser);
        if (find(this.groupMembers, ["id", this._globalServ.idUser])) {
          this.isJoined = true;
        } else {
          this.isJoined = false;
        }
      },
      err => {
        console.log("group detail err: ", err);
      }
    );
  }

  getDuration(tcreated) {
    let now = new Date().getTime();
    let created = new Date(tcreated).getTime();

    if (now > created) {
      let hours = new Date(now - created).getHours();
      if (Math.floor(hours / 24) === 0) {
        return "1d";
      }
      return Math.floor(hours / 24) + "d";
    } else {
      return "";
    }
  }

  getGroupComments() {
    this._groupServ.getGroupComments(this.id).subscribe(
      data => {
        console.log("group comment data: ", data);
        this.comments = get(data, "comment", {});
        this.comments = this.comments.map(o => {
          let duration = this.getDuration(o.created);

          return {
            ...o,
            duration
          };
        });
      },
      err => {
        console.log("group comment err: ", err);
      }
    );
  }

  getGroupPlans() {
    this._groupServ.getGroupPlans(this.id, this.profile.email).subscribe(
      data => {
        console.log("group plans data: ", data);
        if (this._globalServ.org == "CEU") {
          this.plans = data;
        } else {
          this.plans = data["groupPlans"];
        }
        this.plans = this.plans.map(o => {
          return {
            ...o,
            type: "group_plan"
          };
        });
      },
      err => {
        console.log("group plans err: ", err);
      }
    );
  }

  goToGroupPlanItem(plan) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ id: plan.id, email: this.profile.email })
      }
    };
    this.router.navigate(["group-plan-item"], navigationExtras);
  }

  openCalendar() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({
          id: this.id,
          name: this.getItem.title,
          email: this.profile.email
        })
      }
    };
    this.router.navigate(["group-calendar"], navigationExtras);
    this.router.navigate(["group-calendar"], navigationExtras);
  }

  goBack() {
    this.navCtrl.back();
  }

  updateGroupUrl() {
    this.isValidGroupImage = false;
  }

  joinGroup() {
    if (this.isJoined == true) {
      this._groupServ.leaveGroupPlan(this.id, null).subscribe(
        data => {
          console.log(data);
          this.isJoined = false;
          this.ga.trackEvent("Group - " + this.getItem.title, "Left");
        },
        error => {
          console.log(error);
        }
      );
    } else {
      let param = {
        userId: this._globalServ.idUser,
        email: this.profile.email
      };

      this._groupServ.joinGroup(this.id, param).subscribe(
        async data => {
          console.log("joined success: ", data);
          this.getGroupDetail();
          const toast = await this.toastController.create({
            message: "Joined successfully.",
            duration: 2000
          });
          toast.present();
          this.isJoined = true;
          this.ga.trackEvent("Group - " + this.getItem.title, "Joined");
        },
        async err => {
          console.log("join group err: ", err);
          const toast = await this.toastController.create({
            message: err.message,
            duration: 2000
          });
          toast.present();
          this.isJoined = false;
        }
      );
    }
  }

  SentRequest() {

    let param = {
      userId: this._globalServ.idUser,
      groupId: this.id
    };

    this._groupServ.GroupRequestSent(this.id, param).subscribe(
      async data => {
       
        this.getGroupDetail();
        const toast = await this.toastController.create({
          message: "Request Sent successfully.",
          duration: 2000
        });
        toast.present();
        this.issent = true;
       // this.isJoined = true;
       // this.ga.trackEvent("Group - " + this.getItem.title, "Joined");
      },
      async err => {
        console.log("sent request group err: ", err);
        const toast = await this.toastController.create({
          message: err.message,
          duration: 2000
        });
        toast.present();
        this.issent = false;
       // this.isJoined = false;
      }
    );

  }

  writeComment() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ id: this.id }),
        type: JSON.stringify({ type: "group" })
      }
    };
    this.router.navigate(["write-comment"], navigationExtras);
  }

  goToMembers() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({
          id: this.id,
          member: "invite",
          title: this.getItem.title
        })
      }
    };
    this.router.navigate(["group-create-invite"], navigationExtras);
  }

  goToCreatePlan() {
    this.storage.set("plan-create-seats", -1);
    this.storage.set("plan-create-createdby", 1);

    // let navigationExtras: NavigationExtras = {
    //   queryParams: {
    //     data: JSON.stringify(),
    //     group: JSON.stringify(this.getItem)
    //   }
    // };
    this._groupServ
      .getEditGroupDetails(this.id, this.profile.email)
      .subscribe((data: any) => {
        console.log("group edit data: ", data);
        let navigationExtras: NavigationExtras = {
          queryParams: {
            data: JSON.stringify({
              item: data.group,
              groupId: this.id,
              categories: data.groupCategories,
              fromGroup: true
            })
          }
        };
        this.router.navigate(["plan-create"], navigationExtras);
      });
  }

  async presentActionSheet(groupID) {
    const actionSheet = await this.actionSheetController.create({
      // header: 'Delete Group',
      buttons: [
        {
          text: "Delete Group",
          role: "destructive",
          // icon: 'trash',
          handler: () => {
            this.confirm(groupID);
          }
        },
        {
          text: "Edit",
          // icon: 'create',
          handler: () => {
            this._groupServ
              .getEditGroupDetails(this.id, this.profile.email)
              .subscribe(
                data => {
                  console.log("group edit data: ", data);
                  let navigationExtras: NavigationExtras = {
                    queryParams: {
                      data: data
                    }
                  };
                  this.router.navigate(["edit-group"], navigationExtras);
                },
                err => {
                  console.log("group plans err: ", err);
                }
              );
          }
        },
        {
          text: "Cancel",
          // icon: 'close',
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });
    await actionSheet.present();
  }
  
  async confirm(groupID) {
    //this.router.navigate(['change-password'], {});
    let titleLabel;
    let message;
    let cancelLabel;
    let Oklabel;

    if (this._globalServ.language == "en") {
      titleLabel = "Delete Group";
      message = "Are you sure you want to remove this group?";
      Oklabel = "OK";
      cancelLabel = "Cancel";
    }
    if (this._globalServ.language == "es") {
      titleLabel = "Eliminar grupo";
      message = "�Seguro que quieres eliminar este grupo?";
      cancelLabel = "Cancelar";
      Oklabel = "Okay";
    }
    const alert = await this.alertController.create({
      header: titleLabel,
      subHeader: message,
      //message: '123demo@gmail.com',
      buttons: [
        {
          text: cancelLabel,
          role: "cancel",
          handler: () => {
            console.log("Confirm Cancel");
          }
        },
        {
          text: Oklabel,

          handler: () => {
            this._groupServ.deleteGroup(groupID, null).subscribe(
              data => {
                console.log("deleted group: ", data);
                // const toast = this.toastController.create({mytime
                //   message: 'Group has been removed!',
                //   duration: 2000
                // });
                // toast.present();
                this.ga.trackEvent("Group - " + this.getItem.title, "Deleted");
                this.router.navigate(["tabs/groups"], {});
              },
              err => {
                console.log("delete group err: ", err);
              }
            );
          }
        }
      ]
    });

    await alert.present();
  }

  timeDiff(mytime) {
    // console.log("---", mytime);
    var t2 = mytime;
    let time2 = moment(t2);

    var duration1 = this.tempDate.diff(time2, "seconds");

    if (duration1 > 60) {
      duration1 = this.tempDate.diff(time2, "minutes");
      if (duration1 > 60) {
        duration1 = this.tempDate.diff(time2, "hours");
        if (duration1 > 24) {
          duration1 = this.tempDate.diff(time2, "days");
          if (duration1 > 7) {
            duration1 = this.tempDate.diff(time2, "weeks");
            if (duration1 > 5) {
              duration1 = this.tempDate.diff(time2, "month");
              if (duration1 > 12) {
                duration1 = this.tempDate.diff(time2, "years");
                return duration1 + "y";
              } else {
                return duration1 + "m";
              }
            } else {
              return duration1 + "w";
            }
          } else {
            return duration1 + "d";
          }
        } else {
          return duration1 + "h";
        }
      } else {
        return duration1 + "min";
      }
    } else {
      if (duration1 < 0) {
        return "0s";
      } else {
        return duration1 + "s";
      }
    }
  }

  view_all_comments() {
    this.comment_flag = true;
  }

  view_less_comments() {
    this.comment_flag = false;
  }
}
