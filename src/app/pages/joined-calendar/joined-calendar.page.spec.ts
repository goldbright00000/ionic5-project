import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoinedCalendarPage } from './joined-calendar.page';

describe('JoinedCalendarPage', () => {
  let component: JoinedCalendarPage;
  let fixture: ComponentFixture<JoinedCalendarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoinedCalendarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoinedCalendarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
