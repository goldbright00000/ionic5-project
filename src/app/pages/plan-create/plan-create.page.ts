import { Component, OnInit, ElementRef, ViewChild, ChangeDetectorRef } from '@angular/core';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Router, NavigationExtras } from '@angular/router';
import { GlobalService } from '../../services/global.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { environment } from '../../../environments/environment';
import { Location } from '@angular/common';
import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { Storage } from '@ionic/storage';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { get } from 'lodash';
import { PlanService } from 'src/app/services/plan.service';
import {
	ToastController, ActionSheetController, ModalController, Platform, IonContent,
	NavController, AlertController,
} from '@ionic/angular';
import { Crop } from '@ionic-native/crop/ngx';
import { } from 'googlemaps';
import { AutocompletePage } from '../autocomplete/autocomplete.page';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { TranslateService } from '@ngx-translate/core';
import { PreferencesService } from 'src/app/services/preferences.service';
import { Base64 } from '@ionic-native/base64/ngx';
import { group } from "@angular/animations";
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { Calendar } from '@ionic-native/calendar/ngx';
import * as moment from 'moment';

@Component({
	selector: 'app-plan-create',
	templateUrl: './plan-create.page.html',
	styleUrls: ['./plan-create.page.scss'],
})
export class PlanCreatePage implements OnInit {
	public places: ElementRef;
	title: any = '';
	location: any = '';
	planDate: any = moment().format("dddd MM/DD/YYYY");
	limitDate: any = new Date().toISOString();
	limitDateval: any;
	planDateval: any;
	details: any = '';
	myForm: FormGroup;
	avalibleSeats: any;
	post: boolean;
	okIdsCategories: boolean;
	createdBy: any;
	meetingPoint: any = '';
	groupId: any = null;
	profile: any = {};
	pageTitleName: any = false;
	article: any;
	url: any = environment.apiUrl;
	imageUrl: any;
	pathImage: any;
	pathImageCropper: any;
	pathImagePost: any;
	input: HTMLInputElement;
	autocomplete: google.maps.places.Autocomplete;
	modal: any;
	price: any;
	planType: any;
	category: any = [];
	categoryList = [];
	selectedDay:any;
	daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednsday', 'Thursday', 'Friday', 'Saturday']
	public planCreate: boolean = false;

	@ViewChild('input') myInput;
	validated: boolean;
	validated1: boolean;
	@ViewChild('scrollContent') scroll: IonContent;
	done: boolean = true;
	fileName: any;
	fromGroup: any;
	public currentPage: string;

	confirmedExit: boolean = true;
	///public unsubscribeBackEvent: any;
	date: any;

	customDayShortNames = ['s\u00f8n', 'man', 'tir', 'ons', 'tor', 'fre', 'l\u00f8r'];

	datePickerObj: any = {
		inputDate: new Date(),
		dateFormat: 'dddd MM/DD/YYYY',
		clearButton: false
	}


	constructor(
		private datePicker: DatePicker,
		private camera: Camera,
		private sanitizer: DomSanitizer,
		private webview: WebView,
		private router: Router,
		public _globalServ: GlobalService,
		fb: FormBuilder,
		private transfer: FileTransfer,
		private locationUrl: Location,
		private nativeGeocoder: NativeGeocoder,
		private storage: Storage,
		private actRoute: ActivatedRoute,
		private _userServ: UserService,
		private toastController: ToastController,
		private _planServ: PlanService,
		public modalCtrl: ModalController,
		private keyboard: Keyboard,
		public detectorRef: ChangeDetectorRef,
		public actionSheetController: ActionSheetController,
		private translate: TranslateService,
		private _preferencesServ: PreferencesService,
		private platform: Platform,
		private base64: Base64,
		private crop: Crop,
		private alertController: AlertController,
		public navCtrl: NavController,
		public ga: GoogleAnalytics,
		private calendar: Calendar
	) {


		if (this._globalServ.language === 'en')
			this.createdBy = this.planCreate ? "Me" : this.title ? this.title : 'Pick';
		else
			this.createdBy = this.planCreate ? "Me" : this.title ? this.title : 'Recoger';
		if (this.actRoute.snapshot.queryParams.data != null) {
			let getParams = JSON.parse(this.actRoute.snapshot.queryParams.data);
			this.currentPage = 'group';
			console.log("group param =>", getParams);
			this.groupId = getParams.groupId;
			this.storage.set('plan-create-groups', this.groupId);
			console.log('groupId: ', this.groupId);
			this.title = getParams.item.title
			if (this.groupId)
				this.createdBy = this.title
			this.details = getParams.item.description;
			this.fromGroup = getParams.fromGroup;
			this._globalServ.planImagePath = getParams.item.image;
			this.fileName = getParams.item.image.split('/')[5];
			console.log(this._globalServ.planImagePost);
			console.log(getParams.item.image.split('/')[5]);
			if (getParams.categories && getParams.categories.length) {
				getParams.categories.forEach(value => {
					this.categoryList.push(value.id);
				});
			}
		}
		if (this.actRoute.snapshot.queryParams.flag == 'city') {
			let getParams = JSON.parse(this.actRoute.snapshot.queryParams.city)
			this.currentPage = 'city';
			console.log(getParams)
			if (this._globalServ.language == 'en') {
				this.title = getParams.event.name_EN;
				this.details = getParams.event.description_EN;
			} else {
				this.title = getParams.event.name_ES;
				this.details = getParams.event.description_ES;
			}
			this.fileName = getParams.event.image;
			this._globalServ.planImagePath = this._globalServ.planImagePost = this.pathImageCropper = this.url + '/get-image/' + getParams.event.image;
			this.location = getParams.event.Places[0].street + ' ' + getParams.event.Cities[0].city + ' ' + getParams.event.Cities[0].country;
			console.log(this.location);
		}
		if (this.router.getCurrentNavigation().extras.state) {
			this.article = this.router.getCurrentNavigation().extras.state.item;
			let page = this.router.getCurrentNavigation().extras.state.page;
			this.currentPage = page;
			if (page == 'offer') {
				console.log(this.article)
				this.title = this.article.title;
				this.location = this.article.address;
				if (this._globalServ.language == 'en')
					this.details = this.article.description;
				else
					this.details = this.article.description;

				this.imageUrl = this.article.image;
				this._globalServ.planImagePath = this.imageUrl;
			}
			else {


				console.log(this.article)
				this.title = this.article.name_EN;
				this.location = this.article.Places[0].street, + ' ' + this.article.Places[0].City.city + ' ' + this.article.Places[0].City.country;
				if (this._globalServ.language == 'en')
					this.details = this.article.description_EN;
				else
					this.details = this.article.description_ES;

				this.imageUrl = this.article.image;
				this._globalServ.planImagePath = this.url + '/get-image/' + this.imageUrl;
				//this.pathImage = this.url + '/get-image/' + this.article.image;
				//this._globalServ.planImagePost = this.url + '/get-image/' + this.article.image;;
				//this.pathImageCropper = this.url + '/get-image/' + this.article.image;
			}
		}
		if (!this.actRoute.snapshot.queryParams.data && !this.actRoute.snapshot.queryParams.flag && !this.router.getCurrentNavigation().extras.state) {
			this.planCreate = true;
		}
		this.myForm = fb.group({
			'title': ['', Validators.compose([Validators.required])],
			'location': ['', Validators.compose([Validators.required])],
			'meetingPoint': ['', Validators.compose([Validators.required])],
			'details': ['', Validators.compose([Validators.required])],
		});

		if (this.platform.is('android')) {
			window.addEventListener('keyboardWillShow', (event: any) => {
				console.log("keyboard show", event)
				document.getElementById('container').style.marginBottom = (90 + event.keyboardHeight) + 'px';
				this.scroll.scrollToPoint(0, event.path[0].document.activeElement.offsetParent.offsetTop, 500)
			})

			window.addEventListener('keyboardDidHide', (event: any) => {
				console.log("keyboard show", event)
				document.getElementById('container').style.marginBottom = 'unset';
			})
		}
		console.log(this._globalServ.planImagePath);
		if (this.currentPage !== "group") {
			this.storage.set('plan-create-groups', -2);
		}

	}

	ngOnInit() {
		if (this.categoryList.length) {
			this._globalServ.idsCategories = this.categoryList;
		} else {
			this._globalServ.idsCategories = null;
		}

		//this.initializeBackButtonCustomHandler();
	}

	logScrolling(e) {
		if (e.detail.scrollTop > 50) {
			this.pageTitleName = true;
		} else {
			this.pageTitleName = false;
		}
	}

	// initializeBackButtonCustomHandler(): void {
	// 	this.unsubscribeBackEvent = this.platform.backButton.subscribeWithPriority(0, () => {
	// 		this.presentAlert();
	// 	});
	// 	/* here priority 101 will be greater then 100 
	// 	if we have registerBackButtonAction in app.component.ts */
	// }

	async presentAlert() {
		const alert = await this.alertController.create({
			header: 'Discard Plan ?',
			message: "Your Plan hasn't been created yet.If you discard now, your progress won't be saved.",
			buttons: [
				{
					text: 'Keep',
					cssClass: 'secondary',
					handler: (blah) => {
						//this.router.navigate(['plan-create']);
					}
				}, {
					text: 'Discard',
					role: 'cancel',
					handler: () => {
						this.confirmedExit = true;
						this._globalServ.invited = null;
						this.storage.set('group-create-members', []);
						this._globalServ.createdBy = "";
						this._globalServ.price = "";
						this.navCtrl.back();
						console.log('Cancel');
					}
				}
			]
		});
		await alert.present()
		return;
	}

	ionViewDidEnter() {
		this.validate();
		console.log('hey');
		if (this._globalServ.createdBy)
			this.createdBy = this._globalServ.createdBy;

		if (this._globalServ.idsCategories != null) {
			this._preferencesServ.getSupercategories({ 'active': 1 }).subscribe((data: any) => {


				if (this._globalServ.idsCategories != null) {
					this.category = []
					for (let itemOne of data) {
						for (let itemTwo of this._globalServ.idsCategories) {
							if (itemOne.id == itemTwo) {
								itemOne.checked = true;
								this.category.push(itemOne);
								this.okIdsCategories = true;
								console.log(this.category);
							}
						}
					}
					this.validate();

				}
			}, error => {
				console.log(error);
			})
			// if (this._globalServ.category)
			// 	this.category = this._globalServ.category[0];
		} else {
			this.category = [];
			this.okIdsCategories = false;
		}

		this.price = this._globalServ.price;
		this.storage.get('planType').then((val) => {
			if(val === 1) this.planType = 'Public';
			else if(val ===2) this.planType = 'Private';
			else this.planType = ''
		});
		let input = document.getElementById('googlePlaces').getElementsByTagName('input')[0];

		let autocomplete = new google.maps.places.Autocomplete(input, { types: ['geocode'] });

		google.maps.event.addListener(autocomplete, 'place_changed', () => {
			// retrieve the place object for your use
			let place = autocomplete.getPlace();
			console.log(place);
			this.location = place.formatted_address;
			this.detectorRef.detectChanges();
		});

		let inputM = document.getElementById('googlePlacesM').getElementsByTagName('input')[0];
		console.log(inputM);
		let autocompleteM = new google.maps.places.Autocomplete(inputM, { types: ['geocode'] });

		google.maps.event.addListener(autocompleteM, 'place_changed', () => {
			// retrieve the place object for your use
			let place = autocompleteM.getPlace();
			console.log(place);
			this.meetingPoint = place.formatted_address;
			this.detectorRef.detectChanges();
		});
		this._userServ.getPublicProfile(this._globalServ.idUser).subscribe(data => {
			this.profile = get(data, '[0]', {});
		}, err => {
			console.log('err public profile: ', err);
		});

		this.storage.get('plan-create-seats').then((val) => {
			this.avalibleSeats = val;
		});
		this.storage.get('plan-create-groups').then((val) => {
			if (val === -2) { // -2: Me, 0> : selected from group list
				this.groupId = val;
				let lan = this._globalServ.language === "en" ? "Pick" : "Recoger";
				this.createdBy = this.planCreate ? "Me" : (this.currentPage === "group" ? (this.title ? this.title : lan) : "Me");
			} else {
				this.groupId = val;
				this.storage.get('title-plan-create-groups').then((val) => {
					console.log('title-plan-create-groups', val);
					if (val)
						this.createdBy = val;


				});
				if (this.currentPage === "group") {
					this.groupId = val;
					this.createdBy = this.title ? this.title : "Me";
				}
			}
		});

	}

	getPlanCreatedBy() {
		if (this.currentPage !== "group") {
			let navigationExtras: NavigationExtras = {
				queryParams: {
					data: JSON.stringify({ 'groupId': this.groupId })
				}
			};
			this.router.navigate(['plan-create-createdby'], navigationExtras);
		}
	}

	getPlanDate() {
		let date;
		if (this.planDate == null) {
			date = new Date();
		} else {
			date = this.planDate;
		}
		this.datePicker.show({
			date: new Date(date),
			mode: 'date',
			androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT,
			//minDate:new Date()
			allowFutureDates: true,
			allowOldDates: false
		}).then(
			date => {
				console.log('Got date: ', date);
				this.planDate = date;
			},
			err => {
				console.log('Error occurred while getting date: ', err);
			}
		)
	}

	getLimitDate() {
		let date;
		if (this.limitDate == null) {
			date = new Date();
		} else {
			date = this.limitDate;
		}
		this.datePicker.show({
			date: new Date(date),
			mode: 'date',
			androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT,
			allowFutureDates: true,
			allowOldDates: false
		}).then(
			date => {
				console.log('Got date: ', date);
				this.limitDate = date;
			},
			err => {
				console.log('Error occurred while getting date: ', err);
			}
		)
	}

	getavalibleSeats() {
		this.router.navigate(['plan-create-seats']);
	}

	getPrice() {
		this.router.navigate(['price']);
	}

	getPlanType() {
		this.router.navigate(['plan-type']);
	}

	getPlanTypeValue(){
		this.storage.get('planType').then((val) => {
			return val;
		});
	}

	async goToselectImage() {
		let navigationExtras: NavigationExtras = {
			queryParams: {
				data: JSON.stringify({ 'plan': true })
			}
		};
		let titleLabel;
		let cancelLabel;
		let EditLabel;
		let ChangeLabel;
		let RemoveLabel;
		if (this._globalServ.language == 'en') {
			titleLabel = 'Photo';
			cancelLabel = 'Cancel';
			EditLabel = 'Edit Photo';
			ChangeLabel = 'Change Photo';
			RemoveLabel = 'Remove Photo';
		}
		if (this._globalServ.language == 'es') {
			titleLabel = '';
			cancelLabel = 'Cancelar';
			EditLabel = 'Editar foto';
			ChangeLabel = 'Cambiar Fotografía';
			RemoveLabel = 'Eliminar foto'
		}
		// const actionSheetForCancel = await this.actionSheetController.create({
		// 	cssClass: 'actionsheet2',
		// 	buttons: [{
		// 		text: cancelLabel,
		// 		handler: () => {
		// 			actionSheetForPlan_group.dismiss()
		// 		}
		// 	}
		// 	]
		// });


		const actionSheetForPlan_group = await this.actionSheetController.create({
			header: titleLabel,

			buttons: [
				{
					text: EditLabel,

					handler: () => {
						this.cropFunc();
						//this.router.navigate(['image-cropper'], navigationExtras);


					}
				},
				{
					text: ChangeLabel,
					handler: () => {
						this.selectFile(2)
						//this.router.navigate(['image-cropper'], navigationExtras);
					}
				},
				{
					text: RemoveLabel,
					handler: () => {
						this._globalServ.planImagePath = null;
					}
				},
				{
					text: cancelLabel,

					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				}
			]
		}).then(actionsheet => {
			actionsheet.present();
		});;



	}

	async goToselectImg() {
		let navigationExtras: NavigationExtras = {
			queryParams: {
				data: JSON.stringify({ 'plan': true })
			}
		};
		let titleLabel;
		let cancelLabel;
		let EditLabel;
		let ChangeLabel;
		let RemoveLabel;
		if (this._globalServ.language == 'en') {
			titleLabel = 'Photo';
			cancelLabel = 'Cancel';
			EditLabel = 'Take Picture';
			ChangeLabel = 'Choose Photo';
		}
		if (this._globalServ.language == 'es') {
			titleLabel = '';
			cancelLabel = 'Cancelar';
			EditLabel = 'Tomar la foto';
			ChangeLabel = 'Escoge una foto';
		}
		const actionSheetForCancel = await this.actionSheetController.create({
			cssClass: 'actionsheet2',
			buttons: [{
				text: cancelLabel,
				handler: () => {
					actionSheetForPlan_group.dismiss()
				}
			}
			]
		});


		const actionSheetForPlan_group = await this.actionSheetController.create({
			header: titleLabel,
			cssClass: 'actionSheetForPlan_group',
			buttons: [
				{
					text: EditLabel,

					handler: () => {
						actionSheetForCancel.dismiss();
						this.selectFile(1);
					}
				},
				{
					text: ChangeLabel,
					handler: () => {
						actionSheetForCancel.dismiss()
						this.selectFile(2);
						// this.router.navigate(['image-cropper'], navigationExtras);
					}
				}
			]
		});
		actionSheetForCancel.present();
		actionSheetForPlan_group.present();

		actionSheetForPlan_group.onDidDismiss().then(() => {
			actionSheetForCancel.dismiss();
		});
		actionSheetForCancel.onDidDismiss().then(() => {
			actionSheetForPlan_group.dismiss();
		});
	}

	getSupercategory() {
		let navigationExtras: NavigationExtras = {
			queryParams: {
				data: JSON.stringify({ 'plan_create': true })
			}
		};
		// this.router.navigate(['group-create-category'], navigationExtras);
		this.navCtrl.navigateForward(['group-create-category'], navigationExtras)
	}

	selectFile(source) {
		const options: CameraOptions = {
			quality: 50,
			targetWidth: 800,
			targetHeight: 600,
			destinationType: this.camera.DestinationType.FILE_URI,
			encodingType: this.camera.EncodingType.JPEG,
			mediaType: this.camera.MediaType.PICTURE,
			sourceType: source
		}

		this.camera.getPicture(options).then((imageData) => {
			this.pathImage = this.sanitizer.bypassSecurityTrustUrl(this.webview.convertFileSrc(imageData));
			//this.pathImage = this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + imageData);
			// this.base64.encodeFile(imageData).then((base64File: string)=>{
			// 	console.log("base64 data =>",base64File);
			// 	this._globalServ.planImagePost = base64File
			// }).catch(err=>{
			// 	console.log(err);
			// })
			this.pathImageCropper = imageData;
			this.pathImagePost = imageData;
			this._globalServ.planImagePost = imageData;
			this._globalServ.planImagePath = this.pathImage;
			this.cropFunc();
		}, (err) => {
			console.log(err);
		});
	}
	cropFunc() {
		this.crop.crop(this.pathImageCropper, { quality: 75 })
			.then(
				newImage => {
					this.pathImage = this.sanitizer.bypassSecurityTrustUrl(this.webview.convertFileSrc(newImage));
					this.pathImageCropper = newImage;
					this.pathImagePost = newImage;
					this._globalServ.planImagePost = newImage;
					this._globalServ.planImagePath = this.pathImage;
					
				},
				error => console.error('Error cropping image', error)
			);
	}
	async showAlert(msg) {
		const toast = await this.toastController.create({
			message: msg,
			duration: 2000
		});
		toast.present();
	}

	doCreatePlan(endpoint, params) {
		console.log('<<<  starting plan creating....   >>>');
		let navigationExtras: NavigationExtras = {
			state: {
				title: this.title,
			}
		};
		if (this._globalServ.planImagePost) {
			// this._planServ.createPlan(endpoint, params, this._globalServ.planImagePost).subscribe(data => {
			// 	this.done = true;
			// 	console.log('<<<<created plan successfully !!!>>>>');

			// 	this._globalServ.planImagePath = null;
			// 	this._globalServ.planImagePost = null;
			// 	this._globalServ.invited = null;
			// 	this.storage.set('group-create-members', []);
			// 	this.storage.set('plan-create-seats', -1);
			// 	this.storage.set('plan-create-groups', -1);

			// 	this.router.navigate(['plan-create-success'], navigationExtras);
			// }, err => {
			// 	console.log('err: ', err);
			// })

			const fileTransfer: FileTransferObject = this.transfer.create();
			let filename = 'planImage_' + Math.random() * 100000000000000000;
			let options: FileUploadOptions = {
				fileKey: 'image',
				fileName: filename + '.jpg',
				params: params,
			}
			console.log(this._globalServ.planImagePost)
			console.log(environment.apiUrl)
			console.log(endpoint)
			this._globalServ.planImagePost = this._globalServ.planImagePost ? this._globalServ.planImagePost : "";
			fileTransfer.upload(this._globalServ.planImagePost, environment.apiUrl + `/${endpoint}`, options)
				.then((data) => {
					// this.showAlert('Created a plan successfully');
					console.log('<<<<created plan successfully !!!>>>>');

					this.ga.trackEvent((endpoint == "app-add-plan" ? "Student Plan - " : "Group Plan - ") + params.title, "Created");

					this._globalServ.planImagePath = null;
					this._globalServ.planImagePost = null;
					this._globalServ.invited = null;
					this.storage.set('group-create-members', []);
					this.storage.set('plan-create-seats', -1);
					this.storage.set('plan-create-groups', -2);
					let navigationExtras: NavigationExtras = {
						state: {
							title: this.title,
						}
					};
					this.router.navigate(['plan-create-success'], navigationExtras);
				}, (err) => {
					// error
					this.done = false;
					console.log('error', err);
					// this.showAlert(err.message);
				});
		} else {
			this._planServ.createPlanNoImage('add-group-plan-existing-image', params, this.fileName).subscribe(data => {
				this.done = true;
				console.log('<<<<created plan successfully !!!>>>>');
				this.ga.trackEvent("Group Plan - " + params.title, "Created");
				this._globalServ.planImagePath = null;
				this._globalServ.planImagePost = null;
				this._globalServ.invited = null;
				this.storage.set('plan-create-seats', -1);
				this.storage.set('plan-create-groups', -2);

				this.router.navigate(['plan-create-success'], navigationExtras);
			}, err => {
				console.log('err: ', err);
			})
		}

	}

	save(value: any) {
		console.log('Value', value)
		//this.router.navigate(['plan-create-success']);
		if (!this._globalServ.planImagePath) {
			this.showAlert('Please upload the image');
			return;
		}
		let member;
		this.storage.get('group-create-members').then((val) => {
			if (val && val.length != 0) {
				member = val;
				console.log(member);
			}
		})

		console.log('here:  ', this.groupId);

		this.post = true;
		console.log('plan date: ', this.planDate);
		let hour;
		let minute;
		if (new Date(this.limitDate).getHours() < 9) {
			hour = '0' + new Date(this.limitDate).getHours();
		}
		else
			hour = new Date(this.limitDate).getHours();

		if (new Date(this.limitDate).getMinutes() < 9) {

			minute = '0' + new Date(this.limitDate).getMinutes();
		}
		else
			minute = new Date(this.limitDate).getMinutes();

		// this.planDate = this.planDate.slice(0, 11) + hour + ':' + minute;
		this.planDate = moment(this.planDate).format("MM/DD/YYYY")  + ' ' + hour + ':' + minute;
		console.log('Formated Plan Date', this.planDate);


		console.log('this._globalServ.planImagePost: ', this._globalServ.planImagePost);
		console.log('this.okIdsCategories:  ', this.okIdsCategories);
		console.log('this.myForm.valid: ', this.myForm.valid);

		if (this.myForm.valid && (this._globalServ.planImagePost != null || this._globalServ.planImagePath) && this.planDate != null && this.okIdsCategories == true) {
			this.done = false;
			this.calendar.createEvent(value.title, value.location,value.details, new Date(), new Date(this.planDate.slice(0,10))).then(res => {
				console.log('Response', res);
				this.calendar.getCreateCalendarOptions().calendarColor === '#b5915d'
			}).catch(err => console.log('Error Occured', err))
			console.log('save step 1');
			let optionsGeo: NativeGeocoderOptions = {
				useLocale: true,
				maxResults: 1
			};
			this.nativeGeocoder.forwardGeocode(value.location, optionsGeo)
				.then((result: NativeGeocoderResult[]) => {
					console.log('save step 2');
					console.log('The coordinates are latitude=' + result[0].latitude + ' and longitude=' + result[0].longitude);
					const fileTransfer: FileTransferObject = this.transfer.create();
					let options = {};
					let endpoint = '';

					if (this.groupId === null || this.groupId < 0) { // use app-add-plan						
						console.log('Plan Type 1: ', this.planType);
						options = {
							'fk_ceu_user_id': this._globalServ.idUser,
							'ceu_supercategories_ids': this._globalServ.idsCategories.toString(),
							'title': value.title,
							'description': value.details,
							'plan_date': this.planDate,
							'address': value.location,
							'price': this.price,
							'planType': this.planType == 'Private' ? 2 : 1,
							'meeting_point': this.meetingPoint,
							'plan_participant_ids': member,
							'latitude': String(result[0].latitude),
							'longitude': String(result[0].longitude)
						};
						endpoint = 'app-add-plan';
					} else { // use add-group-plan
						if (this.groupId > 0) {
							console.log('Plan Type 2: ', this.planType);
							options = {
								'fk_user_id': this._globalServ.idUser,
								'email': this.profile.email,
								'seats': this.avalibleSeats,
								'group_plan_supercategory_ids': this._globalServ.idsCategories.toString(),
								'title': value.title,
								'description': value.details,
								'plan_date': this.planDate,
								'address': value.location,
								'price': this.price,
								'planType': this.planType == 'Private' ? 2 : 1,
								'meeting_point': this.meetingPoint,
								'group_plan_participant_ids': member,
								'latitude': String(result[0].latitude),
								'longitude': String(result[0].longitude),
								'fk_group_id': this.groupId
							}
						} else {
							console.log('Plan Type: ', this.planType);
							options = {
								'fk_user_id': this._globalServ.idUser,
								'email': this.profile.email,
								'seats': this.avalibleSeats,
								'group_plan_supercategory_ids': this._globalServ.idsCategories.toString(),
								'title': value.title,
								'description': value.details,
								'plan_date': this.planDate,
								'address': value.location,
								'price': this.price,
								'planType': this.planType == 'Private' ? 2 : 1,
								'meeting_point': this.meetingPoint,
								'group_plan_participant_ids': member,
								'latitude': String(result[0].latitude),
								'longitude': String(result[0].longitude)
							}
						}
						endpoint = 'add-group-plan';
					}
					this.doCreatePlan(endpoint, options);
				})
				.catch((error: any) => {
					this.done = true;
					this.showAlert("Enter valid location and meeting point");
					console.log(error)
				});
		}
	}
	getLocation(event) {
		if (this.location.length > 1) {
			let input = document.getElementById('googlePlaces').getElementsByTagName('input')[0];
			let autocomplete = new google.maps.places.Autocomplete(input, { types: ['geocode'] });

		}
	}
	// async showAddressModal(event) {
	// 	this.input = document.getElementById('googlePlaces').getElementsByTagName('input')[0];
	// 	if (this.modal) {
	// 		this.modal.dismiss({

	// 		});
	// 		this.modal = undefined;
	// 	} else {
	// 		this.modal = await this.modalCtrl.create({
	// 			component: AutocompletePage,
	// 			backdropDismiss: false,
	// 			showBackdrop: false,
	// 			cssClass: 'automodal',
	// 			componentProps: {

	// 				'text': event.detail.value
	// 			}
	// 		});
	// 		return await this.modal.present();
	// 		this.myInput.setFocus();
	// 		await this.modal.onDidDismiss().then(res => {
	// 			console.log(res);
	// 			this.location = res;
	// 		})
	// 		let { data } = await this.modal.onDidDismiss();
	// 		console.log(data);
	// 	}
	// }

	invitePeople() {
		this.router.navigate(['group-create-invite']);
	}
	hideKeyboard() {
		this.keyboard.hide();

	}
	validate() {
		console.log('reacghed');
		if (this.title != '' || this.location != '' || this.meetingPoint != '' || this.details != '') {
			this.confirmedExit = false
		}

		if (this.category && this.category.length && this.createdBy && this.title && this.location && this.meetingPoint && this.details) {
			this.validated = true;
		}
		else {
			this.validated = false;
			// this.confirmedExit = true
		}

	}

	// selectFile(source) {
	//     const options: CameraOptions = {
	//         quality: 100,
	//         destinationType: this.camera.DestinationType.FILE_URI,
	//         encodingType: this.camera.EncodingType.JPEG,
	//         mediaType: this.camera.MediaType.PICTURE,
	//         sourceType: source
	//     };
	//     this.camera.getPicture(options).then((imageData) => {
	//         this.pathImage = this.sanitizer.bypassSecurityTrustUrl(this.webview.convertFileSrc(imageData));
	//         this.pathImageCropper = imageData;
	//         this.pathImagePost = imageData;
	//         this.cropFunc();
	//         console.log("Image path postt", this.pathImagePost);
	//     }, (err) => {
	//         console.log(err);
	//     });
	// }

	// cropFunc() {
	//     this.crop.crop(this.pathImageCropper, { quality: 75 })
	//         .then(
	//             newImage => {
	//                 this.pathImage = this.sanitizer.bypassSecurityTrustUrl(this.webview.convertFileSrc(newImage));
	//                 this.pathImageCropper = newImage;
	//                 this.pathImagePost = newImage;
	//                 this.uploadProfile();
	//             },
	//             error => console.error('Error cropping image', error)
	//         );
	// }

	uploadProfile() {
		const fileTransfer: FileTransferObject = this.transfer.create();

		let filename = 'profileImage_' + Math.random() * 100000000000000000 + '.jpg';

		let options: FileUploadOptions = {
			fileKey: 'image',
			fileName: filename,
		};

		fileTransfer.upload(this.pathImagePost, environment.apiUrl + '/app-user-set-profile-image/' + this._globalServ.idUser, options)
			.then((data) => {
				this._globalServ.planImagePath = this.pathImage;
				this._globalServ.planImagePost = this.pathImagePost;
				console.log('success', data);
				this.router.navigate(['plan-create'], {});
			}, (err) => {
				console.log('error', err);
				this.router.navigate(['plan-create'], {});
			})
	}

	goBack() {

		if (this.confirmedExit == false) {
			this.presentAlert();
		} else {
			this.confirmedExit = true;
			this._globalServ.invited = null;
			this.storage.set('group-create-members', []);
			this._globalServ.createdBy = "";
			this._globalServ.price = "";
		}

		// this._globalServ.invited = null;
		// this.storage.set('group-create-members', []);
		// this._globalServ.createdBy = "";
	}
}
