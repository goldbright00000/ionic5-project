import { Component, OnInit } from "@angular/core";
import { Router, NavigationExtras } from "@angular/router";
import { UserService } from "../../services/user.service";
import { GlobalService } from "../../services/global.service";
import { Device } from "@ionic-native/device/ngx";
import { environment } from "../../../environments/environment";
import { Storage } from "@ionic/storage";
import { EmailComposer } from "@ionic-native/email-composer/ngx";

@Component({
  selector: "app-more",
  templateUrl: "./more.page.html",
  styleUrls: ["./more.page.scss"]
})
export class MorePage implements OnInit {
  getDataProfile: any;
  firstName: any;
  lastName: any;
  url: any = environment.apiUrl;
  image: any;

  constructor(
    private router: Router,
    public _userServ: UserService,
    public _globalServ: GlobalService,
    public device: Device,
    private storage: Storage,
    private emailComposer: EmailComposer
  ) {
    this.getDataProfileFunc();
  }

  ngOnInit() {}

  goToSetting() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ setting: true })
      }
    };
    this.router.navigate(["tabs/profile"], navigationExtras);
  }

  goToJoined() {
    this.router.navigate(['joined'], {});
  }

  getDataProfileFunc() {
    this._userServ
      .getProfile(this._globalServ.idUser, { entity: this._globalServ.org })
      .subscribe(
        dataProf => {
          this.getDataProfile = dataProf;
          this._globalServ.firstName = this.firstName = this.getDataProfile.user.first_name;
          this._globalServ.lastName = this.lastName = this.getDataProfile.user.last_name;
          this.image = this.getDataProfile.user.image;
        },
        error => {
          console.log(error);
        }
      );
  }

  goToViewProfile() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ idUser: this._globalServ.idUser })
      }
    };
    this.router.navigate(["/view-profile"], navigationExtras);
  }

  goToAbout() {
    this.router.navigate(["tabs/about"], {});
  }

  goToClubs() {
    this.router.navigate(["tabs/clubs"], {});
  }

  goToGroups() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        calendar: true
      }
    };
    this.router.navigate(["tabs/groups"], navigationExtras);
  }

  logout() {
    this.storage.clear();
    this._globalServ.idUser = null;
    this.router.navigate(["start"], { replaceUrl: true });
  }

  sendEmail() {
    this._userServ.getFeedback().subscribe((data: any) => {
      console.log(data);
      let email = {
        // to: "info@vistingo.com",
        to: data.feedback_settings[0].email_address,
        subject: data.feedback_settings[0].subject,
        body: data.feedback_settings[0].content
      };
      this.emailComposer.open(email);
    });
  }

  goTovoucher() {
    this.router.navigate(["gain-voucher"], {});
  }
}
