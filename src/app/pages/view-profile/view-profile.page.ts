import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../services/global.service';
import { UserService } from '../../services/user.service';
import { get } from 'lodash';
import { ActivatedRoute } from '@angular/router';
import { ClubsService } from '../../services/clubs.service';
import { PlanService } from '../../services/plan.service';
import { environment } from '../../../environments/environment';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { AppLauncher, AppLauncherOptions } from '@ionic-native/app-launcher/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.page.html',
  styleUrls: ['./view-profile.page.scss'],
})
export class ViewProfilePage implements OnInit {

  /*   example: any = {
      id: 9,
      highlight: true,
      fk_ceu_user_id: 3,
      fk_department_id: 6,
      title: "Clases de Yoga",
      tags: null,
      description: "Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum Lor",
      image: "http://178.128.241.57/api/get-image-plan/Dietitian_Cassie_NEW.png",
      plan_date: "2019-09-22T15:00:00.000Z",
      meeting_point: "El Retiro",
      address: "Madrid, Calle de Bravo Murillo, M",
      places: 5,
      private_plan: false,
      status: 1,
      link: "",
      terms: true,
      creation_date: null,
      views: 0,
    }; */

  publicProfile: any = {};
  plans: any = [];
  clubs: any = [];
  profileId: any;
  groups: any = [
    {}, {}, {}
  ];
  userId: any;
  userEmail: any;
  constructor(
    private router: Router,
    private actRoute: ActivatedRoute,
    private location: Location,
    public _globalServ: GlobalService,
    private _userServ: UserService,
    private navParam: ActivatedRoute,
    private iab: InAppBrowser,
    private emailComposer: EmailComposer,
    private appLauncher: AppLauncher, private platform: Platform
  ) {
    console.log("userdata => ", this.actRoute.snapshot.queryParams);
    if (this.actRoute.snapshot.queryParams.data != null) {
      let getParams = JSON.parse(this.actRoute.snapshot.queryParams.data);
      console.log(getParams);
      this.profileId = getParams.idUser;
      this.userId = getParams.id;
      this.userEmail = getParams.email;
    }
  }


  ngOnInit() {
    this.getProfile();
    this.getPlans();
    this.getClubs();
    this.getGroups();
  }

  goBack() {
    this.location.back();
  }

  goToGroups() {
      let navParam: NavigationExtras = {
          queryParams: {
              id: this.userId
          }
      }
      this.router.navigate(['view-profile-groups'], navParam)
  }

  getProfile() {
    if (this.userId === undefined) {
      console.log("user =>", this.userId)
      this._userServ.getProfile(this._globalServ.idUser, {}).subscribe(data => {
        let userLoggedIn = data;

        this._userServ.getPublicProfileForView(this._globalServ.idUser, get(userLoggedIn, 'user.email', '')).subscribe(data => {
          this.publicProfile = get(data, '[0]', {});
          console.log('this.publicProfile: ', this.publicProfile);
        }, err => {
          console.log('err public profile: ', err);
        });
      }, err => {
        console.log('gp: ', err);
      });
    } else {
      console.log("user id =>", this.userId);
      this._userServ.getPublicProfileForView(this.userId, this.userEmail).subscribe(data => {
        console.log(data);
        this.publicProfile = data[0];
        var name = this.publicProfile.first_name.split(' ');
        this.publicProfile.first_name = name[0];
        var lastName = '';
        name.forEach(function (element,key) {
           if(key > 0 ){
               lastName = element ? (lastName + ' ' + element) : element;
           }
        });
        this.publicProfile.last_name = lastName;
        console.log(this.publicProfile);
      })
    }

  }

  getPlans() {
    if (this.userId === undefined) {
      this._userServ.getAppPlans(this._globalServ.idUser).subscribe((data: any) => {
        this.plans = data.plans;
        if(data.plans.length >= 5){
          this.plans = data.plans.slice(0,5);
        }else{
          this.plans = data.plans;
        }
        console.log('user plans: ', this.plans);
      }, err => {
        console.log('err user plans: ', err);
      });
    } else {
      this._userServ.getAppPlans(this.userId).subscribe((data: any) => {
          if(data.plans && data.plans.length) {
              this.plans = data.plans;
          }
        // for (let i = 0; i <= 3; i++) {
        //   this.plans.push(data.plans[i]);
        // }
        console.log('user plans: ', this.plans);
      }, err => {
        console.log('err user plans: ', err);
      });
    }
  }

  getClubs() {
    if (this.userId === undefined) {
      this._userServ.getUserClubs(this._globalServ.idUser).subscribe(data => {
        this.clubs = data;
        this.clubs = this.clubs.slice(0, 3);
      }, err => {
        console.log('err user clubs: ', err);
      });
    } else {
      this._userServ.getUserClubs(this.userId).subscribe(data => {
        this.clubs = data;
        this.clubs = this.clubs.slice(0, 3);
      }, err => {
        console.log('err user clubs: ', err);
      });
    }
  }

  getGroups() {
    if (this.userId === undefined) {
      this._userServ.getUserGroups(this._globalServ.idUser).subscribe(data => {
        this.groups = get(data, 'group', []);
        this.groups = this.groups.slice(0, 3);
      }, err => {
        console.log('err user groups: ', err);
      });
    } else {
      this._userServ.getUserGroups(this.userId).subscribe(data => {
        this.groups = get(data, 'group', []);
        this.groups = this.groups.slice(0, 3);
      }, err => {
        console.log('err user groups: ', err);
      });
    }

  }
  sendMail() {

    this.emailComposer.isAvailable().then((available: boolean) => {
      if (available) {
        //Now we know we can send
      }
    });

    let email = {
      to: 'info@vistingo.com',
      cc: '',


      subject: 's',
      body: '',

    }

    // Send a text message using default options
    this.emailComposer.open(email);
  }

  viewPreferences() {
    this.router.navigate(['/preferences']);
  }

  openInstagram() {
    const options: AppLauncherOptions = {
    }

    if (this.platform.is('ios')) {
      options.uri = 'fb://'
    } else {
      options.packageName = 'com.instagram.android'
    }

    this.appLauncher.canLaunch(options)
      .then((canLaunch: boolean) => console.log('Facebook is available'))
      .catch((error: any) => console.error('Facebook is not available'));
  }
  goToPlans(){
    let navParam: NavigationExtras = {
      queryParams: {
        id: this.userId
      }
    }
    this.router.navigate(['view-profile-plans'], navParam)
  }
    goToGroupDetail(value) {
        let navigationExtras: NavigationExtras = {
            queryParams: {
                data: JSON.stringify({ 'id': value.id , comp: 'view-profile'})
            },
        };
        this.router.navigate(['group-detail'], navigationExtras)
    }
}
