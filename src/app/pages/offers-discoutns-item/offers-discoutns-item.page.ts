import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { } from 'googlemaps';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import {ModalController, AlertController, NavController, ToastController} from '@ionic/angular';
import { ImageModalPage } from '../image-modal/image-modal.page'
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { get } from 'lodash';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { flattenStyles } from '@angular/platform-browser/src/dom/dom_renderer';
import {SocialSharing} from "@ionic-native/social-sharing/ngx";
import { GlobalService } from 'src/app/services/global.service';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { OfferDiscountService } from 'src/app/services/offer-discount.service';
import { BranchIOService } from 'src/app/services/branch-io.service';


@Component({
	selector: 'app-offers-discoutns-item',
	templateUrl: './offers-discoutns-item.page.html',
	styleUrls: ['./offers-discoutns-item.page.scss'],
})
export class OffersDiscoutnsItemPage implements OnInit {

	//latitudeInit: any = 40.4530541;
	//longitudeInit: any = -3.6883445;
	zoom: any = 14;
	item: any;
	read_flag: boolean;
	openingtimeArray:any=[];
	openingDatetime:any = [];
	newSplitArr:any = [];
	id:number;

	@ViewChild("map") mapElement;

	constructor(
		public location: Location,
		public actRoute: ActivatedRoute,
		public modalController: ModalController,
		public alertController: AlertController,
		private iab: InAppBrowser,
		private callNumber: CallNumber,
		private router: Router,
		public _globalServ: GlobalService,
		private navCtrl: NavController,
        private toastCtrl: ToastController,
		private socialShare: SocialSharing,
		private ga: GoogleAnalytics,
		private offerService: OfferDiscountService,
		private branch: BranchIOService,
		) {
		this.read_flag = false;		
		if (this.actRoute.snapshot.queryParams.data != null) {
			let getParams = JSON.parse(this.actRoute.snapshot.queryParams.data);
			this.id = getParams.id;			
		}
	}

	ngOnInit() {
	}

	splittheArray(item){
		return item.split(':')[0];
	}

	splitArrayWithValue(item){
		return item.split(':')[1];
	}

	ionViewDidEnter() {
		this.offerService.getOfferAnddiscountById(this.id).subscribe((res:any)=>{
			this.item = res.discount;
			console.log("THIS",this.item);
			this.openingtimeArray = this.item.openingHours.split('\n');
			for (var i = 0; i < this.openingtimeArray.length; ++i) {
				if(this.openingtimeArray[i]!='')
					this.openingDatetime.push(this.openingtimeArray[i].split(': ')[0],this.openingtimeArray[i].split(': ')[1]+'<br><br>');
			}
			console.log("Opening",this.openingDatetime);
			this.init();
		},(e)=>{
			console.log("Error "+ e);
		});		
	}

	init(){
		this.ga.trackEvent("Discount - "+this.item.title, "Viewed");
		//set map
		if(this.item.latitude && this.item.longitude) {
			let coords = new google.maps.LatLng(Number(this.item.latitude), Number(this.item.longitude));
			let mapOptions: google.maps.MapOptions = {
				center: coords,
				zoom: this.zoom,
				maxZoom: 17,
				minZoom: 3,
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				disableDefaultUI: true,
				styles: [
				{
					"featureType": "landscape",
					"stylers": [
					{
						"visibility": "off"
					}
					]
				},
				{
					"featureType": "poi",
					"stylers": [
					{
						"visibility": "off"
					}
					]
				},
				{
					"featureType": "road",
					"elementType": "labels",
					"stylers": [
					{
						"visibility": "off"
					}
					]
				},
				{
					"featureType": "transit",
					"stylers": [
					{
						"visibility": "off"
					}
					]
				}
				]
			}
			var map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
			var marker: google.maps.Marker = new google.maps.Marker({
				map: map,
				position: { lat: Number(this.item.latitude), lng: Number(this.item.longitude) },
			});
			// marker.setPosition({ lat: this.item.latitude, lng: this.longitudeInit })
		}
	}

	goBack() {
		this.navCtrl.back();
	}
	async presentModal(image) {
		const modal = await this.modalController.create({
			component: ImageModalPage,
			cssClass:'imageModal',
			backdropDismiss: true,
			componentProps: {
				image: image
			}
		});
		return await modal.present();
	}

	readMore(){
		this.read_flag = true;
	}
	readLess(){
		this.read_flag = false;
	}

	async showCode(item){
		let navigationExtras: NavigationExtras = {
			queryParams: {
				data: JSON.stringify({ 'code': this.item })
			},
		};
		this.router.navigate(['/offers-discounts-code'], navigationExtras);
	}


	async callWithNumber(phoneNumber) {
		const alert = await this.alertController.create({
			subHeader: phoneNumber,
			buttons: [
			{
				text: 'Cancel',
				role: 'cancel',
				cssClass: 'secondary',
				handler: (blah) => {
					console.log('Confirm Cancel: blah');
				}
			}, {
				text: 'Call',
				handler: () => {
					console.log('Confirm Okay');
					this.callNumber.callNumber(phoneNumber, true)
					.then(res => console.log('Launched dialer!', res))
					.catch(err => console.log('Error launching dialer', err));
				}
			}
			]
		});

		await alert.present();
	}

  goToCreatePlan(Item) {

		let navigationExtras: NavigationExtras = {
			state: {
				item: Item,
				page:"offer"


			}
		};
		this.router.navigate(['plan-create'], navigationExtras);
	}
	goToSite(website) {
		this.iab.create(website, '_system');
		// this.iab.create(get(this.item.website, 'offerAnddiscount', ''), '_system');
	}

    goToLocation(website) {
        this.iab.create(website + this.item.latitude + "," + this.item.longitude, '_system');
	}

	mailShare(){
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaEmail("Hi! Download Vive IE app and join my offer "+this.item.title+" "+res.url, "Vive IE app", []).then(()=>{
				this.ga.trackEvent("Discount - "+this.item.title, "Shared via Email");	
        	}).catch(()=>{
            	//this.showToast('App not found.');
        	});
		}).catch((err)=>{
			console.log(err);
		});			
	}

	facebookShare(){
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaFacebookWithPasteMessageHint(" ",null,res.url,"Hi! Download Vive IE app and join my offer "+this.item.title).then(()=>{
				this.ga.trackEvent("Discount - "+this.item.title, "Shared via Facebook");
        	}).catch(()=>{
            	//this.showToast('App not found.');
        	});
		}).catch((err)=>{
			console.log(err);
		});        
	}
	instagramShare(){
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaInstagram("Hi! Download Vive IE app and join my offer "+this.item.title+" "+res.url, null).then(()=>{
				this.ga.trackEvent("Discount - "+this.item.title, "Shared via Instagram");
        	}).catch(()=>{
           		// this.showToast('App not found.');
        	});
		}).catch((err)=>{
			console.log(err);
		});        
	}
    whatsappShare(){
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaWhatsApp("Hi! Download Vive IE app and join my offer "+this.item.title+" ",null,res.url).then(()=>{
				this.ga.trackEvent("Discount - "+this.item.title, "Shared via Whatsapp");
			}).catch(()=>{
			   // this.showToast('App not found.');
			});
		}).catch((err)=>{
			console.log(err);
		});         
    }
    twitterShare(){
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaTwitter("Hi! Download Vive IE app and join my offer "+this.item.title+" ",null,res.url).then(()=>{
				this.ga.trackEvent("Discount - "+this.item.title, "Shared via Twitter");
        	}).catch(()=>{
         		//   this.showToast('App not found.');
        	});
		}).catch((err)=>{
			console.log(err);
		});         
	}
	removeUndefined(itemOpening){
		var re = 'undefined';
		var str = itemOpening;
		var actualString = str.replace(re, " "); 
		return actualString;
	}

	checkHours(itemOpening2){
		var str1 = itemOpening2;
		var str2 = 'undefined';
		var res = str1.indexOf(str2);
				
		if(res != -1 || str1 == '' || str1 == null || !str1 ){
			return false;
		}
		return true;
	}
    async showToast(msg){
        const toast = await this.toastCtrl.create({
            message: msg,
            duration: 2000
        })
        await toast.present();
    }
}
