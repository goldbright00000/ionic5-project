import { Component, OnInit } from '@angular/core';
import { PlanService } from '../../services/plan.service';
import { Router, NavigationExtras } from '@angular/router';
import { GlobalService } from '../../services/global.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-joined-plans',
  templateUrl: './joined-plans.page.html',
  styleUrls: ['./joined-plans.page.scss'],
})
export class JoinedPlansPage implements OnInit {

  Participations: any;
  url: any = environment.apiUrl;

  constructor(
    public _planServ: PlanService,
    public _globalServ: GlobalService,
    public router: Router,
    ) {
   this._planServ.getParticipations(this._globalServ.idUser).subscribe(data => {
      this.Participations = data;
      console.log("participation",this.Participations)
;      for (let item of this.Participations) {
        item.image = this.url + '/get-ie-image-plan/' + item.image;
        item.join = true;
        item.type = 'student_plan';
      }
    })
  }

  ngOnInit() {
     
  }

  goToAllPlan(){
    this.router.navigate(['tabs/plan-list']);
  }

}
