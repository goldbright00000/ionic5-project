import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { CityGuideService } from '../../services/city-guide.service';
import { GlobalService } from '../../services/global.service';
import { environment } from '../../../environments/environment';
import { NavController, IonRefresher } from "@ionic/angular";

@Component({
  selector: 'app-city-guide-list',
  templateUrl: './city-guide-list.page.html',
  styleUrls: ['./city-guide-list.page.scss'],
})
export class CityGuideListPage implements OnInit {
  @ViewChild(IonRefresher)
  refresher: IonRefresher;
  placeHolder: string = './assets/img/placeholder.gif';
  getCat: any;
  url: any = environment.apiUrl;
  scrollPos: any = 0;
  pageTitleName: any = false;

  constructor(
    private router: Router,
    public _cityGuideServ: CityGuideService,
    public _globalServ: GlobalService,

    private navCtrl: NavController,
    private actRoute: ActivatedRoute,
  ) {
    this.actRoute.params.subscribe(val => {
      this._cityGuideServ.getDataCityGuideList();
    });

  }

  ngOnInit() {
  }
  ionViewDidEnter() {
    this.refresher.disabled = false;
  }

  ionViewWillLeave() {
    this.refresher.disabled = true;
  }

  logScrolling(e) {
    if (e.detail.scrollTop > 50) {
      this.pageTitleName = true;
    } else {
      this.pageTitleName = false;
    }
  }

  goToCityGuideItemList(value: any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'id': value.id, 'name_EN': value.name_EN, 'name_ES': value.name_ES })
      },
    };
    this.router.navigate(['tabs/city-guide-item-list'], navigationExtras)
  }

  openFilter() {

    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'city_guide': 1 })
      }
    };
    this.navCtrl.navigateForward(['filters'], navigationExtras);
  }
  doRefresh(event) {
   
    console.log('Begin async operation');
    this._cityGuideServ.getDataCityGuideList();

    setTimeout(() => {

      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
}
