import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { GurdGuard } from "./service/gurd/gurd.guard";

const routes: Routes = [
  //{ path: '', redirectTo: '', pathMatch: 'full' },
  // { path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule) },
  // { path: 'tabs', loadChildren: () => import('./pages/tabs/tabs.module').then(m => m.TabsPageModule) },
  // { path: 'profile', loadChildren: () => import('./pages/profile/profile.module').then(m => m.ProfilePageModule) },
  // { path: 'preferences', loadChildren: () => import('./pages/preferences/preferences.module').then(m => m.PreferencesPageModule) },
  // { path: 'preferences-ok', loadChildren: () => import('./pages/preferences-ok/preferences-ok.module').then(m => m.PreferencesOkPageModule) },
  // { path: 'filters', loadChildren: () => import('./pages/filters/filters.module').then(m => m.FiltersPageModule) },
  // //{ path: 'city-guide-list', loadChildren: './pages/city-guide-list/city-guide-list.module#CityGuideListPageModule' },
  // { path: 'city-guide-item-list', loadChildren: () => import('./pages/city-guide-item-list/city-guide-item-list.module').then(m => m.CityGuideItemListPageModule) },
  // { path: 'city-guide-item', loadChildren: () => import('./pages/city-guide-item/city-guide-item.module').then(m => m.CityGuideItemPageModule) },
  // //{ path: 'city-agenda-list', loadChildren: './pages/city-agenda-list/city-agenda-list.module#CityAgendaListPageModule' },
  // { path: 'city-agenda-item', loadChildren: () => import('./pages/city-agenda-item/city-agenda-item.module').then(m => m.CityAgendaItemPageModule) },
  // //{ path: 'offers-discounts-list', loadChildren: './pages/offers-discounts-list/offers-discounts-list.module#OffersDiscountsListPageModule' },
  // { path: 'plan-list', loadChildren: () => import('./pages/plan-list/plan-list.module').then(m => m.PlanListPageModule) },
  // { path: 'plan-item', loadChildren: () => import('./pages/plan-item/plan-item.module').then(m => m.PlanItemPageModule) },
  // { path: 'offers-discounts-map', loadChildren: () => import('./pages/offers-discounts-map/offers-discounts-map.module').then(m => m.OffersDiscountsMapPageModule) },
  // { path: 'image-cropper', loadChildren: () => import('./pages/image-cropper/image-cropper.module').then(m => m.ImageCropperPageModule) },
  // { path: 'language-list', loadChildren: () => import('./pages/language-list/language-list.module').then(m => m.LanguageListPageModule) },
  // { path: 'view-profile', loadChildren: () => import('./pages/view-profile/view-profile.module').then(m => m.ViewProfilePageModule) },
  // { path: 'view-profile-groups', loadChildren: () => import('./pages/view-profile-groups/view-profile-groups.module').then(m => m.ViewProfileGroupsPageModule) },
  // { path: 'view-profile-plans', loadChildren: () => import('./pages/view-profile-plans/view-profile-plans.module').then(m => m.ViewProfilePlansPageModule) },
  // { path: 'offers-discoutns-item', loadChildren: () => import('./pages/offers-discoutns-item/offers-discoutns-item.module').then(m => m.OffersDiscoutnsItemPageModule) },
  // { path: 'plan-create', loadChildren: () => import('./pages/plan-create/plan-create.module').then(m => m.PlanCreatePageModule) },
  // { path: 'plan-create-groups', loadChildren: () => import('./pages/plan-create-groups/plan-create-groups.module').then(m => m.PlanCreateGroupsPageModule) },
  // { path: 'plan-create-createdby', loadChildren: () => import('./pages/plan-create-createdby/plan-create-createdby.module').then(m => m.PlanCreateCreatedbyPageModule) },
  // { path: 'plan-create-seats', loadChildren: () => import('./pages/plan-create-seats/plan-create-seats.module').then(m => m.PlanCreateSeatsPageModule) },
  // { path: 'plan-create-success', loadChildren: () => import('./pages/plan-create-success/plan-create-success.module').then(m => m.PlanCreateSuccessPageModule) },
  // // { path: 'clubs', loadChildren: () => import('./pages/clubs/clubs.module').then(m => m.ClubsPageModule) },
  // { path: 'club-detail', loadChildren: () => import('./pages/club-detail/club-detail.module').then(m => m.ClubDetailPageModule) },
  // // { path: 'groups', loadChildren: () => import('./pages/groups/groups.module').then(m => m.GroupsPageModule) },
  // { path: 'group-detail', loadChildren: () => import('./pages/group-detail/group-detail.module').then(m => m.GroupDetailPageModule) },
  // { path: 'group-create', loadChildren: () => import('./pages/group-create/group-create.module').then(m => m.GroupCreatePageModule), canDeactivate: [GurdGuard] },
  // { path: 'group-create-category', loadChildren: () => import('./pages/group-create-category/group-create-category.module').then(m => m.GroupCreateCategoryPageModule) },
  // { path: 'group-create-settings', loadChildren: () => import('./pages/group-create-settings/group-create-settings.module').then(m => m.GroupCreateSettingsPageModule) },
  // { path: 'group-create-invite', loadChildren: () => import('./pages/group-create-invite/group-create-invite.module').then(m => m.GroupCreateInvitePageModule) },
  // { path: 'group-create-success', loadChildren: () => import('./pages/group-create-success/group-create-success.module').then(m => m.GroupCreateSuccessPageModule) },
  // { path: 'group-plan-item', loadChildren: () => import('./pages/group-plan-item/group-plan-item.module').then(m => m.GroupPlanItemPageModule) },
  // { path: 'members', loadChildren: () => import('./pages/members/members.module').then(m => m.MembersPageModule) },
  // { path: 'joined', loadChildren: () => import('./pages/joined/joined.module').then(m => m.JoinedPageModule) },
  // { path: 'joined-plans', loadChildren: () => import('./pages/joined-plans/joined-plans.module').then(m => m.JoinedPlansPageModule) },
  // { path: 'joined-clubs', loadChildren: () => import('./pages/joined-clubs/joined-clubs.module').then(m => m.JoinedClubsPageModule) },
  // { path: 'joined-group-plans', loadChildren: () => import('./pages/joined-group-plans/joined-group-plans.module').then(m => m.JoinedGroupPlansPageModule) },
  // { path: 'joined-groups', loadChildren: () => import('./pages/joined-groups/joined-groups.module').then(m => m.JoinedGroupsPageModule) },
  // { path: 'change-password', loadChildren: () => import('./pages/change-password/change-password.module').then(m => m.ChangePasswordPageModule) },
  // { path: 'write-comment', loadChildren: () => import('./pages/write-comment/write-comment.module').then(m => m.WriteCommentPageModule) },
  // { path: 'change-password', loadChildren: () => import('./pages/change-password/change-password.module').then(m => m.ChangePasswordPageModule) },
  // { path: 'offers-discounts-code', loadChildren: () => import('./pages/offers-discounts-code/offers-discounts-code.module').then(m => m.OffersDiscountsCodePageModule) },
  // { path: 'price', loadChildren: () => import('./pages/price/price.module').then(m => m.PricePageModule) },
  // { path: 'group-all', loadChildren: () => import('./pages/group-all/group-all.module').then(m => m.GroupAllPageModule) },
  // { path: 'promo-code', loadChildren: () => import('./pages/promo-code/promo-code.module').then(m => m.PromoCodePageModule) },
  // { path: 'promo-code-success', loadChildren: () => import('./pages/promo-code-success/promo-code-success.module').then(m => m.PromoCodeSuccessPageModule) },
  // { path: 'gain-voucher', loadChildren: () => import('./pages/gain-voucher/gain-voucher.module').then(m => m.GainVoucherPageModule) },
  // { path: 'edit-group', loadChildren: () => import('./pages/edit-group/edit-group.module').then(m => m.EditGroupPageModule) },
  // { path: 'tutorial', loadChildren: () => import('./pages/tutorial/tutorial.module').then(m => m.TutorialPageModule) },
  // { path: 'start', loadChildren: () => import('./pages/start/start.module').then(m => m.StartPageModule) },
  // { path: 'profile-first', loadChildren: () => import('./pages/profile-first/profile-first.module').then(m => m.ProfileFirstPageModule) },

  { path: "login", loadChildren: "./pages/login/login.module#LoginPageModule" },
  { path: "tabs", loadChildren: "./pages/tabs/tabs.module#TabsPageModule" },
  {
    path: "profile",
    loadChildren: "./pages/profile/profile.module#ProfilePageModule"
  },
  {
    path: "preferences",
    loadChildren: "./pages/preferences/preferences.module#PreferencesPageModule"
  },
  {
    path: "preferences-ok",
    loadChildren:
      "./pages/preferences-ok/preferences-ok.module#PreferencesOkPageModule"
  },
  {
    path: "filters",
    loadChildren: "./pages/filters/filters.module#FiltersPageModule"
  },
  //{ path: 'city-guide-list', loadChildren: './pages/city-guide-list/city-guide-list.module#CityGuideListPageModule' },
  {
    path: "city-guide-item-list",
    loadChildren:
      "./pages/city-guide-item-list/city-guide-item-list.module#CityGuideItemListPageModule"
  },
  {
    path: "city-guide-item",
    loadChildren:
      "./pages/city-guide-item/city-guide-item.module#CityGuideItemPageModule"
  },
  //{ path: 'city-agenda-list', loadChildren: './pages/city-agenda-list/city-agenda-list.module#CityAgendaListPageModule' },
  {
    path: "city-agenda-item",
    loadChildren:
      "./pages/city-agenda-item/city-agenda-item.module#CityAgendaItemPageModule"
  },
  //{ path: 'offers-discounts-list', loadChildren: './pages/offers-discounts-list/offers-discounts-list.module#OffersDiscountsListPageModule' },
  //{ path: 'plan-list', loadChildren: './pages/plan-list/plan-list.module#PlanListPageModule' },
  {
    path: "plan-item",
    loadChildren: "./pages/plan-item/plan-item.module#PlanItemPageModule"
  },
  {
    path: "offers-discounts-map",
    loadChildren:
      "./pages/offers-discounts-map/offers-discounts-map.module#OffersDiscountsMapPageModule"
  },
  {
    path: "image-cropper",
    loadChildren:
      "./pages/image-cropper/image-cropper.module#ImageCropperPageModule"
  },
  {
    path: "language-list",
    loadChildren:
      "./pages/language-list/language-list.module#LanguageListPageModule"
  },
  {
    path: "view-profile",
    loadChildren:
      "./pages/view-profile/view-profile.module#ViewProfilePageModule"
  },
  {
    path: "view-profile-groups",
    loadChildren:
      "./pages/view-profile-groups/view-profile-groups.module#ViewProfileGroupsPageModule"
  },
  {
    path: "view-profile-plans",
    loadChildren:
      "./pages/view-profile-plans/view-profile-plans.module#ViewProfilePlansPageModule"
  },
  {
    path: "offers-discoutns-item",
    loadChildren:
      "./pages/offers-discoutns-item/offers-discoutns-item.module#OffersDiscoutnsItemPageModule"
  },
  {
    path: "plan-create",
    loadChildren: "./pages/plan-create/plan-create.module#PlanCreatePageModule"
  },
  {
    path: "plan-create-groups",
    loadChildren:
      "./pages/plan-create-groups/plan-create-groups.module#PlanCreateGroupsPageModule"
  },
  {
    path: "plan-create-createdby",
    loadChildren:
      "./pages/plan-create-createdby/plan-create-createdby.module#PlanCreateCreatedbyPageModule"
  },
  {
    path: "plan-create-seats",
    loadChildren:
      "./pages/plan-create-seats/plan-create-seats.module#PlanCreateSeatsPageModule"
  },
  {
    path: "plan-create-success",
    loadChildren:
      "./pages/plan-create-success/plan-create-success.module#PlanCreateSuccessPageModule"
  },
  //{ path: 'clubs', loadChildren: './pages/clubs/clubs.module#ClubsPageModule' },
  {
    path: "club-detail",
    loadChildren: "./pages/club-detail/club-detail.module#ClubDetailPageModule"
  },
  //{ path: 'groups', loadChildren: './pages/groups/groups.module#GroupsPageModule' },
  {
    path: "group-detail",
    loadChildren:
      "./pages/group-detail/group-detail.module#GroupDetailPageModule"
  },
  {
    path: "group-create",
    loadChildren:
      "./pages/group-create/group-create.module#GroupCreatePageModule"
  },
  {
    path: "group-create-category",
    loadChildren:
      "./pages/group-create-category/group-create-category.module#GroupCreateCategoryPageModule"
  },
  {
    path: "group-create-settings",
    loadChildren:
      "./pages/group-create-settings/group-create-settings.module#GroupCreateSettingsPageModule"
  },
  {
    path: "group-create-invite",
    loadChildren:
      "./pages/group-create-invite/group-create-invite.module#GroupCreateInvitePageModule"
  },
  {
    path: "group-create-success",
    loadChildren:
      "./pages/group-create-success/group-create-success.module#GroupCreateSuccessPageModule"
  },
  {
    path: "group-plan-item",
    loadChildren:
      "./pages/group-plan-item/group-plan-item.module#GroupPlanItemPageModule"
  },
  {
    path: "members",
    loadChildren: "./pages/members/members.module#MembersPageModule"
  },
  {
    path: "joined",
    loadChildren: "./pages/joined/joined.module#JoinedPageModule"
  },
  {
    path: "joined-plans",
    loadChildren:
      "./pages/joined-plans/joined-plans.module#JoinedPlansPageModule"
  },
  {
    path: "joined-clubs",
    loadChildren:
      "./pages/joined-clubs/joined-clubs.module#JoinedClubsPageModule"
  },
  {
    path: "joined-group-plans",
    loadChildren:
      "./pages/joined-group-plans/joined-group-plans.module#JoinedGroupPlansPageModule"
  },
  {
    path: "joined-groups",
    loadChildren:
      "./pages/joined-groups/joined-groups.module#JoinedGroupsPageModule"
  },
  {
    path: "change-password",
    loadChildren:
      "./pages/change-password/change-password.module#ChangePasswordPageModule"
  },
  {
    path: "write-comment",
    loadChildren:
      "./pages/write-comment/write-comment.module#WriteCommentPageModule"
  },
  {
    path: "change-password",
    loadChildren:
      "./pages/change-password/change-password.module#ChangePasswordPageModule"
  },
  {
    path: "offers-discounts-code",
    loadChildren:
      "./pages/offers-discounts-code/offers-discounts-code.module#OffersDiscountsCodePageModule"
  },
  { path: "price", loadChildren: "./pages/price/price.module#PricePageModule" },
  {
    path: "plan-type",
    loadChildren: "./pages/plan-type/plan-type.module#PlanTypeModule"
  },
  {
    path: "group-all",
    loadChildren: "./pages/group-all/group-all.module#GroupAllPageModule"
  },
  {
    path: "promo-code",
    loadChildren: "./pages/promo-code/promo-code.module#PromoCodePageModule"
  },
  {
    path: "promo-code-success",
    loadChildren:
      "./pages/promo-code-success/promo-code-success.module#PromoCodeSuccessPageModule"
  },
  {
    path: "gain-voucher",
    loadChildren:
      "./pages/gain-voucher/gain-voucher.module#GainVoucherPageModule"
  },
  {
    path: "edit-group",
    loadChildren: "./pages/edit-group/edit-group.module#EditGroupPageModule"
  },
  {
    path: "tutorial",
    loadChildren: "./pages/tutorial/tutorial.module#TutorialPageModule"
  },
  { path: "start", loadChildren: "./pages/start/start.module#StartPageModule" },
  {
    path: "profile-first",
    loadChildren:
      "./pages/profile-first/profile-first.module#ProfileFirstPageModule"
  },
  {
    path: "group-calendar",
    loadChildren:
      "./pages/group-calendar/group-calendar.module#GroupCalendarPageModule"
  },
  { path: 'plan-calendar', loadChildren: './pages/plan-calendar/plan-calendar.module#PlanCalendarPageModule' },
  { path: 'joined-calendar', loadChildren: './pages/joined-calendar/joined-calendar.module#JoinedCalendarPageModule' },
  { path: 'group-create-type', loadChildren: './pages/group-create-type/group-create-type.module#GroupCreateTypePageModule' },
  { path: 'group-create-admin', loadChildren: './pages/group-create-admin/group-create-admin.module#GroupCreateAdminPageModule' }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
