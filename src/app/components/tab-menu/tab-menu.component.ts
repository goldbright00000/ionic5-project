import { Component, OnInit, ViewChild, ChangeDetectorRef, ViewEncapsulation, } from '@angular/core';
import { TabsPage } from 'src/app/pages/tabs/tabs.page';
import { IonTabs } from '@ionic/angular';
import { GlobalService } from 'src/app/services/global.service';
import { Keyboard } from '@ionic-native/keyboard/ngx';
@Component({
  selector: 'tab-menu',
  templateUrl: './tab-menu.component.html',
  styleUrls: ['./tab-menu.component.scss'],

})
export class TabMenuComponent implements OnInit {
  @ViewChild(IonTabs) tabs: IonTabs;
  select: any = 'none';
  selected: any = 0;

  constructor(public tab: TabsPage, public _globalServ: GlobalService, public detectorRef: ChangeDetectorRef, private keyboard: Keyboard) {
    this._globalServ.showIcon = true;
  }

  ngOnInit() {
    console.log("------********---------" + " tab-menu-ngonint: " + this._globalServ.showIcon);
  }

  async showMore() {
    this._globalServ.select = 'add';
    this.tab.showMore();
  }

  goto(page: string) {
    this._globalServ.select = page;
    this.tab.goto(page);

  }
}
